#include <iostream>
#include <queue>
#include "Common/Common.hpp"
#include "type.h"

class MMTraceSynthesizer{
private:
    int sz;
    int nthreads;
    int i0;
    int tick;
    const AddrInt A = 1;
    const AddrInt B = A+(1<<30);
    const AddrInt C = B+(1<<30);

public:
    queue<Record> refs;
    MMTraceSynthesizer(int _sz = 0, int _nthreads = 20){
        reset();
        sz = _sz;
        nthreads = _nthreads;
    };
    void setsize(int _sz) {sz = _sz;}
    void reset();
    bool IsEnd();
    Record getRef();
};

Record MMTraceSynthesizer::getRef(){
    Record rt;
    if(refs.size() == 0){
        int step = sz/nthreads;
        for(int j = 0; j < sz; j++)
            for(int k = 0; k < sz; k++){
                //A[i][k]
                for(int t = 0; t < nthreads; t++){
                    int i = i0+t*step;
                    rt.addr = A+i*sz+k;
                    rt.accessor = t;
                    rt.time = tick++;
                    rt.type = false;
                    refs.push(rt);
                }
                //B[k][j]
                for(int t = 0; t < nthreads; t++){
                    int i = i0+t*step;
                    rt.addr = B+k*sz+j;
                    rt.accessor = t;
                    rt.time = tick++;
                    rt.type = false;
                    refs.push(rt);
                }
                
                //C[i][j]
                for(int t = 0; t < nthreads; t++){
                    int i = i0+t*step;
                    rt.addr = C+i*sz+j;
                    rt.accessor = t;
                    rt.time = tick++;
                    rt.type = false;
                    refs.push(rt);
                }
                
                //C[i][j]
                for(int t = 0; t < nthreads; t++){
                    int i = i0+t*step;
                    rt.addr = C+i*sz+j;
                    rt.accessor = t;
                    rt.time = tick++;
                    rt.type = true;
                    refs.push(rt);
                }
            }
        i0++;
    }
    rt = refs.front();
    refs.pop();
    return rt;
}

bool MMTraceSynthesizer::IsEnd(){
    if(refs.size()) return false;
    if(i0 >= sz/nthreads) return true;
    return false;
}

void MMTraceSynthesizer::reset(){
    i0 = 0;
    tick = 1;
}

int main(int argc, char **argv){
    int sz = atoi(argv[1]);
    MMTraceSynthesizer mmtrace(sz, 20);
    while(!mmtrace.IsEnd()){
        auto ref = mmtrace.getRef();
        cout << ref.addr << endl;
    }
    return 0;
}
