#include <iostream>
#include <cstdint>

using namespace std;

uint64_t **A;
uint64_t **B;
uint64_t **C;
int sz;

uint64_t** allocateMatrix(int sz){
    uint64_t** rt = new uint64_t*[sz];
    for(int i = 0; i < sz; i++)
        rt[i] = new uint64_t[sz];
    return rt;
}

int main(int argc, char** argv){
    sz = atoi(argv[1]);
    A = allocateMatrix(sz);
    B = allocateMatrix(sz);
    C = allocateMatrix(sz);

    for(int itr = 0; itr < 10; itr++){
    #pragma omp parallel for
    for(int i = 0; i < sz; i++)
        for(int j = 0; j < sz; j++)
            for(int k = 0; k < sz; k++)
                C[i][j] += A[i][k] * B[k][j];
    }
    return 0;
}
