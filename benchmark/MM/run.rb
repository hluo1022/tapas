#!/usr/bin/ruby

require '../events.rb'

for i in [2048,1792,1536,1280,1024,896,768,640,512]
    system("perf stat -e #{Events} -aA -o mm#{i}.perf -r 5 numactl -m 0 ./mm #{i}")
end
