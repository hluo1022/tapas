#include <iostream>
#include <string>
#include <fstream>

#include "type.h"
#include "Trace/TraceWriter.hpp"
#include "MemRef.hpp"

using namespace std;

int main(int argc, char** argv){
    cout << "Usage : len" << endl; 
    int len = atoi(argv[1]);

    ofstream faddress("address");

    for(int i = 0; i < 20; i++){
        string filename = "fakerr.1111." + to_string(i);
        TraceWriter tr;
        tr.open((char*)filename.c_str());
        
        uint64_t tick = i+1;
        AddrInt addr = (i % 10) * len;
        addr *= 1000;
        addr *= 128;

        faddress << "Array" << i << hex << " " << addr << " ";
        
        MEMREF x;
        x.address = addr;
        x.type = false;
        x.thread = i;
        x.time = tick;
        for(int j = 0; j < len; j++, tick+=40, addr += 128){
            x.address = addr;
            x.time = tick;
            //cout << addr << " " << tick << endl;
            tr.write(x);
        }
        faddress << hex << addr << " ";
        faddress << dec << 0 << " " << len << endl;
    }

}
