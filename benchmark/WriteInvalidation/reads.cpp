#include <iostream>
#include <cstdint>
#include <chrono>

#include <omp.h>

#include "../../include/placer.h"

using namespace std;

void writes(uint64_t *A, int len, int itr = 1){
    while(itr--)
        for(int i = 0; i < len; i++)
            A[i]++;
}

uint64_t reads(uint64_t *A, int len, int itr = 1){
    uint64_t rt = 0;
    while(itr--){
        for(int i = 0; i < len; i++)
            rt += A[i];
        #pragma omp barrier
    }
    return rt;
}

uint64_t A[10][1<<27];

void ArrayReg(){
    open_place_file(); 
    register_named_address("Array0", A[0], sizeof(uint64_t));
    register_named_address("Array1", A[1], sizeof(uint64_t));
    register_named_address("Array2", A[2], sizeof(uint64_t));
    register_named_address("Array3", A[3], sizeof(uint64_t));
    register_named_address("Array4", A[4], sizeof(uint64_t));
    register_named_address("Array5", A[5], sizeof(uint64_t));
    register_named_address("Array6", A[6], sizeof(uint64_t));
    register_named_address("Array7", A[7], sizeof(uint64_t));
    register_named_address("Array8", A[8], sizeof(uint64_t));
    register_named_address("Array9", A[9], sizeof(uint64_t));
    place_address_range();
    close_place_file();
}
int main(int argc, char**argv){
    int len = atoi(argv[1]);
    ArrayReg();
    
    for(int i = 0; i < 10; i++){
        for(int j = 0; j < len; j++)
        A[i][j] = j;
    }
    

    auto t1 = chrono::high_resolution_clock::now();
    uint64_t rt = 0;
#pragma omp parallel for
    for(int i = 0; i < 20; i++){
            rt += reads(A[i%10], len, 10);
    }
    auto t2 = chrono::high_resolution_clock::now();
    cout << rt << endl;
    cout << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count()/1000000000.0 << " seconds" << endl;
    return 0;
}
