#include <iostream>
#include <cstdint>

using namespace std;

void writes(uint64_t *A, int len, int itr = 1){
    while(itr--)
    for(int i = 0; i < len; i++)
        A[i]++;
}

uint64_t reads(uint64_t *A, int len, int itr = 1){
    uint64_t rt = 0;
    while(itr--)
    for(int i = 0; i < len; i++)
        rt += A[i];
    return rt;
}


int main(int argc, char**argv){
    int len = atoi(argv[1]);
    uint64_t **A = new uint64_t*[20];
    for(int i = 0; i < 20; i++){
        A[i] = new uint64_t[len];
        for(int j = 0; j < len; j++)
            A[i][j] = j;
    }

    uint64_t rt = 0;
    #pragma omp parallel for
    for(int i = 0; i < 10; i++){
        if(i < 5)
            rt += reads(A[i], len, 100);
        else
            rt += reads(A[i], len, 100);
    }
    cout << rt << endl;

    return 0;
}
