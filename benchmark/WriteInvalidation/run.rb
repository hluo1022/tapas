#!/usr/bin/ruby

require '../events.rb'

BIN_ROOT = "../../build/benchmark/WriteInvalidation/"

places = ["{0},{8},{16},{24},{32},{40},{48},{56},{64},{72}", "{0},{8},{16},{24},{32},{80},{88},{96},{104},{112}", "{0},{8},{16},{24},{32},{120},{128},{136},{144},{152}"]
output = ["01", "02", "03"]

ENV["OMP_NUM_THREADS"] = "10"

for p in 0...places.length
    ENV["OMP_PLACES"] = "#{places[p]}"
#    for i in 19..25
#        system("perf stat -e #{Events} -aA -o write_#{output[p]}_#{i}.perf -r 5 numactl -m 0 #{BIN_ROOT}/writes #{1<<i}")
#    end

    for i in 20..20 #19..25 
        system("perf stat -e #{Events} -aA -o noshare_read_#{output[p]}_#{i}.perf -r 5 numactl -m 0 #{BIN_ROOT}/reads #{(1<<i)+10000}")
    end
end
