#!/usr/bin/ruby

EventName = {
    'r62c04c' => 'PM_DATA_ALL_FROM_MEMORY',
    'r61c048' => 'PM_DATA_ALL_FROM_ON_CHIP_CACHE',
    'r64c04a' => 'PM_DATA_ALL_FROM_OFF_CHIP_CACHE',
    'r61c04a' => 'PM_DATA_ALL_FROM_RL2L3_SHR',
    'r62c046' => 'PM_DATA_ALL_FROM_RL2L3_MOD',
    'r63c048' => 'PM_DATA_ALL_FROM_DL2L3_SHR',
    'r64c048' => 'PM_DATA_ALL_FROM_DL2L3_MOD',
    'r61c04c' => 'PM_DATA_ALL_FROM_LL4',
    'r62c04a' => 'PM_DATA_ALL_FROM_RL4',
    'r63c04c' => 'PM_DATA_ALL_FROM_DL4',
    'r62c048' => 'PM_DATA_ALL_FROM_LMEM',
    'r63c04a' => 'PM_DATA_ALL_FROM_RMEM',
    'r64c04c' => 'PM_DATA_ALL_FROM_DMEM'
}


for file in ARGV
    content = File.readlines(file)

    parsed = []
    for i in 0...content.length
        if !content[i].start_with?("CPU")
            next
        end
        values = content[i].split
        values[1] = values[1].gsub(',','').to_i
        values[2] = EventName[values[2]]
        parsed << values
    end

    values = []
    for i in (0...parsed.length).step(5)
        rt = [(i/5)%4, 0, parsed[i][2]]
        for j in 0...5
            rt[1] += parsed[i+j][1]
        end
        values << rt
    end

    for i in values
        #Event CPU value
        puts "#{i[2]}\t#{i[0]}\t#{i[1]}"
    end

end
