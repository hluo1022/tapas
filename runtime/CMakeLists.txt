file(GLOB_RECURSE Tapas_SOURCES ./**.cpp)
file(GLOB_RECURSE Tapas_HEADERS ./**.hpp)

add_library(Configure SHARED Configure.cpp)
add_library(malloc_hook SHARED malloc_hook.cpp)
add_library(pthread_hook SHARED pthread_hook.cpp)

add_library(RandomThreadBind SHARED RandomThreadBind.cpp)
add_library(TestThreadBind SHARED TestThreadBind.cpp)
add_library(FixedThreadBind SHARED FixedThreadBind.cpp)

add_library(RandomDataBind SHARED RandomDataBind.cpp)
add_library(TestDataBind SHARED TestDataBind.cpp)
add_library(ShowDataPlacement SHARED ShowDataPlacement.cpp)
add_library(FixedDataBind SHARED FixedDataBind.cpp)

target_link_libraries(pthread_hook dl pthread )
target_link_libraries(pthread_hook Configure)
target_link_libraries(FixedThreadBind Configure)

target_link_libraries(malloc_hook dl Configure numa)
target_link_libraries(RandomDataBind Configure numa)
target_link_libraries(ShowDataPlacement Configure numa)
target_link_libraries(FixedDataBind Configure numa)

set_target_properties(Configure PROPERTIES
    COMPILE_FLAGS "${CMAKE_CXX_FLAGS} -fPIC -shared"
    )
set_target_properties(malloc_hook PROPERTIES
    COMPILE_FLAGS "${CMAKE_CXX_FLAGS} -fPIC -shared"
    )
set_target_properties(pthread_hook PROPERTIES
    COMPILE_FLAGS "${CMAKE_CXX_FLAGS} -fPIC -shared -D__HT_Disabled__"
    )
set_target_properties(RandomThreadBind PROPERTIES
    COMPILE_FLAGS "${CMAKE_CXX_FLAGS} -fPIC -shared -D__HT_Disabled__"
    )
set_target_properties(TestThreadBind PROPERTIES
    COMPILE_FLAGS "${CMAKE_CXX_FLAGS} -fPIC -shared -D__HT_Disabled__"
    )
set_target_properties(FixedThreadBind PROPERTIES
    COMPILE_FLAGS "${CMAKE_CXX_FLAGS} -fPIC -shared -D__HT_Disabled__"
    )

set_target_properties(RandomDataBind PROPERTIES
    COMPILE_FLAGS "${CMAKE_CXX_FLAGS} -fPIC -shared"
    )
set_target_properties(TestDataBind PROPERTIES
    COMPILE_FLAGS "${CMAKE_CXX_FLAGS} -fPIC -shared"
    )
set_target_properties(ShowDataPlacement PROPERTIES
    COMPILE_FLAGS "${CMAKE_CXX_FLAGS} -fPIC -shared"
    )
set_target_properties(FixedDataBind PROPERTIES
    COMPILE_FLAGS "${CMAKE_CXX_FLAGS} -fPIC -shared"
    )
