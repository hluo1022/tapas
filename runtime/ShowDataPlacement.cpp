#include <iostream>
#include <vector>
#include <atomic>
#include <random>
#include <map>

#include <numa.h>
#include <numaif.h>

#include <thread>
#include <mutex>

#include "Configure.h"
#include "DataBind.h"

using namespace std;

const char *DataBindPolicyName = "Show Data Placement";

static mutex output_mutex;

class ShowDataPlacementPolicy : public _DataBindPolicy{
private:
    vector<size_t> array_sizes;
    vector<int*> page_nodes;
    int* page_nodes_containers;

    int tid;
    int free_id;
    int mchunk_size;
    ofstream fout;

    int total_pages;
public:
    ShowDataPlacementPolicy(int _tid);
    ~ShowDataPlacementPolicy();
    void RemoveData(void *address);
    bool BindPolicy(void *address, size_t size);
    void GetPagePlacement(void *address);
};

void ShowDataPlacementPolicy::GetPagePlacement(void *address){
    int id = array_id[address];
    //fout << "GetPagePlacement : " << address << " " << id << "\n";
    int s = NumPages(array_sizes[id]);
    address = PageBase(address);
    void* p[s];
    for(int i = 0; i < s; i++)
        p[i] = (char*)address + i*page_size;
    move_pages(0, s, p, NULL, page_nodes[id]-s, MPOL_MF_MOVE);
}

ShowDataPlacementPolicy::ShowDataPlacementPolicy(int _tid){
    _DataBindPolicy_Ready = STOP;

    tid = _tid;
    free_id = 0;
    total_pages = 0;
    page_nodes_containers = new int[1<<20]; //tract up to 1M pages, may overflow!!!!
    fill(page_nodes_containers, page_nodes_containers+(1<<20), -100);
    mchunk_size = stoi(Configure[ConfigureChunkSize]);
    string fbase;
    if(Configure.Contains("ShowDataPlacementPolicy.Place"))
        fbase = Configure["ShowDataPlacementPolicy.Place"] + "." + to_string(tid);
    else
        fbase = "stdout";
    fout.open(fbase);
    assert(fout.good());

    _DataBindPolicy_Ready=USING;
}

ShowDataPlacementPolicy::~ShowDataPlacementPolicy(){
    for(auto a : array_id){
        GetPagePlacement(a.first);
    }
    for(int i = 0, pos = 0; i < page_nodes.size(); i++){
        int s = NumPages(array_sizes[i]);
        fout << s;
        while(s--)
            fout << " " << page_nodes_containers[pos++];
        fout << endl;
    }
    fout.close();
}

void ShowDataPlacementPolicy::RemoveData(void *address){
    if(array_id.find(address) != array_id.end()){
        GetPagePlacement(address);
        array_id.erase(address);
    }
}

bool ShowDataPlacementPolicy::BindPolicy(void *address, size_t size){
    if(size >= mchunk_size){
        //fout << "Malloc : " << address << " " << size << endl;

        int id = page_nodes.size(); 
        int s = NumPages(size);
        total_pages += s;
        assert(total_pages < (1<<20) && "Too many pages, change the size of page_nodes_containers");

        array_id[address] = id;
        array_sizes.push_back(size);

        if(id == 0)
            page_nodes.push_back(page_nodes_containers+s);
        else
            page_nodes.push_back(page_nodes.back()+s);
    }
    return false;
}

class ShowDataPlacementFactory : public _DataBindFactory{
    private:
    atomic<int> tid;
    public:
    ShowDataPlacementFactory(){
        tid = 0;
    }
    _DataBindPolicy *GetPolicy(){
        return new ShowDataPlacementPolicy(tid++);
    }
};

ShowDataPlacementFactory DataBindFactory;
