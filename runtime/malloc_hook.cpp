#include <stdio.h>
#include <malloc.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pthread.h>
#include <stdint.h>
#include <inttypes.h>
#include <iostream>
#include <numa.h>
#include <numaif.h>
#include <dlfcn.h>
#include <thread>
#include <mutex>
#include <queue>

#include <typeinfo>

#include "Configure.h"
#include "DataBind.h"

using namespace std;
/*
 * LD_PRELOAD=./libmalloc_hook.so
 */

static _DataBindFactory *DataBindFactory = NULL;
thread_local _DataBindPolicy* DataBindPolicy = NULL;
static mutex instances_mutex;
static queue<_DataBindPolicy*> instances;

static class _DataBindFactoryHelper{
public:
    _DataBindFactoryHelper(){ 
        printf("DataBind Helper Init\n");
        void *handler = dlopen(Configure["DataBindPolicy"].c_str(), RTLD_LAZY);
        char *err = dlerror();
        if(err) cout << "dlopen error : " << err << endl;
        char **PolicyName = (char**)dlsym(handler, "DataBindPolicyName");
        cout << Info_Prefix << "Data Bind Policy : " << *PolicyName << endl;
        DataBindFactory = (_DataBindFactory*)dlsym(handler, "DataBindFactory");
        assert(DataBindFactory && "Load DataBindFactory fail");
        //DataBindPolicy = DataBindFactory->GetPolicy();
        //assert(DataBindPolicy && "Load DataBindPolicy fail");
    }
    ~_DataBindFactoryHelper(){   
        DataBindPolicy = NULL;
        while(instances.size()){
            //auto x = instances.front();
            //cout << typeid(x).name() << endl;
            delete instances.front();
            instances.pop(); 
        }
        
    }
}DataBindFactoryHelper;


extern "C" void *__libc_malloc(size_t size);
extern "C" void __libc_free(void *p);
extern "C" void *__libc_calloc(size_t nitems, size_t size);
extern "C" void *__libc_realloc(void *p, size_t size);


extern "C" void* malloc(size_t size){
    //void *caller = __builtin_return_address(0);
    void *rt = __libc_malloc(size);
    uint64_t tid = pthread_self();
    if(_DataBindPolicy_Ready == INIT && DataBindPolicy == NULL && DataBindFactory != NULL){

        _DataBindPolicy_Ready = READY;
        DataBindPolicy = DataBindFactory->GetPolicy();
        {
            lock_guard<mutex> lock(instances_mutex);
            instances.push(DataBindPolicy);
        }
    }
    
    if(_DataBindPolicy_Ready == USING)
        DataBindPolicy->AddData(rt, size);
      
    return rt;
}

extern "C" void free(void *p){
    uint64_t tid = pthread_self();
    void *caller = __builtin_return_address(0);
    //printf("free %" PRIu64 " %p %p\n", tid, p, caller);
    if(DataBindPolicy != NULL){
        DataBindPolicy->RemoveData(p);
    }
    __libc_free(p);
}

extern "C" void* calloc(size_t nitems, size_t size){
    void *caller = __builtin_return_address(0);
    void *rt = __libc_calloc(nitems, size);
    uint64_t tid = pthread_self();
    //printf("calloc %" PRIu64 " %zu %zu %p %p\n", nitems, tid, size, rt, caller);
    return rt;
}

extern "C" void* realloc(void *p, size_t size){
    void *caller = __builtin_return_address(0);
    void *rt = __libc_realloc(p, size);
    uint64_t tid = pthread_self();
    //printf("realloc %" PRIu64 " %p %zu %p %p\n", tid, p, size, rt, caller);
    return rt;
}




