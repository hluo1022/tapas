/*
 * Randomly bind threads to spare cores
 * When there is no spare core, bind to an random core
 */

#include <string>
#include <random>
#include <string.h>

#include "ThreadBind.h"

using namespace std;

const char *ThreadBindPolicyName = "Random Thread Bind";

class RandomThreadBind : public _ThreadBindPolicy{
private:
    random_device rd;
    mt19937 *gen;
    uniform_int_distribution<> dis;
    cpu_set_t *allocated;
    bool verbose;
public:
    cpu_set_t *BindThread(pthread_t tid);
    void RemoveThread(pthread_t tid);
    RandomThreadBind();
};

RandomThreadBind::RandomThreadBind(){
    gen = new mt19937(rd());
    allocated = CPU_ALLOC(num_cpus);
    CPU_ZERO_S(cpu_set_size, allocated);
    verbose = false;
    if(Configure.Contains("RandomThreadBindPolicy.Verbose"))
        verbose = Configure["RandomThreadBindPolicy.Verbose"] == "True";
}
//Bind an incoming thread to an spare core.
cpu_set_t *RandomThreadBind::BindThread(pthread_t tid){
    int c = CPU_COUNT_S(cpu_set_size, allocated);
    cpu_set_t *rt = CPU_ALLOC(num_cpus);
    CPU_ZERO_S(cpu_set_size, rt);
    int target = dis(*gen) % num_cpus;
    if(c != num_cpus){
        while(CPU_ISSET_S(target, cpu_set_size, allocated))
            target = dis(*gen) % num_cpus;
    }
    if(verbose)
        cout << "Bind thread " << tid << " on processor #" << target/hthread<< endl;
    target /= hthread;
    target *= hthread;
    for(int i = 0; i < hthread; i++){
        CPU_SET_S(target+i, cpu_set_size, rt);
        CPU_SET_S(target+i, cpu_set_size, allocated);
    }
    return rt;
}

void RandomThreadBind::RemoveThread(pthread_t tid){
    int id = tidmap[tid];
    //Do something
    cpu_set_t *tmp = cpu_sets[id];
    CPU_XOR_S(cpu_set_size, allocated, allocated, tmp);
    _ThreadBindPolicy::RemoveThread(tid);   
}

class RandomFactory : public _ThreadBindFactory{
public:
    _ThreadBindPolicy *GetPolicy(){
        return new RandomThreadBind();
    }
};

RandomFactory ThreadBindFactory;

