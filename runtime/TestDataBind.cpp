#include <iostream>
#include <vector>
#include <atomic>
#include <random>

#include <numa.h>

#include "Configure.h"
#include "DataBind.h"

using namespace std;

const char *DataBindPolicyName = "Test Data Bind";

class TestDataBind : public _DataBindPolicy{
private:
public:
    TestDataBind();
    ~TestDataBind();
    void AddData(void *address, size_t size);
    void RemoveData(void *address);
};

TestDataBind::TestDataBind(){
    _DataBindPolicy_Ready=2;
}

TestDataBind::~TestDataBind(){
}

void TestDataBind::AddData(void *address, size_t size){
    cout << "Malloc : " << address << " " << size << endl;
}

void TestDataBind::RemoveData(void *address){
    cout << "Free : " << address << endl;
}

class TestDataBindFactory : public _DataBindFactory{
public:
    _DataBindPolicy *GetPolicy(){
        return new TestDataBind();
    }
};

TestDataBindFactory DataBindFactory;
