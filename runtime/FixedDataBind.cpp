#include <iostream>
#include <vector>
#include <atomic>
#include <random>
#include <map>
#include <algorithm>

#include <numa.h>
#include <numaif.h>

#include <thread>
#include <mutex>

#include "Configure.h"
#include "DataBind.h"

using namespace std;

const char *DataBindPolicyName = "Fix Data Bind According to Placement Files";

static mutex output_mutex;

class FixDataBindPolicy : public _DataBindPolicy{
private:
    int tid;
    int mcnt;
    int pcnodes;
    vector<int> nchunks;
    vector<int> cnodes;
    size_t mchunk_size;
public:
    FixDataBindPolicy(int _tid);
    ~FixDataBindPolicy();
    void RemoveData(void *address);
    bool BindPolicy(void *address, size_t size);
    int GetID(void *address);
};

FixDataBindPolicy::FixDataBindPolicy(int _tid){
    tid = _tid;
    mcnt = 0;
    pcnodes = 0;
    mchunk_size = stoi(Configure[ConfigureChunkSize]);
    string fbase = Configure["FixedDataBindPolicy.Place"] + "." + to_string(tid);
    ifstream fin(fbase);
    assert(fin.good());
    int nck;
    while(fin >> nck){
        nchunks.push_back(nck);
        for(int i = 0; i < nck; i++){
            int x;
            fin >> x;
            cnodes.push_back(x);
        }
    }
    fin.close();

    _DataBindPolicy_Ready=USING;
}

FixDataBindPolicy::~FixDataBindPolicy(){
}

void FixDataBindPolicy::RemoveData(void *address){
}

bool FixDataBindPolicy::BindPolicy(void *address, size_t size){
    if(size < mchunk_size) return false;

    bind_policy.clear();
    bind_address.clear();
    bind_len.clear();

    address = PageBase(address);
    
    for(int i = 0; i < nchunks[mcnt]; i++){
        bind_address.push_back((char*)address + i*mchunk_size);
        bind_len.push_back(mchunk_size);
    }
    for(int i = 0; i < nchunks[mcnt]; i++){
        if(cnodes[pcnodes] < 0)
            bind_policy.push_back(~(nodemask)0);
        else
            bind_policy.push_back(1<<cnodes[pcnodes++]);
    }
    mcnt++;

    return true;
}

class FixDataBindFactory : public _DataBindFactory{
private:
    atomic<int> tid;
public:
    FixDataBindFactory(){
        tid = 0;
    }
    _DataBindPolicy *GetPolicy(){
        return new FixDataBindPolicy(tid++);
    }
};

FixDataBindFactory DataBindFactory;
