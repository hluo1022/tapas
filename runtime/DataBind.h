#pragma once

#include <iostream>
#include <vector>
#include <atomic>
#include <map>
#include <stdint.h>
#include <set>

#include <numa.h>
#include <numaif.h>

#include <unistd.h>

#include "Configure.h"

using namespace std;

enum DataBindStat {INIT = 0, READY, USING, STOP};

thread_local int _DataBindPolicy_Ready = INIT;

class _DataBindPolicy{
protected:
    typedef unsigned long nodemask;
    const string ConfigureChunkSize = "DataBindPolicy.ChunkSize";
    map<void*, int> array_id; 
    struct bitmask *node_mask;
    vector<int> node_avai;
    set<pthread_t> skipped;
    void* PageBase(void* addr);
    int NumPages(size_t sz);

    vector<nodemask> bind_policy;
    vector<void*> bind_address;
    vector<size_t> bind_len;

public:
    size_t page_size;
    int nnode_max;
    
    _DataBindPolicy();
    virtual ~_DataBindPolicy();
    virtual void AddData(void *address, size_t size);
    virtual void RemoveData(void *address);
    virtual bool BindPolicy(void *address, size_t size);
    virtual bool IsSkipped(pthread_t tid);
};

void _DataBindPolicy::RemoveData(void *address){
};

bool _DataBindPolicy::BindPolicy(void *address, size_t size){
    return false;
}

void _DataBindPolicy::AddData(void *address, size_t size){
    _DataBindPolicy_Ready = STOP;

    if(BindPolicy(address, size)){
        
        for(int i = 0; i < bind_address.size(); i++){
            int x = mbind(bind_address[i], bind_len[i], MPOL_BIND, &(bind_policy[i]), sizeof(unsigned long)+1/*DataBindPolicy->nnode_max*/, MPOL_MF_MOVE);
            if(x){
                printf("error : %d\n", x);
                perror(NULL);
                printf("%p %zu\n", address, size);
                assert(0);
            }
        }
    }

    _DataBindPolicy_Ready = USING;
}

int _DataBindPolicy::NumPages(size_t size){
    return (size+page_size-1)/page_size;
}

void* _DataBindPolicy::PageBase(void* addr){
    uint64_t a = (uint64_t)addr;
    a ^= (a & (page_size-1));
    return (void*)a;
}

_DataBindPolicy::_DataBindPolicy(){
    page_size = getpagesize();
    node_mask = numa_get_mems_allowed();   
    nnode_max = numa_max_node();
    for(int i = 0; i < nnode_max; i++){
        if(numa_bitmask_isbitset(node_mask, i))
            node_avai.push_back(i);
    }
}

bool _DataBindPolicy::IsSkipped(pthread_t tid){
    return skipped.find(tid) != skipped.end();
}

_DataBindPolicy::~_DataBindPolicy(){
}

class _DataBindFactory{
public:
    virtual _DataBindPolicy* GetPolicy() = 0;
    virtual ~_DataBindFactory() { }
};


