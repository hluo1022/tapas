#pragma once

#include <iostream>
#include <map>
#include <pthread.h>
#include <atomic>
#include <vector>
#include <string.h>
#include <sched.h>

#include <unistd.h>

#include "Configure.h"

using namespace std;

//hyperthreading disabled or not
#ifndef __HT_Disabled__
    const int hthread = 1;
#else
    #ifdef __PPC64__
        const int hthread = 8;
    #else
        const int hthread = 2;
    #endif
#endif


class _ThreadBindPolicy{
protected:
    map<pthread_t, int> tidmap; // mapping tid with the order
    atomic<int> thread_stamp;
    vector<cpu_set_t*> cpu_sets; 
    vector<pthread_attr_t*> thread_attrs;
    int num_cpus;
    int cpu_set_size;
public:
    _ThreadBindPolicy();
    virtual void AddThread(pthread_t tid, const pthread_attr_t *attr);
    virtual void RemoveThread(pthread_t tid);
    virtual cpu_set_t *BindThread(pthread_t tid);
    virtual ~_ThreadBindPolicy();
};


_ThreadBindPolicy::_ThreadBindPolicy(){
    thread_stamp = 0;
    num_cpus = sysconf(_SC_NPROCESSORS_ONLN) * hthread; //in case of ht is disabled
    cpu_set_size = CPU_ALLOC_SIZE(num_cpus);
}

_ThreadBindPolicy::~_ThreadBindPolicy(){
}

void _ThreadBindPolicy::AddThread(pthread_t tid, const pthread_attr_t *attr = NULL){
    int ts = thread_stamp++;
    tidmap[tid] = ts;
    cpu_sets.push_back(NULL);
    thread_attrs.push_back(NULL);

    if(attr != NULL){
        thread_attrs[ts] = new pthread_attr_t;
        memcpy(thread_attrs[ts], attr, sizeof(pthread_attr_t));
    }
    cpu_sets[ts] = BindThread(tid);
    if(cpu_sets[ts] != NULL){
        int s = pthread_setaffinity_np(tid, sizeof(cpu_set_t), cpu_sets[ts]);
        assert(s==0 && "set affinity error");
    }
}

void _ThreadBindPolicy::RemoveThread(pthread_t tid){
    tidmap.erase(tidmap.find(tid));
}

//The Policy Function
cpu_set_t *_ThreadBindPolicy::BindThread(pthread_t tid){
    //do nothing while using the default policy
    tid++; //make compiler happy
    return NULL;
}

class _ThreadBindFactory{
public:
    virtual _ThreadBindPolicy *GetPolicy() = 0;
    virtual ~_ThreadBindFactory() {};
};

