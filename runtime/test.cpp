#include <iostream>
#include <cstring>
#include <string.h>
#include <cstdlib>
#include <sys/types.h>
#include <pthread.h>
#include <thread>
using namespace std;

void f(int n){
    cout << pthread_self() << endl;
    int *a = NULL;
    for(int i = 0; i < n; i++){
        a = (int*)malloc(90000);
        for(int j = 0; j < 100; j++)
            a[j] = j;
        free(a);
    }
}

int main(){
    cout << "Program Begin" << endl;
    thread t1(f, 100);
    thread t2(f, 100);
    t1.join();
    t2.join();
    cout << "Program End" << endl;
    return 0;

}
