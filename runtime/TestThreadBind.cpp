/*
 * Print information when threads are created or join.
 * Used for test
 */
#include <string>
#include <random>
#include <string.h>

#include "ThreadBind.h"

using namespace std;

const char *ThreadBindPolicyName = "Print when threads are created or joined";

class TestThreadBind : public _ThreadBindPolicy{
private:
public:
    virtual cpu_set_t *BindThread(pthread_t tid);
    virtual void RemoveThread(pthread_t tid);
    virtual void AddThread(pthread_t tid, const pthread_attr_t *attr);
    TestThreadBind();
    ~TestThreadBind();
};

TestThreadBind::TestThreadBind(){
    cout << Info_Prefix << "TestThreadBind Constructor" << endl;
}

TestThreadBind::~TestThreadBind(){
    cout << Info_Prefix << "Number of thread created : " << thread_stamp << endl;
}

//Bind an incoming thread to an spare core.
cpu_set_t *TestThreadBind::BindThread(pthread_t tid){
    tid++; //make compiler happy
    return NULL;
}

void TestThreadBind::RemoveThread(pthread_t tid){
    cout << Info_Prefix << "Join " << tid << endl;
    _ThreadBindPolicy::RemoveThread(tid);   
}

void TestThreadBind::AddThread(pthread_t tid, const pthread_attr_t *attr){
    cout << Info_Prefix << "Create " << tid << endl;
    _ThreadBindPolicy::AddThread(tid, attr);
}

class TestFactory : public _ThreadBindFactory{
public:
    _ThreadBindPolicy *GetPolicy(){
        return new TestThreadBind();
    }
};

TestFactory ThreadBindFactory;

