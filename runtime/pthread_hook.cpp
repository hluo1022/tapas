#include <pthread.h>
#include <stdio.h>
#include <dlfcn.h>
#include <iostream>
#include "ThreadBind.h"
#include "Configure.h"
using namespace std;

//_Configure Configure(default_config); // must be allocated once

_ThreadBindFactory *ThreadBindFactory = NULL;
_ThreadBindPolicy *ThreadBindPolicy = NULL;

static class _Deconstructor{
public:
    ~_Deconstructor(){
        delete ThreadBindPolicy;
        //delete ThreadBindFactory;
    }
}Deconstructor;

void load_threadbind_policy(){
    void *handle = dlopen(Configure["ThreadBindPolicy"].c_str(), RTLD_LAZY);
    char *err = dlerror();
    if(err) cout << "dlopen error : " << err << endl;
    char **PolicyName = (char**)dlsym(handle, "ThreadBindPolicyName");
    cout << Info_Prefix << "Thread Bind Policy : " << *PolicyName << endl;
    ThreadBindFactory = (_ThreadBindFactory*)dlsym(handle, "ThreadBindFactory");
    assert(ThreadBindFactory && "Load ThreadBindFactory fail");
    ThreadBindPolicy = ThreadBindFactory->GetPolicy();
    assert(ThreadBindPolicy && "Load ThreadBindPolicy fail");
}

int (*original_pthread_create)(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg) = NULL;
int (*original_pthread_join)(pthread_t thread, void **retval) = NULL;

void load_original_pthread(const char *funcname, void **func) {
    //In Ubuntu, libpthread.so is a text file, can not be linked
    void *handle = dlopen("libpthread.so.0", RTLD_LAZY);
    char *err = dlerror();
    if (err) {
        printf("Open Error : %s\n", err);
    }
    *func = dlsym(handle, funcname);
    err = dlerror();
    if (err) {
        printf("%s\n", err);
    }

    //Load Policy Hook
    if(ThreadBindPolicy == NULL){
        load_threadbind_policy();
        
        if(Configure["ThreadBindPolicy.IgnoreMainThread"] == "True"){
        }
        else{
            pthread_t tmp = pthread_self();
            ThreadBindPolicy->AddThread(tmp, NULL);
        }
    }
}

int pthread_create(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg) {
    if (original_pthread_create == NULL) {
        load_original_pthread("pthread_create", (void**)&original_pthread_create);
    }
    int rt = original_pthread_create(thread, attr, start_routine, arg);
    //printf("Thread %llu created\n", *thread);
    ThreadBindPolicy->AddThread(*thread, attr);
    return rt;
}

int pthread_join(pthread_t thread, void **retval){
    if(original_pthread_join == NULL){
        load_original_pthread("pthread_join", (void**)&original_pthread_join);
    }
    //printf("JOIN thread %llu\n", thread);
    //cout << "Join thread " << thread << endl;
    ThreadBindPolicy->RemoveThread(thread);
    return original_pthread_join(thread, retval);
}
