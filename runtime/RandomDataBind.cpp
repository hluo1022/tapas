#include <iostream>
#include <vector>
#include <atomic>
#include <random>

#include <numa.h>

#include "Configure.h"
#include "DataBind.h"

using namespace std;

const char *DataBindPolicyName = "Random Data Bind";

class RandomDataBind : public _DataBindPolicy{
private:
    atomic<int> array_count;
    size_t csize;

    random_device rd;
    mt19937 *gen;
    uniform_int_distribution<> dis;

public:
    RandomDataBind();
    ~RandomDataBind();
    bool BindPolicy(void *address, size_t size);
    void CheckEnvironment();
};

void RandomDataBind::CheckEnvironment(){
    if(!Configure.Contains(ConfigureChunkSize))
        cout << Info_Prefix << "Didn't find \"" << ConfigureChunkSize << "\". Use default value : " << csize << endl;
    else
        csize = stoi(Configure[ConfigureChunkSize]);
}

RandomDataBind::RandomDataBind(){
    array_count = 0;
    csize = page_size;
    gen = new mt19937(rd());
    CheckEnvironment();
    _DataBindPolicy_Ready = 2;
}

RandomDataBind::~RandomDataBind(){
    _DataBindPolicy_Ready = 1;
    cout << "Number of array allocated : " << array_count << endl;
}

bool RandomDataBind::BindPolicy(void *address, size_t size){
    if(size < csize) return false; //skip the small array
    
    int id = array_count++;
    //printf("%d %d %lld\n", id, size, tid);

    address = PageBase(address);
    int nchunk = (size+csize-1)/csize;

    bind_address.clear();
    bind_len.clear();
    bind_policy.clear();
    
    char *address_pos = (char*)address;
    for(int i = 0; i < nchunk; i++){
        bind_address.push_back(address_pos);
        bind_len.push_back(csize);
        address_pos += csize;
    }
    
    int nnode_avail = node_avai.size();
    for(int i = 0; i < nchunk; i++){
        nodemask x = 1;
        x <<= node_avai[(dis(*gen) % nnode_avail)];
        bind_policy.push_back(x);
    }
    return true;
}

class RandomDataBindFactory : public _DataBindFactory{
public:
    _DataBindPolicy *GetPolicy(){
        return new RandomDataBind();
    }
    ~RandomDataBindFactory(){}
};

RandomDataBindFactory DataBindFactory;
