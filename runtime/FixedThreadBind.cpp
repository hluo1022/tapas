#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <ctype.h>

#include "ThreadBind.h"
#include "Configure.h"
using namespace std;

const char *ThreadBindPolicyName = "Bind threads according to the plan file";

class FixedThreadBind : public _ThreadBindPolicy{
private:
    int GetNumber(string &str, size_t &pos);
    int NumThreads;
    bool verbose;
public:
    FixedThreadBind();
    virtual cpu_set_t *BindThread(pthread_t tid);
};

int FixedThreadBind::GetNumber(string &str, size_t &pos){
    if(pos == str.length() || !isdigit(str[pos]))
        return -1;
    int a = 0;
    while(isdigit(str[pos]))
        a = a*10 + str[pos++] - '0';
    return a;
}
FixedThreadBind::FixedThreadBind(){
    NumThreads = 0;
    verbose = false;
    if(Configure.Contains("FixedThreadBindPolicy.Verbose"))
        verbose = Configure["FixedThreadBindPolicy.Verbose"] == "True";
    string fname = Configure["FixedThreadBindPolicy.Plan"];
    ifstream fin(fname);
    assert(fin.good() && "[FixedThreadBind] Open plan file failed");
    string x;
    while(fin >> x){
        NumThreads++;
        cpu_set_t *rt = CPU_ALLOC(num_cpus);
        CPU_ZERO_S(cpu_set_size, rt);
        
        size_t pos = 0;
        int a = 0;
        while((a=GetNumber(x,pos)) != -1){
            if(pos == x.length()){
                CPU_SET_S(a*hthread, cpu_set_size, rt);
                break;
            }
            if(x[pos] == ',')
                CPU_SET_S(a*hthread, cpu_set_size, rt);
            else if(x[pos] == '-'){
                int b = GetNumber(x,++pos);
                for(; a <= b; a++)
                    CPU_SET_S(a*hthread,cpu_set_size, rt);
            }
            else
                assert(0 && "Unrecongnized formation");

            pos++;
        }
        cpu_sets.push_back(rt);
    }
}

cpu_set_t *FixedThreadBind::BindThread(pthread_t tid){
    
    int id = tidmap[tid];
    assert(id < NumThreads && "Number of threads in plan file is less than threads in real program");
    cpu_set_t *rt = cpu_sets[id];
    
    if(verbose){
        cout << "Thread #" << id << " : ";

        for(int i = 0; i < num_cpus; i++){
            if(CPU_ISSET_S(i, cpu_set_size, rt))
                cout << i/hthread << " ";
        }
        cout << endl;
    }
    assert(rt != NULL && "No plan for this thread");    
    return cpu_sets[id];
}

class FixThreadBindFactory : public _ThreadBindFactory{
public:
    _ThreadBindPolicy *GetPolicy(){
        return new FixedThreadBind();
    }
};

FixThreadBindFactory ThreadBindFactory;
