/*
 *  for further work: use this class with a wrapper.
 *  The class will be called in the wrapper.
 *  The wrapper will take parameters, such as the configuration files, and will pass it to configuration class.
    The wrapper will call the programs.
 */

#pragma once

#include <string>
#include <fstream>
#include <map>
#include <assert.h>
#include <iostream>
using namespace std;

//const string default_config = "../../runtime/config.txt";
const string default_config = "/u/cye/tapas/runtime/config.txt";
const char *Info_Prefix = "[TAPAS] ";

class _Configure{
private:
    map<string, string> config;
public:
    /* Configures */

    void Parser(ifstream &fin){
        string a;
        string b;
        string c;
        while(fin >> a >> b >> c){
            assert(b == "=" && "format error");
            config[a] = c;
        }
    }

    _Configure(string file = "config.txt"){
        ifstream fin(file);
        assert(fin.good() && "open config.txt fail");
        Parser(fin);
        fin.close();
    }

    string operator[](string x){
        return config[x];
    }
    
    bool Contains(string x){
        return config.find(x) != config.end();
    }
};

extern _Configure Configure;


