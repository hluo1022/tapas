# README #

Tapas is an optimization framework for thread and data placement on non-uniform memory accesses (NUMA) systems. Tapas is developed by Hao Luo at University of Rochester Computer Science Department.

### Introduction to Tapas ###

* Tapas is a performance tuning framework that optimizes the task and data placement on NUMA systems. It builds on the state-of-the-art locality analysis for parallel programs and analyzes the system's memory bandwidth usage. Tapas has a very simple user interface. Programmers only need to specify a set of memory regions for Tapas in their program. Tapas will profile the program's execution and determine a placement of the tasks and the memory regions to achieve optimal memory performance. Tapas has lightweight profiling, its runtime overhead for profiling is less than 4%. Tapas gives guarantees on the quality of generated placement due to its use of exhaustive search. Tapas' performance model was not built on the statistics obtained from modern hardware counters, therefore it is resilient to the errors caused by hardware counter usage.
 
* Tapas is currently in experimental stage. Its interfaces are still very limited. However, preliminary experimental results show that using Tapas, **1.78X** speedup can be achieved for modern high-performance memory intensive parallel applications. Only 10 lines of code are needed for profiling and speedups can be achieved within 10 minutes of modeling.   

### Setup ###

* Setting up Tapas is easy. Tapas uses CMake, which makes its building procedure cross-platform. We deliberately include very few 3rd party packages for Tapas dependence to maintain portability. The only dependence of Tapas is numactl package for memory placement (http://oss.sgi.com/projects/libnuma/).

* To compile, make a new directory for out-of-source build. Within the directory, use cmake to build. 

### Usage ###

* Tapas is a profile-guided optimization framework. It consists of three procedures: profiling, modeling and optimization. 

  - Before profile, first add the APIs provided in [Tapas_Source_Dir]/interface to the source code for preparation of profiling. The instruction of using these APIs can be found under *interface* directory.

  - Profile: To profile, first run

    1. **[Tapas_Build_Dir]/bin/GenTrace**

          * This command generates two tools, PerfGenTrace and PinGenTrace, in current directory. Either one can be used to collect memory traces. PerfGenTrace uses Intel's PEBS sampling facility to collect trace. It has very low overhead. PinGenTrace uses Intel's Pin binary instrumentation tool to collect trace. Its overhead is high. We illutrate the procedure of using PerfGenTrace to collect memory traces below.


    2. **./PerfGenTrace [workload]**

          * This command will collect memory traces for the workload. Once the workload finishes execution, a perf.data file is generated which stores the raw binary format of the sampled memory trace. Immediately followed the program completion, a perf script will be executed to extract the memory addresses from perf.data and translate the raw memory addresses to a format recognizable by Tapas. The translated memory trace files are named as trace.[pid].[tid]

  - Model: The model component of Tapas has two parts, trace analysis and optimal placement search engine.

    1. **./bin/TraceProfile [Options]**

        * TraceProfile takes a set of memory traces as input and builds a locality summary to these traces. This summary can be used in the optimal placement search engine. 


    2. **./bin/OptDataPlace [Options]**

        * OptDataPlace searches through the whole space of all possible thread/data placements and try to find the optimal placement. Users can customize the search procedure by modifying the OptDataPlace driver. This procedure will dump the generated optimized data/thread placement into text file, which can be used to guide specific data and thread placement.

  - Optimize: To optimize, user of Tapas needs to insert the relevant APIs in source code to enforce placement. Tapas provides such APIs in the interface directory. More detailed instruction of using these APIs can be found in that directory.

### File Organization ###

* bin: Frontend of Tapas, including binary tools used in profiling and modeling.

* lib: Backend of Tapas, including the locality model and bandwidth model.

* interface: The APIs used to profile applications and guide thread/memory placement.

* include: Header files.

* other: Miscelaneous stuff, including 1) a python interface and 2) the tracer.

* tests: Test files.

* example: Examples to demo Tapas.

### Contact ###

* Should you have any question, please contact Hao Luo (hluo [at] cs.rochester.edu)

### TODO ###

* Cache the modeled result for each target memory region

* Model memory page replication

* Parallelize the procedure of searching for optimality

* employ a graph partitioning technique to clustering memory or data for reducing overhead

### Documentation ###
doxygen doxygen-config

### Input/Output ###
Traces : xxxx.pid.tid, the traces of profiled program
TraceProfile : xxx.footprint, the footprint of program
                xxx.traffic, the traffic over program

