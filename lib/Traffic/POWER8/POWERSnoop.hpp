#pragma once 

#include <unordered_map>
#include <bitset>
#include <map>
#include <vector>
#include <fstream>
#include <iostream>
#include <cstdint>

#include "general.hpp"
#include "Common/Common.hpp"
#include "Common/Histo.hpp"

using namespace std;
using namespace histo;
namespace Tapas {

//<! per memory range structure
class POWERSnoopState{
friend class POWERSnoopBuilder;
protected:
    map<uint64_t, int> idx; //<! the id of a thread combination(in mask)

    //<! for each (thread, thread combination) pair, counting the filltime
    vector<vector<Histogram<uint64_t>>> rdtimes;
    vector<vector<Histogram<uint64_t>>> wrtimes; //<! as rdtimes, count writes
    vector<map<AddrInt, uint64_t>> writes;
    vector<vector<int>> comb;

public:
    POWERSnoopState();
    
    //<! total number of memory writes
    uint64_t getTotalMemoryTraffic() ;
    uint64_t getMemoryTraffic(NodeType n, DataPlacement &dp);
    //<! total number of write traffics from srcThread to a chip(threads in a mask)
    uint64_t getWriteChipTraffic(NodeType srcThread, uint64_t chipmask, uint64_t filltime);
    //<! total number of read traffics from srcThread to a chip
    uint64_t getReadChipTraffic(NodeType srcThread, uint64_t chipmask, uint64_t filltime);

    //<! traffic on all of the links and nodes
    void getInterconnectTraffic(NetworkType<uint64_t, uint64_t> &out, vector<uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config);
    void getInterconnectAccess(NetworkType<uint64_t, uint64_t> &out, vector<uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config);
    
    void print(std::ostream &out) ;
    void serialize(std::ostream &OS);
    static POWERSnoopState* deserialize(std::istream &IS);
};

// ==================================== //
class POWERSnoopBuilder {
private:
    POWERSnoopState *TS;
    MemRange *range;
    unordered_map<AddrInt, vector<TimeInt>> previous;
private:
    LinkType makelink(uint32_t src, uint32_t dst);

public:

    POWERSnoopBuilder(POWERSnoopState *_ts, MemRange *_range, int nThreads, int kThreadsPerChip);

    void onReference(AddrInt addr, uint32_t accessor, TimeInt tick, bool type);

    void finish() { cout << previous.size() << endl;}
    void serialize(ostream &os) {TS->serialize(os);}
};

} // namespace Tapas
