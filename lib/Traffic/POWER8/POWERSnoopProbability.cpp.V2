#include "POWERSnoopProbability.hpp"
#include <cassert>
#include <iostream>
#include <cstdint>

using namespace std;

namespace Tapas {

static uint64_t cstep = 0;

void POWERSnoopProbabilityBuilder::onReference(AddrInt addr, NodeType accessor, TimeInt tick, bool type) {
    if(cstep % 1000000 == 0)
        cout << cstep/1000000 << endl;
    cstep++;

    vector<TimeInt> times(nThreads, TimeMax);
    int lastAccessor = -1;
    for(int i = 0; i < nThreads; i++){
        if(previous[i].find(addr) != previous[i].end()){
            times[i] = previous[i][addr];
            if(lastAccessor == -1) lastAccessor = i;
            else lastAccessor = times[i] > times[lastAccessor] ? i : lastAccessor;
        }
    }
    for(int i = 0; i < nThreads; i++)
        times[i] = sublog_value_to_index(tick-times[i]);
    times.push_back(MAX_WINDOW-1);

    vector<size_t> idx(times.size());
    iota(idx.begin(), idx.end(), 0);
    sort(idx.begin(), idx.end(), [&times](size_t i, size_t j){return times[i] < times[j];});
    
    cps[accessor][0][0][0]++;
    cps[accessor][0][0][times[idx[0]]]--;
    #pragma omp parallel for schedule(auto)
    for(int i = 0; i < nThreads; i++){
        if(times[idx[i]] == times[idx[i+1]])
            continue;
        for(int j = 0; j <= i; j++){
            cps[accessor][i+1][idx[j]][times[idx[i]]]++;
            cps[accessor][i+1][idx[j]][times[idx[i+1]]]--;
        }
    }

    if(type == MEMWRITE){
        for(int i = 0; i < nThreads; i++){
            if(previous[i].find(addr) != previous[i].end())
                previous[i].erase(addr);
        }
    }
    previous[accessor][addr] = tick;
}

void POWERSnoopProbabilityBuilder::finish(){
    TS->copies.resize(extents[nThreads][nThreads+1][nThreads][MAX_WINDOW]);
    auto &copies = TS->copies;

    #pragma omp parallel for schedule(auto)
    for(int i = 0; i < nThreads; i++)
        for(int j = 0; j <= nThreads; j++)
            for(int k = 0; k < nThreads; k++){
                copies[i][j][k][0] = cps[i][j][k][0];
                for(int l = 1; l < MAX_WINDOW; l++){
                    copies[i][j][k][l] = cps[i][j][k][l] + copies[i][j][k][l-1];
                }
            }
}

POWERSnoopProbabilityState::POWERSnoopProbabilityState(){
}


uint64_t POWERSnoopProbabilityState::getTotalMemoryTraffic(){ 
    return 0;
}

uint64_t POWERSnoopProbabilityState::getMemoryTraffic(NodeType n, DataPlacement &dp){
    return 0;
}

uint64_t POWERSnoopProbabilityState::getWriteChipTraffic(NodeType srcThread, uint64_t chipmask, uint64_t filltime){
    return 0;
}

uint64_t POWERSnoopProbabilityState::getReadChipTraffic(NodeType srcThread, uint64_t chipmask, uint64_t filltime){
    return 0;
}

void POWERSnoopProbabilityState::getInterconnectAccess(NetworkType<uint64_t, uint64_t> &out, vector<uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config){
    //4 kinds of accesses : in-chip, in-dcm, global, miss
    vector<vector<double>> numaccess(config.ncores, vector<double>(4, 0.0));
    vector<int> chipmask(config.nchips, 0);

    for(int i = 0; i < config.ncores; i++)
        chipmask[config.Core2Chip[i]] |= (1<<tp.Core2Thread[i]);
    
    #pragma omp parallel for
    for(int i = 0; i < config.ncores; i++){ //accessor
        NodeType core = tp.Thread2Core[i];
        NodeType chip = config.Core2Chip[core];
        NodeType dcm = config.Chip2DCM[chip];
        uint64_t ft = sublog_value_to_index(filltime[chip]);

        for(int j = 1; j <= config.ncores; j++){ //number of copies
            uint64_t nreuses = 0;
            for(int k = 0; k < config.ncores; k++){ //all of the reuses
                nreuses += copies[i][j][k][ft];
            }
            if(nreuses == 0) continue;
            nreuses /= j;
            vector<double> proba(config.ncores, 0);
            for(int k = 0; k < config.ncores; k++)
                proba[k] = copies[i][j][k][ft] / (double)nreuses;

            double p = 0; //onchip accesses
            double sp = 0;
            for(int k = 0; k < (1<<config.ncores); k++){
                if(__builtin_popcount(k) != j) continue;
                double tp = 1;
                for(int l = 0; l < config.ncores; l++){
                    if(k & (1<<l)) tp *= proba[l];
                    else tp *= (1-proba[l]);
                }
                if((chipmask[chip] & k)) p += tp;
                sp += tp;
            }
            p = p / sp;
            out.link[LinkType(chip, chip)] += p * nreuses;
            out.node[chip] = (1-p)*nreuses;
        }
    }
}

//Get Traffic over Interconnection
void POWERSnoopProbabilityState::getInterconnectTraffic(NetworkType<uint64_t, uint64_t> &out, vector<uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config){
}

void POWERSnoopProbabilityState::print(ostream &out) {
    out << "OUTPUT POWERSnoopProbabilityState\n"; 
    out << "TO BE IMPLEMENTED\n";
    out.flush();
}

void POWERSnoopProbabilityState::serialize(ostream &os) {
    const size_t *shape_copies = copies.shape();
    for(int i = 0; i < 4; i++)
        Write(os, shape_copies[i]);
    for(int i = 0; i < shape_copies[0]; i++)
        for(int j = 0; j < shape_copies[1]; j++)
            for(int k = 0; k < shape_copies[2]; k++)
                for(int l = 0; l < shape_copies[3]; l++) 
                    Write(os, copies[i][j][k][l]);
}

POWERSnoopProbabilityState* POWERSnoopProbabilityState::deserialize(istream &is) {
    POWERSnoopProbabilityState *rt = new POWERSnoopProbabilityState();
    size_t shape[4];
    for(int i = 0; i < 4; i++)
        Read(is, shape[i]);
    rt->copies.resize(extents[shape[0]][shape[1]][shape[2]][shape[3]]);
    for(int i = 0; i < shape[0]; i++)
        for(int j = 0; j < shape[1]; j++)
            for(int k = 0; k < shape[2]; k++)
                for(int l = 0; l < shape[3]; l++){
                    Read(is, rt->copies[i][j][k][l]);
                }
    /*
    for(int i = 0; i < shape[3]; i++){
        uint64_t sum = 0;
        for(int j = 0; j < shape[0]; j++)
            for(int k = 0; k < shape[1]; k++){
                uint64_t tsum = 0;
                for(int l = 0; l < shape[2]; l++)
                    tsum += rt->copies[j][k][l][i];
                assert(tsum % (k==0?1:k) == 0);

                tsum /= (k==0?1:k);
                sum += tsum;
            }
        cout << sum << endl;
    }
    */
    return rt;
}

POWERSnoopProbabilityBuilder::POWERSnoopProbabilityBuilder(POWERSnoopProbabilityState *ts, MemRange *_range, int _nThreads){
    nThreads = _nThreads;
    TS = ts;
    range = _range;
    cps.resize(extents[nThreads][nThreads+1][nThreads][MAX_WINDOW]);
    for(int i = 0; i < nThreads; i++)
        for(int j = 0; j <= nThreads; j++)
            for(int k = 0; k < nThreads; k++)
                for(int p = 0; p < MAX_WINDOW; p++)
                    cps[i][j][k][p] = 0;
}

} // namespace Tapas
