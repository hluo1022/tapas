#pragma once

#include <unordered_map>
#include <bitset>
#include <map>
#include <vector>
#include <fstream>
#include <iostream>
#include <cstdint>

#include "general.hpp"
#include "Common/Common.hpp"
#include "Common/Histo.hpp"

#include "boost/multi_array.hpp"

using namespace std;
using namespace histo;
using namespace boost;

namespace Tapas {

//<! per memory range structure
class POWERSnoopProbabilityState{
friend class POWERSnoopProbabilityBuilder;
protected:
    multi_array<uint64_t, 4> exists; //existence
public:
    POWERSnoopProbabilityState();

    //<! total number of memory writes
    uint64_t getTotalMemoryTraffic() ;
    uint64_t getMemoryTraffic(NodeType n, DataPlacement &dp);
    //<! total number of write traffics from srcThread to a chip(threads in a mask)
    uint64_t getWriteChipTraffic(NodeType srcThread, uint64_t chipmask, uint64_t filltime);
    //<! total number of read traffics from srcThread to a chip
    uint64_t getReadChipTraffic(NodeType srcThread, uint64_t chipmask, uint64_t filltime);

    //<! traffic on all of the links and nodes
    void getInterconnectTraffic(NetworkType<uint64_t, uint64_t> &out, vector<uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config);
    void getInterconnectTraffic(InternalTraffic &x, NetworkType<uint64_t, uint64_t> &out, vector<uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config);

    void getInterconnectAccess(NetworkType<uint64_t, uint64_t> &out, vector<uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config);
    void getInterconnectAccess(InternalTraffic &x, NetworkType<uint64_t, uint64_t> &out, vector<uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config);

    InternalTraffic getInternalTraffic(vector<uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config);
    void print(std::ostream &out) ;
    void serialize(std::ostream &OS);
    static POWERSnoopProbabilityState* deserialize(std::istream &IS);
};

// ==================================== //
class POWERSnoopProbabilityBuilder {
private:
    POWERSnoopProbabilityState *TS;
    MemRange *range;
    unordered_map<AddrInt, TimeInt> previous[128];
    int nThreads;

    multi_array<uint64_t, 4> exists;

private:
    LinkType makelink(uint32_t src, uint32_t dst);
public:

    POWERSnoopProbabilityBuilder(POWERSnoopProbabilityState *_ts, MemRange *_range, int _nThreads);

    void onReference(AddrInt addr, uint32_t accessor, TimeInt tick, bool type);

    void finish();
    void serialize(ostream &os) {TS->serialize(os);}
};

} // namespace Tapas
