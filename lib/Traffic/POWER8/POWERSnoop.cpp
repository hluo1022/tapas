#include "POWERSnoop.hpp"
#include <cassert>
#include <iostream>
#include <cstdint>

using namespace std;

namespace Tapas {

static uint64_t cstep = 0;

void POWERSnoopBuilder::onReference(AddrInt addr, NodeType accessor, TimeInt tick, bool type) {
    if(cstep++ % 1000000 == 0)
        cout << cstep << endl;

    if(previous.find(addr) == previous.end())
        previous[addr] = vector<TimeInt>(32, TimeMax);
    auto &rts = previous[addr];
    
    #pragma omp parallel for schedule(auto)
    for(int i = 0; i < TS->comb.size(); i++){
        TimeInt minrt = TimeMax;
        uint64_t id = 0;
        for(auto j : TS->comb[i]){
            minrt = min(tick-rts[j], minrt);
            id |= 1ull << j;
        }
        TimeInt idtick = sublog_value_to_index(minrt);
        uint64_t &rcnt = TS->rdtimes[accessor][TS->idx[id]][idtick];
        //__transaction_atomic{rcnt++;}
        rcnt++;
        if(type == MEMWRITE){
            uint64_t &wcnt = TS->wrtimes[accessor][TS->idx[id]][idtick];
            //__transaction_atomic{wcnt++;}
            wcnt++;
        }
    }

    if(type == MEMWRITE){
        rts.clear();
        rts.resize(32, TimeMax);
        int id = range->getID(addr);
        if(id != NOTFOUND)
            TS->writes[accessor][id]++;
    }
    previous[addr][accessor] = tick;
}

POWERSnoopState::POWERSnoopState(){
}


uint64_t POWERSnoopState::getTotalMemoryTraffic(){ 
    uint64_t rt = 0;
    for(auto i : writes)
        for(auto j : i)
            rt += j.second;
    return rt;
}

uint64_t POWERSnoopState::getMemoryTraffic(NodeType n, DataPlacement &dp){
    uint64_t rt = 0;
    for(auto i : writes){
        for(int j = 0; j < dp.places.size(); j++){
            if(dp.places[j] != n) continue;
            rt += i[j];
        }
    }
    return rt;
}

uint64_t POWERSnoopState::getWriteChipTraffic(NodeType srcThread, uint64_t chipmask, uint64_t filltime){
    filltime = sublog_value_to_index(filltime);
    vector<uint64_t> rt(filltime+1, 0);
    for(auto x : idx){
        if((chipmask | x.first) != chipmask)
            continue;
        uint64_t tmp = 0;
        for(uint64_t i = 0; i <= filltime; i++)
            rt[i] = max(rdtimes[srcThread][x.second][i], rt[i]);
    }
    return accumulate(rt.begin(), rt.end(), (uint64_t)0);
}

uint64_t POWERSnoopState::getReadChipTraffic(NodeType srcThread, uint64_t chipmask, uint64_t filltime){
    filltime = sublog_value_to_index(filltime);
    vector<uint64_t> rt(filltime+1, 0);
    for(auto x : idx){
        if((chipmask | x.first) != chipmask)
            continue;
        uint64_t tmp = 0;
        for(uint64_t i = 0; i <= filltime; i++)
            rt[i] = max(rdtimes[srcThread][x.second][i], rt[i]);
    }
    return accumulate(rt.begin(), rt.end(), (uint64_t)0);
}

void POWERSnoopState::getInterconnectAccess(NetworkType<uint64_t, uint64_t> &out, vector<uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config){
    //prepare threads vector
    vector<NodeType> threads;
    for(auto i : tp.Thread2Core)
        threads.push_back(i.first);
    
    //get abstract traffic
    uint64_t rdtraffic[config.nchips][config.nchips];
    for(int i = 0; i < config.nchips; i++)
        for(int j = 0; j < config.nchips; j++){
            rdtraffic[i][j] = 0;
        }

    for(auto i : threads){
        NodeType core = tp.Thread2Core[i];
        NodeType chip = config.Core2Chip[core];
        NodeType dcm = config.Chip2DCM[dcm];
        
            for(NodeType c = 0; c < config.nchips; c++){
                uint64_t chipmask = 0;
                for(auto t : config.Chip2Cores[c]){
                    if(t == core) continue; //doesn't count local accesses
                    chipmask |= 1ull << tp.Core2Thread[t];
                }
                rdtraffic[chip][c] += getReadChipTraffic(i, chipmask, filltime[c]);
            }
    }

    for(NodeType i = 0; i < config.nchips; i++)
        for(NodeType j = 0; j < config.nchips; j++)
            out.link[LinkType(i,j)] = rdtraffic[i][j];
    
    for(NodeType i = 0; i < config.nchips; i++)
        out.node[i] = getMemoryTraffic(i, dp);
}

//Get Traffic over Interconnection
void POWERSnoopState::getInterconnectTraffic(NetworkType<uint64_t, uint64_t> &out, vector<uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config){
    
    //prepare threads vector
    vector<NodeType> threads;
    for(auto i : tp.Thread2Core)
        threads.push_back(i.first);
    
    //get abstract traffic
    uint64_t rdtraffic[config.nchips][config.nchips];
    uint64_t wrtraffic[config.nchips][config.nchips];
    for(int i = 0; i < config.nchips; i++)
        for(int j = 0; j < config.nchips; j++){
            rdtraffic[i][j] = 0;
            wrtraffic[i][j] = 0;
        }

    for(auto i : threads){
        NodeType core = tp.Thread2Core[i];
        NodeType chip = config.Core2Chip[core];
        NodeType dcm = config.Chip2DCM[dcm];
        
            for(NodeType c = 0; c < config.nchips; c++){
                uint64_t chipmask = 0;
                for(auto t : config.Chip2Cores[c]){
                    if(t == core) continue; //doesn't count local accesses
                        chipmask |= 1ull << tp.Core2Thread[t];
                }
                rdtraffic[chip][c] += getReadChipTraffic(i, chipmask, filltime[c]);
                wrtraffic[chip][c] += getWriteChipTraffic(i, chipmask, filltime[c]);

            }
    }

    //TODO : eliminate duplication
    


    //map abstract traffic to links
    for(NodeType src = 0; src < config.nchips; src++)
        for(NodeType dst = 0; dst < config.nchips; dst++){
        auto links = config.getLinksInScope(src, dst);
        for(auto i : links){
            out.link[i] += rdtraffic[src][dst];
            out.link[i] += wrtraffic[src][dst];
        }
    }
    
    //Set memory traffic on each link
    for(NodeType i = 0; i < config.nchips; i++)
        out.node[i] = getMemoryTraffic(i, dp)*16;


    //rdtraffic also stores parts of data transfer. 
    for(auto i : threads){
        NodeType src = tp.Thread2Core[i];
        src = config.Core2Chip[src];
        for(int j = 0; j < dp.places.size(); j++){
            NodeType dst = dp.places[j];
            rdtraffic[src][dst] += writes[src][j];
        }
    }
    
    //weight data traffic, because data traffic is heavier than coherence traffic
    for(NodeType i = 0; i < config.nchips; i++)
        for(NodeType j = 0; j < config.nchips; j++)
            rdtraffic[i][j] *= 4;


    for(NodeType i = 0; i < config.nchips; i++)
        for(NodeType j = 0; j < config.nchips; j++){
            auto nodes = config.getShortestPath(i, j);
            for(int k = 1; k < nodes.size(); k++)
                out.link[LinkType(nodes[k-1], nodes[k])] += rdtraffic[i][j];
        }
}

void POWERSnoopState::print(ostream &out) {
    out << "OUTPUT POWERSnoopState\n"; 
    out << "TO BE IMPLEMENTED\n";
    out.flush();
}

void POWERSnoopState::serialize(ostream &os) {
    //serialize idx
    Write(os, idx.size());
    for(auto i : idx){
        Write(os, i.first);
        Write(os, i.second);
    }

    //rdtimes
    Write(os, rdtimes.size());
    for(auto i : rdtimes){
        Write(os, i.size());
        for(auto j : i)
            j.serialize(os);
    }

    //wrtimes
    Write(os, wrtimes.size());
    for(auto i : wrtimes){
        Write(os, i.size());
        for(auto j : i)
            j.serialize(os);
    }

    //writes
    Write(os, writes.size());
    for(auto i : writes){
        Write(os, i.size());
        for(auto j : i){
            Write(os, j.first);
            Write(os, j.second);
        }
    }


    Write(os, comb.size());
    for(auto i : comb){
        Write(os, i.size());
        for(auto j : i)
            Write(os, j);
    }
}

POWERSnoopState* POWERSnoopState::deserialize(istream &is) {
    POWERSnoopState* rt = new POWERSnoopState();
    size_t sz;

    //idx
    Read(is, sz);
    for(size_t i = 0; i < sz; i++){
        uint64_t x;
        int y;
        Read(is, x);
        Read(is, y);
        rt->idx[x] = y;
    }

    //rdtimes
    Read(is, sz);
    for(size_t i = 0; i < sz; i++){
        rt->rdtimes.push_back(vector<Histogram<uint64_t>>());
        size_t sz1;
        Read(is, sz1);
        for(size_t j = 0; j < sz1; j++)
            rt->rdtimes[i].push_back(*Histogram<uint64_t>::deserialize(is));
    }

    //wrtimes
    Read(is, sz);
    for(size_t i = 0; i < sz; i++){
        rt->wrtimes.push_back(vector<Histogram<uint64_t>>());
        size_t sz1;
        Read(is, sz1);
        for(size_t j = 0; j < sz1; j++)
            rt->wrtimes[i].push_back(*Histogram<uint64_t>::deserialize(is));
    }

    //writes
    Read(is, sz);
    for(size_t i = 0; i < sz; i++){
        rt->writes.push_back(map<AddrInt, uint64_t>());
        size_t sz1;
        Read(is, sz1);
        while(sz1--){
            AddrInt a;
            uint64_t b;
            Read(is, a);
            Read(is, b);
            rt->writes[i][a] = b;
        }
    }

    //comb
    Read(is, sz);
    for(size_t i = 0; i < sz; i++){
        rt->comb.push_back(vector<int>());
        size_t sz1;
        Read(is, sz1);
        for(size_t j = 0; j < sz1; j++){
            int x;
            Read(is, x);
            rt->comb[i].push_back(x);
        }
    }
    return rt;
}

POWERSnoopBuilder::POWERSnoopBuilder(POWERSnoopState *ts, MemRange *_range, int nThreads, int kThreadsPerChip){
    TS = ts;
    range = _range;
    TS->comb = getAllComb(nThreads, kThreadsPerChip);
    TS->rdtimes.clear();
    TS->rdtimes.resize(nThreads, vector<Histogram<uint64_t>>());
    TS->wrtimes.clear();
    TS->wrtimes.resize(nThreads, vector<Histogram<uint64_t>>());
    TS->writes.resize(nThreads);

    for(int i = 0; i < TS->comb.size(); i++){
        uint64_t x = 0;
        for(auto j : TS->comb[i])
            x |= 1ull << j;
        TS->idx[x] = i;
        for(int j = 0; j < nThreads; j++){
            TS->rdtimes[j].push_back(Histogram<uint64_t>());
            TS->wrtimes[j].push_back(Histogram<uint64_t>());
        }
    }
}

} // namespace Tapas
