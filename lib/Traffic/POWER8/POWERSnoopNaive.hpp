#pragma once 

#include <unordered_map>
#include <bitset>
#include <map>
#include <vector>
#include <fstream>
#include <iostream>

#include "Common/Common.hpp"

namespace Tapas {

//<! per memory range structure
class POWERSnoopNaiveState{
friend class POWERSnoopNaiveBuilder;
public:
    vector<Record> trace;
    ScopeTraffic st;
public:
    POWERSnoopNaiveState();

    uint64_t getMemoryTraffic() ;

    void getScopeTraffic(map<NodeType, uint64_t> &filltime,
                                ThreadPlacement &tp, SystemConfig &config); 
    void getInterconnectTraffic(NetworkType<uint64_t, uint64_t> &out, map<NodeType, uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config);

    void print(std::ostream &out) ;
    void serialize(std::ostream &OS);
    static POWERSnoopNaiveState* deserialize(std::istream &IS);
};

// ==================================== //
class POWERSnoopNaiveBuilder {


private:
    POWERSnoopNaiveState *TS;

private:
    LinkType makelink(uint32_t src, uint32_t dst);

public:

    POWERSnoopNaiveBuilder(POWERSnoopNaiveState *_ts) : TS(_ts)
    {}

    void onReference(AddrInt addr, uint32_t accessor, TimeInt tick, bool type);

    void finish() {}
    void serialize(ostream &os) {TS->serialize(os);}
};

} // namespace Tapas
