#include "POWERSnoopNaive.hpp"
#include <cassert>
#include <iostream>
using namespace std;
namespace Tapas {

POWERSnoopNaiveState::POWERSnoopNaiveState(){
}

void POWERSnoopNaiveBuilder::onReference(AddrInt addr, NodeType accessor, TimeInt tick, bool type) {
    TS->trace.push_back(Record(addr, accessor, tick, type));
}

uint64_t POWERSnoopNaiveState::getMemoryTraffic() {
    uint64_t rt = 0;
    for(auto i : st.MemRequest)
        for(auto j : i.second)
            rt += j;
    return rt;
}

//<! get all traffic, results are stored in out
void POWERSnoopNaiveState::getScopeTraffic(map<NodeType, uint64_t> &filltime, //<! per-chip fill time
                            ThreadPlacement &tp, SystemConfig &config){
    size_t nchips = filltime.size();
    ScopeTraffic &rt = st;
    //<! counting requests solved in chip
    map<AddrInt, vector<TimeInt>> uses;

    for(auto request : trace){
        NodeType c = config.Core2Chip[tp.Thread2Core[request.accessor]];

        bool chit = false;
        bool dhit = false;
        bool shit = false;
        
        if(uses.find(request.addr) == uses.end()){
            uses[request.addr] = vector<TimeInt>(nchips, TimeMax);
            st.MemRequest[request.addr] = vector<uint64_t>(nchips, 0);
        }
        
        auto &refs = uses[request.addr];
        chit |= (request.time-refs[c] <= filltime[c]);

        for(auto x : config.DCM2Chips[config.Chip2DCM[c]])
            dhit |= request.time-refs[x] <= filltime[x];

        for(NodeType x = 0; x < config.nchips; x++)
            shit |= request.time-refs[x] <= filltime[x];

        uint64_t mask = 0;
        for(NodeType x = 0; x < config.nchips; x++){
            if(request.time-refs[x] <= filltime[x])
                mask |= 1ull << x;
        }

        if(chit) rt.CTraffic[c] += 2; //<! in-chip hit, control and data
        else if(dhit) rt.DTraffic[config.Chip2DCM[c]] += 2; //<! in-dcm hit, control and data
        else if(shit){
            rt.boardcast++;
            //Send data from one of the candidates
            rt.STraffic[make_pair(c,mask)]++; 
        }
        else{
            rt.boardcast +=2; //<! control message
            if(rt.MemRequest.find(request.addr) == rt.MemRequest.end())
                rt.MemRequest[request.addr] = vector<uint64_t>(nchips, 0);
            rt.MemRequest[request.addr][c]++;
        }
        
        if(request.type == MEMWRITE){
            if(shit) rt.boardcast++;
            else if(dhit) rt.DTraffic[config.Chip2DCM[c]]++;
            else rt.CTraffic[c]++;

            //invalid all of the other copies
            uses[request.addr] = vector<TimeInt>(nchips, TimeMax);
            //Will introduce memory write back later
            if(rt.MemRequest.find(request.addr) == rt.MemRequest.end())
                rt.MemRequest[request.addr] = vector<uint64_t>(nchips, 0);
            rt.MemRequest[request.addr][c]++;
        }
        
        uses[request.addr][c] = request.time;

    }
}

void POWERSnoopNaiveState::getInterconnectTraffic(NetworkType<uint64_t, uint64_t> &out, map<NodeType, uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config){
    st.clear();
    getScopeTraffic(filltime, tp, config);
    //TODO:calculate traffic on every link
    
    //Traffic in DCM
    for(NodeType i = 0; i < config.ndcms; i++){
        auto &x = config.DCM2Chips[i];
        assert(x.size() == 2 && "a DCM must have excatly 2 chips");
        out.link[LinkType(x[0], x[1])] += st.DTraffic[i];
        out.link[LinkType(x[1], x[0])] += st.DTraffic[i];
    }

    //Traffic between DCM
    for(auto x : st.STraffic){
        NodeType src = x.first.first;
        NodeType dst = config.getClosetChip(src, x.first.second);
        vector<NodeType> path = config.getShortestPath(src, dst); //path is connected chips

        for(int i = 1; i < path.size(); i++){
            out.link[LinkType(path[i-1], path[i])] += x.second;
            out.link[LinkType(path[i], path[i-1])] += x.second;
        }
    }

    //boardcast
    for(auto x : config.LinkID){
        out.link[x.first] += st.boardcast;
    }

    //Traffic introduced by cache miss
    for(auto x : st.MemRequest){
        NodeType src = dp.getPlacement(x.first);
        for(NodeType i = 0; i < x.second.size(); i++){
            out.node[src] += x.second[i]; //memory traffic
            vector<NodeType> path = config.getShortestPath(src, i);
            for(int j = 1; j < path.size(); j++){
                out.link[LinkType(path[j-1], path[j])] += x.second[j];
                out.link[LinkType(path[j], path[j-1])] += x.second[j];
            }
        }
    } 
}

void POWERSnoopNaiveState::print(ostream &out) {
    out << "OUTPUT POWERSnoopNaiveState\n"; 
    out << "TO BE IMPLEMENTED\n";
    st.print(out);
    out.flush();
}

void POWERSnoopNaiveState::serialize(ostream &os) {
    os << trace.size() << " ";
    for(auto x : trace)
        x.serialize(os);
}

POWERSnoopNaiveState* POWERSnoopNaiveState::deserialize(istream &is) {
    POWERSnoopNaiveState* rt = new POWERSnoopNaiveState();
    size_t sz;
    is >> sz;
    for(size_t i = 0; i < sz; i++){
        Record tmp;
        tmp.deserialize(is);
        rt->trace.push_back(tmp);
    }
    return rt;
}

} // namespace Tapas
