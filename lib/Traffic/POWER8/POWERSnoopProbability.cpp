#include "POWERSnoopProbability.hpp"
#include <cassert>
#include <iostream>
#include <cstdint>

using namespace std;

namespace Tapas {

static uint64_t cstep = 0;

void POWERSnoopProbabilityBuilder::onReference(AddrInt addr, NodeType accessor, TimeInt tick, bool type) {

    if(cstep % 1000000 == 0)
        cout << cstep/1000000 << endl;
    cstep++;
    int rid = range->getID(addr);
    if(rid == NOTFOUND) rid = range->size();

    for(int i = 0; i < nThreads; i++){
        uint64_t t;
        if(previous[i].find(addr) != previous[i].end())
            t = tick-previous[i][addr];
        else t = TimeMax;
        exists[rid][accessor][i][sublog_value_to_index(t)]++;
    }

    if(type == MEMWRITE){
        for(int i = 0; i < nThreads; i++){
            if(previous[i].find(addr) != previous[i].end())
                previous[i].erase(addr);
        }
    }
    previous[accessor][addr] = tick;
}

void POWERSnoopProbabilityBuilder::finish(){
    TS->exists.resize(extents[range->size()+1][nThreads][nThreads][MAX_WINDOW]);
    auto &rtexists = TS->exists;

    for(int l = 0; l <= range->size(); l++)
    for(int i = 0; i < nThreads; i++)
        for(int j = 0; j < nThreads; j++){
            rtexists[l][i][j][0] = exists[l][i][j][0];
            for(int k = 1; k < MAX_WINDOW; k++)
                rtexists[l][i][j][k] = exists[l][i][j][k] + rtexists[l][i][j][k-1];
        }
}

POWERSnoopProbabilityState::POWERSnoopProbabilityState(){
}


uint64_t POWERSnoopProbabilityState::getTotalMemoryTraffic(){
    return 0;
}

uint64_t POWERSnoopProbabilityState::getMemoryTraffic(NodeType n, DataPlacement &dp){
    return 0;
}

uint64_t POWERSnoopProbabilityState::getWriteChipTraffic(NodeType srcThread, uint64_t chipmask, uint64_t filltime){
    return 0;
}

uint64_t POWERSnoopProbabilityState::getReadChipTraffic(NodeType srcThread, uint64_t chipmask, uint64_t filltime){
    return 0;
}

InternalTraffic POWERSnoopProbabilityState::getInternalTraffic(vector<uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config){
  InternalTraffic out(dp.size());
  for(int rid = 0; rid <= dp.size(); rid++){
    for(int i = 0; i < config.ncores; i++){ //accessor
      NodeType core = tp.Thread2Core[i];
      NodeType chip = config.Core2Chip[core];
      NodeType dcm = config.Chip2DCM[chip];
      vector<double> prob(config.ncores, 0);

      uint64_t naccess = 0;
      naccess += exists[rid][i][0][MAX_WINDOW-1];
      if(naccess == 0) continue;

      for(int j = 0; j < config.ncores; j++){
        NodeType jcore = tp.Thread2Core[j];
        NodeType jchip = config.Core2Chip[jcore];
        uint64_t jft = sublog_value_to_index(filltime[jchip]);

        if(exists[rid][i][j][MAX_WINDOW-1] == 0)
          prob[j] = 0;
        else
          prob[j] = exists[rid][i][j][jft] / (double)exists[rid][i][j][MAX_WINDOW-1];
      }

      double p = 1;
      for(auto j : config.getOrderedChip(chip)){
        double tprob = 1;
        for(auto k : config.Chip2Cores[j])
        tprob *= (1-prob[tp.Core2Thread[k]]);
        out.access[LinkType(chip, j)] += p*(1-tprob)*naccess;
        p *= tprob;
      }
      //miss
      out.miss[chip][rid] += p*naccess;
    }
  }
  return out;
}

void POWERSnoopProbabilityState::getInterconnectAccess(InternalTraffic &x, NetworkType<uint64_t, uint64_t> &out, vector<uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config){
  for(int i = 0; i < config.nchips; i++)
    for(int j = 0; j < config.nchips; j++)
      out.link[LinkType(i,j)] = x.access[LinkType(i,j)];

  for(int i = 0; i < config.nchips; i++)
    for(int j = 0; j <= dp.size(); j++)
      out.node[i] += x.miss[i][j];

  for(int i = 0; i < config.nchips; i++){
    out.node[i] = 0;
    for(int j = 0; j < config.nchips; j++)
      if(i != j) out.node[i] += out.link[LinkType(i,j)];
  }
}

void POWERSnoopProbabilityState::getInterconnectAccess(NetworkType<uint64_t, uint64_t> &out, vector<uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config){
  InternalTraffic x = getInternalTraffic(filltime, tp, dp, config);
  getInterconnectAccess(x, out, filltime, tp, dp, config);
}

void POWERSnoopProbabilityState::getInterconnectTraffic(NetworkType<uint64_t, uint64_t> &out, vector<uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config){
  InternalTraffic x = getInternalTraffic(filltime, tp, dp, config);
  getInterconnectTraffic(x, out, filltime, tp, dp, config);
}

//Get Traffic over Interconnection
void POWERSnoopProbabilityState::getInterconnectTraffic(InternalTraffic &x, NetworkType<uint64_t, uint64_t> &out, vector<uint64_t> &filltime, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config){
    //Coherence message : 8B*2, boardcast and response
    //Data transfer : 128B, through shortest path
    
    //Traffic by cache hit
    for(NodeType i = 0; i < config.nchips; i++){
        for(NodeType j = 0; j < config.nchips; j++){
            uint64_t amount = x.access[LinkType(i,j)];
            if(i == j) out.link[LinkType(i,j)] += amount * (8*2+128);
            else if(config.Chip2DCM[i] == config.Chip2DCM[j]){
                out.link[LinkType(i,j)] += amount * 8*2; //coherence message
                out.link[LinkType(i,j)] += amount * 128; //data transfer
            }
            else{
                for(auto l : config.LinkID)
                    out.link[l.first] += amount * 8*2;
                auto path = config.getShortestPath(j, i);
                for(int i = 1; i < path.size(); i++)
                    out.link[LinkType(path[i-1], path[i])] += amount * 128;
            }
        }
    }

    //Traffic by cache miss
    for(int i = 0; i < config.nchips; i++){
        for(int j = 0; j < dp.size(); j++){
            uint64_t amount = x.miss[i][j];
            for(auto l : config.LinkID)
                out.link[l.first] += amount * 8*2; //no response
            auto path = config.getShortestPath(dp.places[j], i);
            for(int i = 1; i < path.size(); i++)
                out.link[LinkType(path[i-1], path[i])] += amount * 128;
            out.node[dp.places[j]] += amount * 128;
        }
    }
}

void POWERSnoopProbabilityState::print(ostream &out) {
    out << "OUTPUT POWERSnoopProbabilityState\n";
    out << "TO BE IMPLEMENTED\n";
    out.flush();
}

void POWERSnoopProbabilityState::serialize(ostream &os) {
    const size_t *shape_copies = exists.shape();
    for(int i = 0; i < 4; i++)
        Write(os, shape_copies[i]);

    for(int i = 0; i < shape_copies[0]; i++)
        for(int j = 0; j < shape_copies[1]; j++)
            for(int k = 0; k < shape_copies[2]; k++)
                for(int l = 0; l < shape_copies[3]; l++)
                    Write(os, exists[i][j][k][l]);
}

POWERSnoopProbabilityState* POWERSnoopProbabilityState::deserialize(istream &is) {
    POWERSnoopProbabilityState *rt = new POWERSnoopProbabilityState();
    size_t shape[4];
    for(int i = 0; i < 4; i++){
        Read(is, shape[i]);
    }
    rt->exists.resize(extents[shape[0]][shape[1]][shape[2]][shape[3]]);
    for(int i = 0; i < shape[0]; i++)
        for(int j = 0; j < shape[1]; j++)
            for(int k = 0; k < shape[2]; k++)
                for(int l = 0; l < shape[3]; l++){
                    Read(is, rt->exists[i][j][k][l]);
                    //if(rt->exists[i][j][k]) cout << i << " " << j << " " << k << " " << rt->exists[i][j][k] << endl;
            }
    return rt;
}

POWERSnoopProbabilityBuilder::POWERSnoopProbabilityBuilder(POWERSnoopProbabilityState *ts, MemRange *_range, int _nThreads){
    nThreads = _nThreads;
    TS = ts;
    range = _range;
    exists.resize(extents[range->size()+1][nThreads][nThreads][MAX_WINDOW]);
    for(int k = 0; k <= range->size(); k++)
    for(int i = 0; i < nThreads; i++)
        for(int j = 0; j < nThreads; j++)
                for(int p = 0; p < MAX_WINDOW; p++)
                    exists[k][i][j][p] = 0;
}

} // namespace Tapas
