#include "SourceSnoop.hpp"
#include <iostream>
using namespace std;
namespace Tapas {

void SourceSnoopBuilder::onReference(AddrInt addr,
    uint32_t accessor, TimeInt tick, bool type) {

    if ( type == MEMWRITE ) directory[addr].reset();
    if (!directory[addr].test(accessor)) {
        if (owner.find(addr) != owner.end()) {
            Record r = owner[addr];
            std::size_t idx = histo::sublog_value_to_index(tick - r.time);
            auto link = std::make_pair(r.accessor, accessor);

            TS->forward[link][idx]++;
            TS->request[accessor]++;
            TS->response++;

            if (type == MEMWRITE) {
                // write back
                TS->writeback[accessor]++;
            }
        }
        else {
            TS->writeback[accessor]++;
        }

        owner[addr].accessor = accessor;
    }

    // next request will be forwarded from accessor
    owner[addr].time = tick;
    owner[addr].type = type;

    directory[addr].set(accessor);

}

//<! get traffic on link
uint64_t SourceSnoopState::getLinkTraffic(LinkType link, std::size_t filltime,
                std::map<NodeType, std::vector<uint32_t> > &domain) {

    uint64_t traffic = 0;
    /// TODO optimize this loop from various other possible impl.

    /// This loop handles the data transfer traffic. Each
    /// cacheline is directly forwarded from the last
    /// accessor. This forwarding was recorded in 'forward'
    /// map. 'forward' maps a pair of threadids to its
    /// footprint. The pair of threadids should be converted
    /// to NodeType before processing
    //cout << "F : " << forward.size() << endl;
    for(auto src : domain[link.first]) {
        for(auto dst : domain[link.second]) {
            auto t = forward.find(makelink(src, dst));
            if ( t != forward.end() ) {
                DistHisto dh = t->second;

                // is input an index?
                std::size_t ws = histo::sublog_value_to_index(filltime);
                for(std::size_t i = 0; i < ws; ++i) {
                    traffic += dh[i];
                }
            }
        }
    }

    /// This block encodes the response traffic incurred from
    /// home node
    if ( link.first == home || link.second == home ) {
        traffic += response;
    }

    /// This block encodes the request traffic
    /// if either end of 'link' was a requestor
    for (auto t : domain[link.first]) {
        traffic += request[t];
    }
    for (auto t : domain[link.second]) {
        traffic += request[t];
    }

    /// This block encodes the writeback traffic
    /// if the link is on the writeback route.
    if (link.first == home) {
        for(auto t : domain[link.second]) {
            traffic += writeback[t];
        }
    }
    if (link.second == home) {
        for(auto t : domain[link.first]) {
            traffic += writeback[t];
        }
    }

    return traffic;

}

uint64_t SourceSnoopState::getMemoryTraffic() const {
    uint64_t traffic = 0;
    //cout << "W : " << writeback.size() << endl;
    for(auto n : writeback) {
        traffic += n.second;
    }
    return traffic;
}

void SourceSnoopState::getInterconnectTraffic(
                            NetworkType<uint32_t, uint64_t> &out,
                            NetworkType<std::size_t, std::size_t> &filltime,
                            std::map<NodeType, std::vector<uint32_t> > &domain) {
    out.node[home] = getMemoryTraffic();
    // for every domain pair
    for(auto src : domain) {
        for(auto dst : domain) {
            if ( src.first != dst.first ) {
                std::size_t ft = filltime.node[src.first];
                LinkType link = makelink(src.first, dst.first);
                out.link[link] = getLinkTraffic(link, ft, domain);
            }
        }
    }
}

void SourceSnoopState::print(std::ostream &out) const {
    out << "Home : " << home << "\n";
    out << "Response : " << response << "\n";
    out << "Request :\n";
    for(auto i : request) {
        out << "  " << i.first<< " : " << i.second << "\n";
    }
    out << "Forward :\n";
    for(auto i : forward) {
        out << "  (" << std::setw(2) << i.first.first << " -> "
                   << std::setw(2) << i.first.second << ") :\n";
        for(std::size_t j = 0; j < MAX_WINDOW; ++j) {
            if (i.second[j] != 0) {
                out << "    " << j << " : " << i.second[j] << "\n";
            }
        }
    }
    out << "Writeback : \n";
    for(auto i : writeback) {
        out << "  " << i.first << " : " << i.second << "\n";
    }
    out << std::endl;

}

void SourceSnoopState::serialize(std::ostream &OS) {
    OS.write((char*)&response, sizeof(uint64_t));
    OS.write((char*)&home,     sizeof(NodeType));

    std::size_t s = request.size();
    OS.write((char*)&s, sizeof(std::size_t));
    for(auto i : request) {
        OS.write((char*)&i.first,  sizeof(uint32_t));
        OS.write((char*)&i.second, sizeof(uint64_t));
    }

    s = writeback.size();
    OS.write((char*)&s, sizeof(std::size_t));
    for(auto i : writeback) {
        OS.write((char*)&i.first,  sizeof(uint32_t));
        OS.write((char*)&i.second, sizeof(uint64_t));
    }

    s = forward.size();
    OS.write((char*)&s, sizeof(std::size_t));
    for(auto i : forward) {
        OS.write((char*)&i.first.first,  sizeof(uint32_t));
        OS.write((char*)&i.first.second, sizeof(uint32_t));
        OS.write((char*)(i.second.value), sizeof(uint64_t) * MAX_WINDOW);
    }
}

SourceSnoopState* SourceSnoopState::deserialize(std::ifstream &IS) {
    SourceSnoopState* obj = new SourceSnoopState;
    IS.read((char*)&obj->response, sizeof(uint64_t));
    IS.read((char*)&obj->home,     sizeof(NodeType));

    std::size_t s;
    IS.read((char*)&s, sizeof(std::size_t));
    for(std::size_t i = 0; i < s; ++i) {
        uint32_t k;
        uint64_t v;
        IS.read((char*)&k, sizeof(uint32_t));
        IS.read((char*)&v, sizeof(uint64_t));
        obj->request[k] = v;
    }

    IS.read((char*)&s, sizeof(std::size_t));
    for(std::size_t i = 0; i < s; ++i) {
        uint32_t k;
        uint64_t v;
        IS.read((char*)&k, sizeof(uint32_t));
        IS.read((char*)&v, sizeof(uint64_t));
        obj->writeback[k] = v;
    }

    IS.read((char*)&s, sizeof(std::size_t));
    for(std::size_t i = 0; i < s; ++i) {
        uint32_t k1, k2;
        DistHisto h;
        IS.read((char*)&k1, sizeof(uint32_t));
        IS.read((char*)&k2, sizeof(uint32_t));
        IS.read((char*)&h.value, sizeof(uint64_t) * MAX_WINDOW);
        obj->forward[std::make_pair(k1, k2)] = h;
    }
    //obj->print(cout);
    //cout << obj->forward.size() << endl;
    //for(auto i : obj->forward)
    //    cout << i.first.first << " " << i.first.second << endl;
    //assert(0);
    return obj;
}

} // namespace Tapas
