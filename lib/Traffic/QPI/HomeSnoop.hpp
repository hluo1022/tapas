#include <bitset>
#include <unordered_map>
#include <ostream>
#include <map>
#include <vector>
#include <fstream>
#include <ostream>

#include "Common/Common.hpp"
#include "Common/Histo.hpp"

namespace Tapas {

// ==================================== //
class HomeSnoopState {

    friend class HomeSnoopBuilder;

public:
    typedef std::bitset<256> Directory;
    typedef histo::Histogram<unsigned long> DistHisto;

private:

    std::unordered_map<Directory, unsigned long> broadcast;
    std::unordered_map<unsigned, unsigned long> writeback;
    std::unordered_map<std::pair<unsigned, unsigned>, DistHisto, 
                        std::uintpairhash> forward;

    NodeType home;

public:
    void serialize(std::ostream &OS);
    static HomeSnoopState* deserialize(std::ifstream &IS);

    unsigned long getLinkTraffic(LinkType link, std::size_t filltime, 
                                std::map<NodeType, std::vector<unsigned> > &domain);
    unsigned long getMemoryTraffic() const;

    void getInterconnectTraffic(NetworkType<uint32_t, uint64_t> &out,
                                NetworkType<std::size_t, std::size_t> &filltime,
                                std::map<NodeType, std::vector<unsigned> > &domain);

    inline void setHome(NodeType h) { home = h; }
    inline NodeType getHome() const { return home; }

    void print(std::ostream &out) const;
};

// ==================================== //
class HomeSnoopBuilder {

    struct Record {
        unsigned accessor;
        TimeInt  time;
        char type;
    };

    typedef HomeSnoopState::Directory Directory;

    std::unordered_map<AddrInt, Directory> directory;
    std::unordered_map<AddrInt, Record> owner;  

private:
    HomeSnoopState *TS;

public:

    HomeSnoopBuilder(HomeSnoopState *_ts) : TS(_ts)
    {}

    void onReference(AddrInt addr, unsigned accessor, TimeInt tick, bool type);

};

} // namespace Tapas
