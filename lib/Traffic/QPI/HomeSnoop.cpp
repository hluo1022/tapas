#include "HomeSnoop.hpp"

namespace Tapas {

void HomeSnoopBuilder::onReference(AddrInt addr, 
    unsigned accessor, TimeInt tick, bool type) {

    if ( type == MEMWRITE ) directory[addr].reset();

    if (!directory[addr].test(accessor)) {
        if (owner.find(addr) != owner.end()) {
            Record r = owner[addr];
            std::size_t idx = histo::sublog_value_to_index(tick - r.time);
            auto link = std::make_pair(r.accessor, accessor);
            TS->forward[link][idx]++;

            auto sm = directory[addr];
            sm.set(accessor);
            TS->broadcast[sm]++;

            if (type == MEMWRITE) {
                // write back
                TS->writeback[accessor]++;
            }
        }
        else {
            // first access needs to load from memory
            TS->writeback[accessor]++;
        }
    }

    // next request will be forwarded from accessor
    owner[addr].accessor = accessor;
    owner[addr].time = tick;
    owner[addr].type = type;

    directory[addr].set(accessor);
}

unsigned long 
HomeSnoopState::getLinkTraffic(LinkType link, std::size_t filltime, 
                std::map<NodeType, std::vector<unsigned> > &domain) {

    unsigned long traffic = 0;

    /// TODO optimize this loop from various other possible impl.

    /// This loop handles the data transfer traffic. Each
    /// cacheline is directly forwarded from the last
    /// accessor. This forwarding was recorded in 'forward'
    /// map. 'forward' maps a pair of threadids to its
    /// footprint. The pair of threadids should be converted
    /// to NodeType before processing
    for(auto src : domain[link.first]) {
        for(auto dst : domain[link.second]) {
            auto t = forward.find(makelink(src, dst)); 
            if ( t != forward.end() ) {
                DistHisto dh = t->second;

                // is input an index?
                std::size_t ws = histo::sublog_value_to_index(filltime);
                for(std::size_t i = 0; i < ws; ++i) {
                    traffic += dh[i];
                }
            }
        }
    }

    if ( link.first == home || link.second == home ) {
        for(auto i : broadcast) {
            Directory d = i.first;

            if ( link.first == home ) {
                for(auto t : domain[link.second]) {
                    if (d.test(t)) {
                        traffic += i.second * 2;
                        break; // only do it for one thread, not all
                    }
                }
            }
            else {
                for(auto t : domain[link.first]) {
                    if (d.test(t)) {
                        traffic += i.second * 2;
                        break; // only do it for one thread, not all
                    }
                }
            }
        }
    }

    /// This block encodes the writeback traffic
    /// if the link is on the writeback route.
    if (link.first == home) {
        for(auto t : domain[link.second]) {
            traffic += writeback[t];
        }
    }
    if (link.second == home) {
        for(auto t : domain[link.first]) {
            traffic += writeback[t];
        }
    }

    return traffic;
}

unsigned long HomeSnoopState::getMemoryTraffic() const {
    unsigned long traffic = 0;
    for(auto n : writeback) {
        traffic += n.second;
    }
    return traffic;
}

void HomeSnoopState::getInterconnectTraffic(
                            NetworkType<uint32_t, uint64_t> &out,
                            NetworkType<std::size_t, std::size_t> &filltime,
                            std::map<NodeType, std::vector<unsigned> > &domain) {

    out.node[home] = getMemoryTraffic();

    // for every domain pair
    for(auto src : domain) {
        for(auto dst : domain) {
            if ( src.first != dst.first ) {
                std::size_t ft = filltime.node[src.first];
                LinkType link = makelink(src.first, dst.first);
                out.link[link] = getLinkTraffic(link, ft, domain);
            }
        }
    }
}

void HomeSnoopState::print(std::ostream &out) const {
    out << "Home : " << home << "\n";
    out << "Broadcast :\n";
    for(auto i : broadcast) {
        out << "  " << i.first.to_string() << "\t" << i.second << "\n";
    }
    out << "Forward :\n";
    for(auto i : forward) {
        out << "  (" << std::setw(2) << i.first.first << " -> " 
                   << std::setw(2) << i.first.second << ") :\n";
        for(std::size_t j = 0; j < MAX_WINDOW; ++j) {
            if (i.second[j] != 0) {
                out << "    " << j << " : " << i.second[j] << "\n";
            }
        }
    }
    out << "Writeback : \n";
    for(auto i : writeback) {
        out << "  " << i.first << " : " << i.second << "\n";
    }
    out << std::endl;
}

void HomeSnoopState::serialize(std::ostream &OS) {
    OS.write((char*)&home,     sizeof(NodeType));

    std::size_t s = broadcast.size();
    OS.write((char*)&s, sizeof(std::size_t));
    for(auto i : broadcast) {
        OS.write((char*)&i.first,  sizeof(Directory));
        OS.write((char*)&i.second, sizeof(unsigned long));
    }

    s = writeback.size();
    OS.write((char*)&s, sizeof(std::size_t));
    for(auto i : writeback) {
        OS.write((char*)&i.first,  sizeof(unsigned));
        OS.write((char*)&i.second, sizeof(unsigned long));
    }

    s = forward.size();
    OS.write((char*)&s, sizeof(std::size_t));
    for(auto i : forward) {
        OS.write((char*)&i.first.first,  sizeof(unsigned));
        OS.write((char*)&i.first.second, sizeof(unsigned));
        OS.write((char*)(i.second.value), sizeof(unsigned long) * MAX_WINDOW);
    }
}

HomeSnoopState* HomeSnoopState::deserialize(std::ifstream &IS) {
    HomeSnoopState* obj = new HomeSnoopState;
    IS.read((char*)&obj->home,     sizeof(NodeType));

    std::size_t s;
    IS.read((char*)&s, sizeof(std::size_t));
    for(std::size_t i = 0; i < s; ++i) {
        unsigned k;
        unsigned long v;
        IS.read((char*)&k, sizeof(Directory));
        IS.read((char*)&v, sizeof(unsigned long));
        obj->broadcast[k] = v;
    }

    IS.read((char*)&s, sizeof(std::size_t));
    for(std::size_t i = 0; i < s; ++i) {
        unsigned k;
        unsigned long v;
        IS.read((char*)&k, sizeof(unsigned));
        IS.read((char*)&v, sizeof(unsigned long));
        obj->writeback[k] = v;
    }

    IS.read((char*)&s, sizeof(std::size_t));
    for(std::size_t i = 0; i < s; ++i) {
        unsigned k1, k2;
        DistHisto h;
        IS.read((char*)&k1, sizeof(unsigned));
        IS.read((char*)&k2, sizeof(unsigned));
        IS.read((char*)&h.value, sizeof(unsigned long) * MAX_WINDOW);
        obj->forward[std::make_pair(k1, k2)] = h;
    }

    return obj;
}

} // namespace Tapas
