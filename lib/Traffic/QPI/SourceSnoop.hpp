#include <unordered_map>
#include <bitset>
#include <ostream>
#include <map>
#include <vector>
#include <fstream>
#include <ostream>

#include "Common/Common.hpp"
#include "Common/Histo.hpp"

namespace Tapas {

// ==================================== //
class SourceSnoopState {

    friend class SourceSnoopBuilder;

public:
    typedef std::bitset<256> Directory;
    typedef histo::Histogram<uint64_t> DistHisto;

public:
    uint64_t response;
    std::unordered_map<uint32_t, uint64_t> request;
    std::unordered_map<uint32_t, uint64_t> writeback;
    std::unordered_map<std::pair<uint32_t, uint32_t>, DistHisto,
                        std::uintpairhash> forward;

    NodeType home;

public:

    SourceSnoopState() : response(0), home(0)
    {}

    uint64_t getLinkTraffic(LinkType link, std::size_t filltime,
                            std::map<NodeType, std::vector<uint32_t> > &domain);
    uint64_t getMemoryTraffic() const;

    void getInterconnectTraffic(NetworkType<uint32_t, uint64_t> &out,
                                NetworkType<std::size_t, std::size_t> &filltime,
                                std::map<NodeType, std::vector<uint32_t> > &domain);

    inline void setHome(NodeType h) { home = h; }
    inline NodeType getHome() const { return home; }

    void print(std::ostream &out) const;
    void serialize(std::ostream &OS);
    static SourceSnoopState* deserialize(std::ifstream &IS);

};

// ==================================== //
class SourceSnoopBuilder {

    struct Record {
        uint32_t accessor;
        TimeInt  time;
        char type;
    };

    typedef SourceSnoopState::Directory Directory;

    std::map<AddrInt, Directory> directory;
    std::map<AddrInt, Record> owner;

private:
    SourceSnoopState *TS;

private:
    LinkType makelink(uint32_t src, uint32_t dst);

public:

    SourceSnoopBuilder(SourceSnoopState *_ts) : TS(_ts)
    {}

    void onReference(AddrInt addr, uint32_t accessor, TimeInt tick, bool type);

};

} // namespace Tapas
