#pragma once

#include <list>
#include <fstream>
#include <ostream>

#include "Common/Common.hpp"

#include "Common/RangeTable.hpp"
#include "Common/Util.hpp"

#include "Traffic/QPI/SourceSnoop.hpp"
#include "Traffic/QPI/HomeSnoop.hpp"

namespace Tapas {

// structure modeling the event of changing one region's placement
struct UpdateEvent {
    NodeType src;
    NodeType dst;
    AddrInt  addr;

};

template<typename S, typename B>
class TrafficManager {

public:
    AddressRangeTable<S*> mState;
    AddressRangeTable<B*> mBuilder;

    NetworkType<uint32_t, uint64_t> mPrevTraffic;

    std::list<UpdateEvent> mUpdateList;
    
    uint64_t untracked_addresses;
    uint64_t tracked_addresses;

public:

    ~TrafficManager();
    TrafficManager();

    void serialize(std::ostream &OS);
    static void deserialize(std::ifstream &IS, TrafficManager<S, B>* obj);

    /// prepare, register address range
    void registerAddressRange(AddrInt start, AddrInt end);

    /// profile
    void refer(AddrInt addr,       // target address
               unsigned accessor,  // accessor
               TimeInt tick,       // time
               bool type);         // access type 'R'/'W'
    void finish();

    /// model, input configuration
    void setHome(AddrInt start, AddrInt end, NodeType home);
    void setHome(AddrInt start, NodeType home);
    void getInterconnectTraffic(NetworkType<uint32_t, uint64_t> &out,
    	                        NetworkType<std::size_t, std::size_t> &filltime,
                                std::map<NodeType, std::vector<unsigned> > &domain,
                                bool incremental
                               );
    void getInterconnectTraffic(NetworkType<uint32_t,uint64_t> &out,
                                NetworkType<std::size_t, std::size_t> &filltime,
                                std::map<NodeType, std::vector<unsigned>> &domain,
                                AddrInt start, AddrInt end, NodeType home = 0);


    /// print, for debug
    void printRanges(std::ostream &out);
    void printTraffic(std::ostream &out);
};

// class specialization declaration
template class TrafficManager<SourceSnoopState, SourceSnoopBuilder>;
template class TrafficManager<HomeSnoopState, HomeSnoopBuilder>;

/* implementation of the template class */
template<typename S, typename B>
TrafficManager<S,B>::TrafficManager(){
    tracked_addresses = 0;
    untracked_addresses = 0;
}

template<typename S, typename B>
TrafficManager<S, B>::~TrafficManager() {
    for(auto s : mState) {
        delete s.second;
    }
    cout << "Tracked/Untracked Data : " << tracked_addresses << " " << untracked_addresses << endl;
}

template<typename S, typename B>
void TrafficManager<S, B>::registerAddressRange(AddrInt start, AddrInt end) {
    S* state = new S;
    B* builder = new B(state);
    mState.insert(start, end, state);
    mBuilder.insert(start, end, builder);
}

template<typename S, typename B>
void TrafficManager<S, B>::refer(AddrInt addr, unsigned accessor,
                                 TimeInt tick, bool type) {
    B* ptr = NULL;
    bool status;
    AddrInt s, e;
    status = mBuilder.GetRange(addr, ptr, s, e);
    if ( !status ) {
        untracked_addresses++;
        //std::cerr << "[WARN] : No address range found for the address "
        //          << std::hex << "0x" << addr << std::dec << "\n";
        return;
    }
    else
        tracked_addresses++;
    assert(s < e);
    ptr->onReference(addr, accessor, tick, type);
}

template<typename S, typename B>
void TrafficManager<S, B>::finish() {
    for(auto b : mBuilder) {
        delete b.second;
    }
}


template<typename S, typename B>
void TrafficManager<S, B>::setHome(AddrInt end, NodeType new_home) {

    AddrInt s, e;
    S *state = NULL;
    mState.GetRange(end, state, s, e);
    setHome(s, e, new_home);
}

/*! Legacy code, don't know why designed like this */
template<typename S, typename B>
void TrafficManager<S, B>::setHome(AddrInt start, AddrInt end, NodeType new_home) {

    AddrInt s, e;
    S* state = NULL;
    mState.GetRange(end, state, s, e);

    //The implementation has some problem, the address hasn't been perfectly divied
    //assert(s == start && e == end && "[ERROR] address range management has problem.");

    NodeType old_home = state->getHome();
    state->setHome(new_home);

    // record this update
    UpdateEvent ue;
    ue.src = old_home;
    ue.dst = new_home;
    ue.addr = s;

    mUpdateList.push_back(ue);

}

template<typename S, typename B>
void TrafficManager<S, B>::getInterconnectTraffic(
            NetworkType<uint32_t, uint64_t> &out,
            NetworkType<std::size_t, std::size_t> &filltime,
            std::map<NodeType, std::vector<unsigned> > &domain,
            bool incremental
            ) {
    if (incremental) {

        out = mPrevTraffic;
        while(!mUpdateList.empty()) {
            UpdateEvent ue = mUpdateList.front();
            mUpdateList.pop_front();

            // apply the recorded updates
            AddrInt s = ue.addr;
            NodeType src = ue.src;
            NodeType dst = ue.dst;

            S *state = NULL;
            mState.GetState(s, state);

            NodeType curr_home = state->getHome();

            // derive the traffic of source home
            state->setHome(src);
            NetworkType<uint32_t, uint64_t> n1;
            state->getInterconnectTraffic(n1, filltime, domain);

            // derive the traffic of destination home
            state->setHome(dst);
            NetworkType<uint32_t, uint64_t> n2;
            state->getInterconnectTraffic(n2, filltime, domain);

            MergeNetwork(out, n2);
            DiffNetwork(out, n1);

            // restore the home
            state->setHome(curr_home);
        }
    }
    else {
        for(auto s : mState) {
            S* state = s.second;
            //cout << state->forward.size() << endl; 
            // 0xffff is the invalid value, which means this range has no
            // traffic at all
            if ( state->getHome() == 0xffff) continue;
            NetworkType<uint32_t, uint64_t> temp;
            state->getInterconnectTraffic(temp, filltime, domain);
            MergeNetwork(out, temp);
        }

        // reset the update list
        mUpdateList.clear();

    }

    // record the last traffic evaluated
    mPrevTraffic = out;

}

template<typename S, typename B>
void TrafficManager<S,B>::getInterconnectTraffic(NetworkType<uint32_t, uint64_t> &out,
                                NetworkType<std::size_t, std::size_t> &filltime,
                                std::map<NodeType, std::vector<unsigned>> &domain,
                                AddrInt start, AddrInt end, NodeType newHome){

    S* state = NULL;
    AddrInt s, e;
    mState.GetRange(start, state, s, e);
    assert(s == start && e == end && "[ERROR] address range management has problem.");
    //cout << s << " " << start << " " << e << " " << end << endl;
    NodeType oldHome = state->getHome();
    state->setHome(newHome);
    NetworkType<uint32_t, uint64_t> rt;
    state->getInterconnectTraffic(rt, filltime, domain);
    out = rt;
    state->setHome(oldHome);
}

template<typename S, typename B>
void TrafficManager<S, B>::serialize(std::ostream &OS) {
    std::size_t s = mState.data.size();
    OS.write((char*)&s, sizeof(std::size_t));
    for(auto i : mState.data) {
        OS.write((char*)&i.first, sizeof(AddrInt));
        i.second->serialize(OS);
    }
    for(auto i : mState.end2start) {
        OS.write((char*)&i.second, sizeof(AddrInt));
        OS.write((char*)&i.first, sizeof(AddrInt));
        assert(mState.data.find(i.first) != mState.data.end());
    }
}

template<typename S, typename B>
void TrafficManager<S, B>::deserialize(std::ifstream &IS, TrafficManager<S, B>* obj) {
    std::size_t s;
    IS.read((char*)&s, sizeof(std::size_t));
    for(std::size_t i = 0; i < s; ++i) {
        AddrInt a;
        IS.read((char*)&a, sizeof(AddrInt));
        S* state = S::deserialize(IS);
        obj->mState.data[a] = state;
    }
    for(std::size_t i = 0; i < s; ++i) {
        AddrInt a, b;
        IS.read((char*)&a, sizeof(AddrInt));
        IS.read((char*)&b, sizeof(AddrInt));
        obj->mState.end2start[b] = a;
    }
}

template<typename S, typename B>
void TrafficManager<S, B>::printRanges(std::ostream &out) {
   // for(auto &i : mState.ends) {
    //    out << mState.starts[i] << " " << i << "\n";
    //}
//    out << "\n" << mState.starts.size() << " " << mState.ends.size() << endl;
}

template<typename S, typename B>
void TrafficManager<S, B>::printTraffic(std::ostream &out) {
    PrintNetwork(mPrevTraffic, out);
}

} // namespace Tapas
