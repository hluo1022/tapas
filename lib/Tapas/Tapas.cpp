#include <numeric>  // std::iota
#include <iostream>
#include <iomanip>

#include "Common/Common.hpp"
#include "Trace/TraceReader.hpp"
#include "Common/Util.hpp"

#include "Traffic/TrafficManager.hpp"
#include "Footprint/FootprintManager.hpp"
#include "Trace/TraceManager.hpp"
#include "Model/ModelManager.hpp"

#include "Model/GradientDescent.hpp"
#include "Model/PlainScore.hpp"

#include "Tapas.hpp"

namespace Tapas {

TrafficManager<SourceSnoopState, SourceSnoopBuilder> gTrafficMgr; //!< Global Mgr, including address ranges, interconnections and traffics over them
FootprintManager gFpMgr; //!< Global Mgr, Manage footprint histogram
TraceManager gTraceMgr; //!< Global Mgr, read traces from files, deal with the basename, pid and tids
ModelManager gModelMgr; //!< Global Mgr, System configuration and performnace model

GradientDescent  GDModel;
PlainScore       PSModel;


bool EnableProgressBar = true;

/** notify the traffic manager about this address range */
void RegisterAddressRange(uint64_t start, uint64_t end) {
    gTrafficMgr.registerAddressRange(start, end);
}

/*! place an address range on home node */
void SetHomeNode(uint64_t end, uint32_t home){
    gTrafficMgr.setHome(end, home);
}

/*! place an address range in the speficied home node */
void LocateAddressRange(uint64_t start, uint64_t end, uint32_t home) {
    gTrafficMgr.setHome(start, end, home);
}

void RawGraph::AddNode(uint32_t n){
  node.insert(n);
}

/*! Insert directed edge */
void RawGraph::AddEdge(uint32_t n1, uint32_t n2){
  edge.insert(std::make_pair(n1, n2));
}


/** routines for setting up the network topology as a graph */
void AddRawGraphNode(RawGraph &g, uint32_t node) {
    g.node.insert(node);
}

void AddRawGraphEdge(RawGraph &g, uint32_t n1, uint32_t n2) {
    g.edge.insert(std::make_pair(n1, n2));
    g.edge.insert(std::make_pair(n2, n1));
}

void RegisterGraph(RawGraph &g) {
    gModelMgr.setRawGraph(g);
}

void RegisterSearchModel(ModelType t) {
    if ( t == GradientDescentModel ) {
        gModelMgr.setModel(&GDModel);
    }
    else if ( t == PlainScoreModel ) {
        gModelMgr.setModel(&PSModel);
    }
    else {
        std::cerr << "[Tapas] : Unknown model\n";
    }
}

/*! profile the trace to set up footprint and traffic
  trace is the base name of trace files
  pid is the pid of the profiled program
  tids is tids of the profiled program
  Output : FPMgr and TrafficMgr
*/
void ProfileTrace(const std::string &trace, const std::string &pid, const vector<uint32_t> &tids) {

    std::size_t progress = 0, total;
    std::size_t threshold = 0;
    std::stringstream bar;

    TimeInt tick = 0;
    MEMREF e;
    
    /*! set basenames and suffixes */
    gTraceMgr.setSource(trace, pid);
    gTraceMgr.open(tids);
    /*! get total number of accesses */
    total = gTraceMgr.getTotalWork();
    // TODO add address sharing check
    while( gTraceMgr.next(e) ) {
        tick = e.time - gTraceMgr.start + 1;

        AddrInt addr = ( e.address >> 6 ) << 6;
        gTrafficMgr.refer(addr, e.thread, tick, e.type);
        gFpMgr.refer(addr, e.thread, tick, e.type);
    }
    gTraceMgr.close();

    gTrafficMgr.finish();
    gFpMgr.finish(tick);
}

template<typename SnoopBuilder>
void ProfileTrace(const std::string &trace, const std::string &pid, const std::vector<uint32_t> &tids, SnoopBuilder &x){
    std::size_t progress = 0, total;
    std::size_t threshold = 0;
    std::stringstream bar;

    TimeInt tick = 0;
    MEMREF e;
    
    /*! set basenames and suffixes */
    gTraceMgr.setSource(trace, pid);
    gTraceMgr.open(tids);
    /*! get total number of accesses */
    total = gTraceMgr.getTotalWork();
    // TODO add address sharing check
    while( gTraceMgr.next(e) ) {
        tick = e.time - gTraceMgr.start + 1;

        AddrInt addr = ( e.address >> 6 ) << 6;
        x.onReference(addr, e.thread, tick, e.type);
        gFpMgr.refer(addr, e.thread, tick, e.type);
    }
    gTraceMgr.close();

    x.finish();
    gFpMgr.finish(tick);
}

/*! serialize the profiled trace stats to filename */
void DumpTraceProfile(const std::string &filename) {
    std::ofstream traffic_file;
    std::ofstream footprint_file;

    traffic_file.open(string(filename) + ".traffic",
           std::ios_base::out | std::ios_base::binary);

    footprint_file.open(string(filename) + ".footprint",
           std::ios_base::out | std::ios_base::binary);

    if ( !traffic_file.is_open() || !footprint_file.is_open() ) {
        std::cerr << "[ERROR] : Can not open serialized traffic/footprint files\n";
        exit(-1);
    }

    gTrafficMgr.serialize(traffic_file);
    gFpMgr.serialize(footprint_file);

    traffic_file.close();
    footprint_file.close();
}

/* deserialize the profiled trace stats */
void ReadTraceProfile(const std::string &filename) {

    std::ifstream traffic_file;
    std::ifstream footprint_file;

    traffic_file.open(string(filename) + ".traffic",
           std::ios_base::in | std::ios_base::binary);

    footprint_file.open(string(filename) + ".footprint",
           std::ios_base::in | std::ios_base::binary);

    if ( !traffic_file.is_open() || !footprint_file.is_open() ) {
        std::cerr << "[ERROR] : Can not open serialized traffic/footprint files\n";
        exit(-1);
    }

    TrafficManager<SourceSnoopState, SourceSnoopBuilder>::deserialize(traffic_file, &gTrafficMgr);
    FootprintManager::deserialize(footprint_file, &gFpMgr);

    traffic_file.close();
    footprint_file.close();
}

void PrintScore(std::ostream &out, const SystemConfig &config,
                DomainMap &plan) {
    gModelMgr.printScore(out, plan, config);
    gModelMgr.printTraffic(out, plan, config);
}


ScoreType GetScore(const SystemConfig &config, DomainMap &opt){
    ScoreType score;
    gModelMgr.isOptimal(opt, config, score, false);
    return score;
}

/*! Search for thread binding plan, lower score is better */
ScoreType SearchForOptimal(const SystemConfig &config,
                 DomainMap &opt, bool incremental) {

    ScoreType score;
    DomainMap plan = opt;
    int rounds = 0;

#ifdef DEBUG
    std::cout << "[PROFILE] : " << rounds << " Rounds.\n";
    for(auto i : plan) {
        std::cout << "  Thread " << i.first << " -> "
                  << "Socket " << i.second << std::endl;
    }
#endif
//    gModelMgr.isOptimal(plan, config, score);
    while( !gModelMgr.isOptimal(plan, config, score, incremental) ) {
        DomainMap next;
        gModelMgr.getNext(plan, next);
        plan = next;
        rounds++;
#ifdef DEBUG
        std::cout << "[PROFILE] : " << rounds << " Rounds.\n";
        for(auto i : plan) {
            std::cout << "  Thread " << i.first << " -> "
                      << "Socket " << i.second << std::endl;
        }
#endif
    }

    opt = plan;

#ifdef DEBUG
    std::cout << "[RESULTS] : Optimal mapping\n";
    for(auto i : opt) {
        std::cout << "  Thread " << i.first << " -> "
                  << "Socket " << i.second << std::endl;
    }
#endif

    return score;
}

//<! lower score is better
bool SearchForDP(vector<RangeInfo> &place, SystemConfig &config, DomainMap &plan, ScoreType &best, Statistics &stat, int iterations, int FrequentDump){
    uint32_t trials = 0;
    uint32_t NumNodes = config.nnodes;

    vector<uint64_t> cap;
    for(int i = 0; i < config.nnodes; i++)
        cap.push_back(config.MemCapacity[i]);
    
    for(int i = 0; i < iterations; i++) {
        cout << "Number of memory region : " << place.size() << endl;
        for(auto &entry : place) {
            uint32_t src = entry.node;
            if ( src >= NumNodes ) continue;
            
            //<! Try new nodes
            uint32_t dst;
            while(1){
                dst = (src + rand() % (NumNodes - 2) + 1) % NumNodes;
                if(entry.GetSize() <= cap[dst])
                    break;
            }
            
            cap[src] += entry.GetSize();
            cap[dst] -= entry.GetSize();
            
            AddrInt addr_end = entry.end;

            SetHomeNode(addr_end, dst);
            ScoreType score = SearchForOptimal(config, plan, false);

            /*
               cout << "[PROFILE] : Try mapping "
               << units[addr].id
               << "[" << units[addr].start_i << ","
               << units[addr].end_i << ")"
               << " from " << src
               << " to " << dst << "\n";
               cout << "[PROFILE] : " << "best "
               << best;

               if ( score < best ) {
               cout << " - " << best - score;
               }
               else {
               cout << " + " << score - best;
               }
               cout << "\n";
            */
            // take the absolute score change as the impact of this region
            //units[addr].impact = abs(score - best);

            // only insert the impact once for one iteration
            /*
            if ( i == iterations - 1 ) {
                stat.impact += units[addr].impact;
                stat.impact_list.push(units[addr]);
            }
            */

            if ( score < best || best == 0) {
                entry.node = dst;
                best = score;
                stat.remapped.insert(addr_end);
            }
            else if (score >= best){
                SetHomeNode(addr_end, src);
                cap[src] -= entry.GetSize();
                cap[dst] += entry.GetSize();
            }

            stat.covered.insert(addr_end);

            /*
            // frequently dump the placement
            if ( FrequentDump > 0 &&
                    trials++ % FrequentDump == (FrequentDump-1) ) {

                ofstream plan_profile("plan");
                print_plan(plan_profile, plan);
                plan_profile.close();

                ofstream place_profile("place");
                print_place(place_profile, addresses, place, units);
                place_profile.close();
            }
            */
        }
    }
    return true;
}

vector<L2DVector<ScoreType>> GetScoreOnLinks(const SystemConfig &config, DomainMap &plan, std::map<uint64_t, RangeInfo> regions){
    NetworkType<uint32_t, uint64_t> out;
    NetworkType<size_t, size_t> filltime;
    gModelMgr.M->getTraffic(plan, config, out, filltime, true);

    std::map<NodeType, std::vector<uint32_t> > Cores;
    std::map<NodeType, std::vector<LinkType> > Links;
    gModelMgr.M->revertDomainMap(plan, Cores, Links);
    out.clear();

    vector<L2DVector<ScoreType>> traffics;
    for(auto i : regions){
        traffics.push_back(L2DVector<ScoreType>(config.LinkID.size()/2));
        auto& itr = traffics.back();
        for(uint32_t j = 0; j < config.nnodes; j++){
            out.clear();
            gTrafficMgr.getInterconnectTraffic(out, filltime, Cores, i.first, i.second.end, j);
            set<LinkType> visited;
            for(auto l : out.link){
                if(visited.find(l.first) != visited.end())
                    continue;
                int t = l.second;
                LinkType ReverseDirection = reverselink(l.first);
                if(out.link.find(ReverseDirection) != out.link.end()){
                    t += out.link[ReverseDirection];
                    visited.insert(ReverseDirection);
                }
                itr.push_back(t);
            }
        }
    }
    return traffics;
}

vector<L2DVector<ScoreType>> GetScoreOnM2MLinks(const SystemConfig &config, DomainMap &plan, std::map<uint64_t, RangeInfo> regions){
    NetworkType<uint32_t, uint64_t> out;
    NetworkType<size_t, size_t> filltime;
    gModelMgr.M->getTraffic(plan, config, out, filltime, true);

    std::map<NodeType, std::vector<uint32_t> > Cores;
    std::map<NodeType, std::vector<LinkType> > Links;
    gModelMgr.M->revertDomainMap(plan, Cores, Links);
    out.clear();

    vector<L2DVector<ScoreType>> traffics;
    for(auto i : regions){
        traffics.push_back(L2DVector<ScoreType>(config.LinkID.size()/2));
        auto& itr = traffics.back();
        for(uint32_t j = 0; j < config.nnodes; j++){
            out.clear();
            gTrafficMgr.getInterconnectTraffic(out, filltime, Cores, i.first, i.second.end, j);
            set<LinkType> visited;
            for(auto l : out.link){
                if(visited.find(l.first) != visited.end())
                    continue;
                int t = l.second;
                LinkType ReverseDirection = reverselink(l.first);
                if(out.link.find(ReverseDirection) != out.link.end()){
                    t += out.link[ReverseDirection];
                    visited.insert(ReverseDirection);
                }
                itr.push_back(t);
            }
        }
    }
    return traffics;
}

vector<L2DVector<ScoreType>> GetScoreOnC2MLinks(const SystemConfig &config, DomainMap &plan, std::map<uint64_t, RangeInfo> regions){
    NetworkType<uint32_t, uint64_t> out;
    NetworkType<size_t, size_t> filltime;
    gModelMgr.M->getTraffic(plan, config, out, filltime, true);

    std::map<NodeType, std::vector<uint32_t> > Cores;
    std::map<NodeType, std::vector<LinkType> > Links;
    gModelMgr.M->revertDomainMap(plan, Cores, Links);
    out.clear();

    vector<L2DVector<ScoreType>> traffics;
    for(auto i : regions){
        traffics.push_back(L2DVector<ScoreType>(config.nnodes));
        auto& itr = traffics.back();
        for(uint32_t j = 0; j < config.nnodes; j++){
            out.clear();
            gTrafficMgr.getInterconnectTraffic(out, filltime, Cores, i.first, i.second.end, j);
            for(auto l : out.node){
                itr.push_back(l.second);
            }
        }
    }
    return traffics;
}

vector<L2DVector<ScoreType>> GetScoreOnAllLinks(const SystemConfig &config, DomainMap &plan, std::map<uint64_t, RangeInfo> regions){
    NetworkType<uint32_t, uint64_t> out;
    NetworkType<size_t, size_t> filltime;
    gModelMgr.M->getTraffic(plan, config, out, filltime, true);

    std::map<NodeType, std::vector<uint32_t> > Cores;
    std::map<NodeType, std::vector<LinkType> > Links;
    gModelMgr.M->revertDomainMap(plan, Cores, Links);

    vector<L2DVector<ScoreType>> traffics;
    for(auto i : regions){
        out.clear();
        traffics.push_back(L2DVector<ScoreType>(config.nnodes + config.LinkID.size()/2));
        auto& itr = traffics.back();

        for(uint32_t j = 0; j < config.nnodes; j++){
            gTrafficMgr.getInterconnectTraffic(out, filltime, Cores, i.first, i.second.end, j);
            for(int k = 0; k < config.nnodes; k++)
                itr.push_back(0);
            for(auto l : out.node){
                itr[j][l.first] = l.second;
            }

            set<LinkType> visited;
            for(auto l : out.link){
                if(visited.find(l.first) != visited.end())
                    continue;
                int t = l.second;
                LinkType ReverseDirection = reverselink(l.first);
                if(out.link.find(ReverseDirection) != out.link.end()){
                    t += out.link[ReverseDirection];
                    visited.insert(ReverseDirection);
                }
                itr.push_back(t);
            }
        }
    }
    return traffics;
}

void print_stat(ostream &out, Statistics &stat) {
   out << "[PROFILE]: " << stat.covered.size() * 100 / stat.total << "%"
       << " address ranges were covered\n";
   out << "[PROFILE]: " << stat.remapped.size() * 100 / stat.total << "%"
       << " address ranges were remapped\n";

   while(!stat.impact_list.empty()) {
       RangeInfo ri = stat.impact_list.top();
       std::cout << ri.id
           << "[" << ri.start_i
           << ","
           << ri.end_i << "]"
           << " " << ri.impact
           << std::endl;

       stat.impact_list.pop();
   }

   out << "Total_Impact " << stat.impact << std::endl;

   const vector<TP> &timer = stat.timer;
   out << "[PROFILE] : Registering address range takes "
       << duration_cast<milliseconds>(timer[1] - timer[0]).count()
       << " millisecond(s).\n";
   out << "[PROFILE] : Reading trace profile takes     "
       << duration_cast<milliseconds>(timer[2] - timer[1]).count()
       << " millisecond(s).\n";
   out << "[PROFILE] : Locating address range takes    "
       << duration_cast<milliseconds>(timer[3] - timer[2]).count()
       << " millisecond(s).\n";
   out << "[PROFILE] : Searching for optimality takes  "
       << duration_cast<milliseconds>(timer[4] - timer[3]).count()
       << " millisecond(s).\n";
 }

void read_plan(string filename, DomainMap& plan){
    ifstream fin(filename);
    assert(fin.good());
    uint32_t x;
    int count = 0;
    while(fin >> x)
        plan[x] = count++ / 5;
}

void print_plan(ostream &out, DomainMap plan) {
     std::vector<pair<uint32_t, NodeType>> thd;
     std::map<NodeType, uint32_t> counters;
     std::set<NodeType> sockets;
 
     for(auto t : plan) {
         cout << t.first << " " << t.second << std::endl;
         sockets.insert(t.second);
         thd.push_back(make_pair(t.second, t.first));
     }
     sort(thd.begin(), thd.end());

     /*
     //<! FIXME : using system config making plan
     for(auto t : plan) {
         // FIXME change to Power8's core topology
         // thd[t.first] = t.second + sockets.size() * (counters[t.second]++);
        thd[t.first] = t.second * 5 + (counters[t.second]++);
         
     }
     */

     for(auto core : thd) {
         out << core.second << " ";
     }
 }

void read_place(string filename, vector<RangeInfo> &address_placement){
    ifstream fin(filename);
    assert(fin.good());
    RangeInfo x;
    while(fin >> x.id){
        fin >> x.start_i >> x.end_i >> x.node;
        address_placement.push_back(x);
    }
}

void print_place(ostream &out, vector<RangeInfo> address_placement){
    for(auto i : address_placement){
        out << i.id << "\t" << i.start_i << "\t" << i.end_i << " " << i.node << endl;
    }
}

} // namespace Tapas
