#pragma once

#include <ostream>
#include <utility>
#include <set>
#include <map>
#include <vector>
#include <cstdint>

#include "Common/Common.hpp"
#include "Common/Util.hpp"

#include "Traffic/POWER8/POWERSnoopNaive.hpp"

namespace Tapas {
void ProfileTrace(POWERSnoopNaiveBuilder &SnoopBuilder, const std::string &trace, const std::string &pid);
void DumpTraceProfile(POWERSnoopNaiveBuilder &SnoopBuilder, const std::string &filename);
void ReadTraceProfile(const std::string &filename, POWERSnoopNaiveState* &SnoopState);
ThreadPlacement ReadTP(string filename = "tp");
} // namespace Tapas
