#pragma once

#include <ostream>
#include <utility>
#include <set>
#include <map>
#include <vector>
#include <cstdint>

#include "Common/Common.hpp"
#include "Common/Util.hpp"

#include "Traffic/POWER8/POWERSnoop.hpp"
#include "Traffic/POWER8/POWERSnoopProbability.hpp"

namespace Tapas {

typedef POWERSnoopProbabilityState SnoopState;
typedef POWERSnoopProbabilityBuilder SnoopBuilder;

void ProfileTrace(SnoopBuilder& SnoopBuilder, const std::string &trace);
void ProfileTrace(SnoopBuilder &SnoopBuilder, const std::string &trace, const std::string &pid);
void DumpTraceProfile(SnoopBuilder &SnoopBuilder, const std::string &filename);
SnoopState* ReadTraceProfile(const std::string &filename);
ThreadPlacement ReadTP(string filename = "tp");

void SearchForOptimal(SnoopState &psn, SystemConfig &config);
ScoreType getAccessScore(NetworkType<uint64_t, uint64_t> &access, SystemConfig &config);
ScoreType getAccessScore(SnoopState &psn, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config);
ScoreType getScore(SnoopState &psn, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config);
ScoreType getScore(NetworkType<uint64_t, uint64_t> &traffic, SystemConfig &config);
vector<uint64_t> getFilltime(ThreadPlacement &tp, SystemConfig &config);
NetworkType<uint64_t, uint64_t> getTraffic(SnoopState &psn, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config);
NetworkType<uint64_t, uint64_t> getAccess(SnoopState &psn, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config);

void getRandomDataPlacement(DataPlacement &dp, SystemConfig &config);
void getRandomThreadPlacement(ThreadPlacement &tp, SystemConfig &config);
} 
// namespace Tapas
