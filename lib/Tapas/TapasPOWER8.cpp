#include <numeric>  // std::iota
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <functional>
#include <random>

#include "Common/Common.hpp"
#include "Traffic/POWER8/POWERSnoop.hpp"
//#include "Traffic/POWER8/POWERSnoopProbability.hpp"


#include "Trace/TraceReader.hpp"
#include "Common/Util.hpp"

#include "Traffic/TrafficManager.hpp"
#include "Footprint/FootprintManager.hpp"
#include "Trace/TraceManager.hpp"
//#include "Model/ModelManager.hpp"


#include "TapasPOWER8.hpp"

namespace Tapas {

FootprintManager gFpMgr; //!< Global Mgr, Manage footprint histogram
TraceManager gTraceMgr; //!< Global Mgr, read traces from files, deal with the basename, pid and tids

void ProfileTrace(SnoopBuilder& SnoopBuilder, const std::string &trace) {
    string pid = "";
    ProfileTrace(SnoopBuilder, trace, pid);
}

void ProfileTrace(SnoopBuilder& SnoopBuilder, const std::string &trace, const std::string &pid) {
    //cout << "Traffic skipped" << endl;
    cout << "TapasPOWER8 Profiling" << endl;
    std::size_t progress = 0, total;
    std::size_t threshold = 0;
    std::stringstream bar;

    TimeInt tick = 0;
    MEMREF e;
    
    /*! set basenames and suffixes */
    gTraceMgr.setSource(trace, pid);
    gTraceMgr.open();
    cout << "Traces length : " << gTraceMgr.getTotalWork() << endl;
    /*! get total number of accesses */
    total = gTraceMgr.getTotalWork();
    while( gTraceMgr.next(e) ) {
        tick = e.time - gTraceMgr.start + 1;

        AddrInt addr = ( e.address >> 6 ) << 6;
        SnoopBuilder.onReference(addr, e.thread, tick, e.type);
        gFpMgr.refer(addr, e.thread, tick, e.type);
    }
    gTraceMgr.close();

    SnoopBuilder.finish();
    gFpMgr.finish(tick);
}

/*! serialize the profiled trace stats to filename */
void DumpTraceProfile(SnoopBuilder &SnoopBuilder, const std::string &filename) {
    std::ofstream traffic_file;
    std::ofstream footprint_file;

    traffic_file.open(string(filename) + ".traffic",
           std::ios_base::out | std::ios_base::binary);

    footprint_file.open(string(filename) + ".footprint",
           std::ios_base::out | std::ios_base::binary);
    assert(traffic_file.good());
    assert(footprint_file.good());

    SnoopBuilder.serialize(traffic_file);
    gFpMgr.serialize(footprint_file);

    traffic_file.close();
    footprint_file.close();
}

/* deserialize the profiled trace stats */
SnoopState* ReadTraceProfile(const std::string &filename) {

    std::ifstream traffic_file;
    std::ifstream footprint_file;

    traffic_file.open(string(filename) + ".traffic");

    footprint_file.open(string(filename) + ".footprint",
           std::ios_base::in | std::ios_base::binary);

    if ( !traffic_file.is_open() || !footprint_file.is_open() ) {
        std::cerr << "[ERROR] : Can not open serialized traffic/footprint files\n";
        exit(-1);
    }
    
    SnoopState *SnoopState = SnoopState::deserialize(traffic_file);
    FootprintManager::deserialize(footprint_file, &gFpMgr);

    traffic_file.close();
    footprint_file.close();
    return SnoopState;
}


ThreadPlacement ReadTP(string filename){
    ifstream fin(filename);
    assert(fin.good() && "tp file can't open");
    ThreadPlacement tp;
    NodeType c = 0;
    NodeType x;
    while(fin >> x)
        tp.Thread2Core[x] = c++;
    return tp;
}

vector<uint64_t> getFilltime(ThreadPlacement &tp, SystemConfig &config){
    vector<uint64_t> rt;
    for(NodeType i = 0; i < config.nchips; i++){
        vector<NodeType> threads;
        for(auto j : config.Chip2Cores[i])
            threads.push_back(tp.Core2Thread[j]);
        uint64_t ft = gFpMgr.getFilltime(threads, config.LLCCapacity[i]);
        rt.push_back(ft);
    }
    for(NodeType i = 0; i < config.ndcms; i++){
        vector<NodeType> threads;
        uint64_t llc = 0;
        for(auto j : config.DCM2Chips[i]){
            llc += config.LLCCapacity[j];
            for(auto k : config.Chip2Cores[j])
                threads.push_back(tp.Core2Thread[k]);
        }
        uint64_t ft = gFpMgr.getFilltime(threads, llc);
        rt.push_back(ft);
    }
    return rt;
}

//<! given a dp and tp, return the traffic on links
NetworkType<uint64_t, uint64_t> 
getTraffic(SnoopState &psn, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config){
    //<! 1. Prepare fill times
    vector<uint64_t> filltime = getFilltime(tp, config);
    //<! 2. When having fill time, get the traffics
    NetworkType<uint64_t, uint64_t> out;
    psn.getInterconnectTraffic(out, filltime, tp, dp, config);
    return out;
}

NetworkType<uint64_t, uint64_t>
getAccess(SnoopState &psn, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config){
    vector<uint64_t> filltime = getFilltime(tp, config);
    NetworkType<uint64_t, uint64_t> out;
    psn.getInterconnectAccess(out, filltime, tp, dp, config);
    return out;
}

void SearchForOptimal(SnoopState &psn, SystemConfig &config){
    //Prepare an intial placement
    ThreadPlacement bestTP;
    for(NodeType i = 0; i < config.ncores; i++)
        bestTP.MapT2C(i, i);
    

    DataPlacement bestDP;
    ScoreType bestScore = ScoreMAX;
    

    for(int itr = 0; itr < 10; itr++){
        ThreadPlacement tp;
        DataPlacement dp;

        for(NodeType i = 0; i < config.ncores; i++)
           for(NodeType j = i+1; j < config.ncores; j++){           
               NodeType t = tp.Core2Thread[i];
               tp.MapT2C(tp.Core2Thread[j], i);
               tp.MapT2C(t, j);
               ScoreType s = getScore(psn, tp, dp, config);
               if(s < bestScore){
                   bestScore = s;
                   bestTP = tp;
                   bestDP = dp;
               }
           }
    }
    //return best
    cout << bestScore << endl;
    bestTP.serialize(cout);
    bestDP.serialize(cout);
}

ScoreType getAccessScore(SnoopState &psn, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config){
    NetworkType<uint64_t, uint64_t> out = getAccess(psn, tp, dp, config);
    return getAccessScore(out, config);
}

ScoreType getAccessScore(NetworkType<uint64_t, uint64_t> &access, SystemConfig &config){
    vector<uint64_t> accesses(config.nchips, 0);
    for(auto &i : access.link){
        ScopeType s = config.getScope(i.first.first, i.first.second);
        if(!config.isChipScope(s))
            accesses[i.first.first] += i.second;
    }
    uint64_t maxAccess = *max_element(accesses.begin(), accesses.end());
    uint64_t sumAccess = accumulate(accesses.begin(), accesses.end(), (uint64_t)0);
//    return maxAccess*4 + sumAccess*6;
    ScoreType rt = 0;
    for(auto i : access.node){
        rt += i.second;
    }
    return rt;
}

ScoreType getScore(SnoopState &psn, ThreadPlacement &tp, DataPlacement &dp, SystemConfig &config){
    NetworkType<uint64_t, uint64_t> out = getTraffic(psn, tp, dp, config);
    //NetworkType<uint64_t, uint64_t> out = getAccess(psn, tp, dp, config);
    return getScore(out, config);
}

/**
 Given traffic of a plan, give the score of that plan
 traffic.node are the number of memory traffic
 traffic.link are the traffic of interconnections
 Score means the cost, lower is better
*/
ScoreType getScore(NetworkType<uint64_t, uint64_t> &traffic, SystemConfig &config){
    //First : Weight the traffics according to sysmtem configuration
    
    //traffic.serialize(cout);
    vector<uint64_t> linkTraffic(config.nlinks, 0);
    for(auto &i : traffic.link){
        cout << i.first.first << " " << i.first.second << " " << i.second << endl;
        uint32_t lid = config.LinkID[i.first]; 
        i.second /= config.LinkBandwidth[lid];
        i.second *= config.LinkLatency[lid];
        linkTraffic[lid] += i.second;
    }
    for(uint32_t i = 0; i < config.LinkIsBidirectional.size(); i++)
        if(config.LinkIsBidirectional[i])
            linkTraffic[i] /= 2;

    vector<uint64_t> nodeTraffic(config.nchips, 0);
    for(auto &i : traffic.node){
        i.second /= config.LLC2MBandwidth[i.first];
        i.second *= config.LLC2MLatency[i.first];
        nodeTraffic[i.first] += i.second;
    }
    //Second : Evaluate the traffic
    ScoreType maxlink = *max_element(linkTraffic.begin(), linkTraffic.end());
    ScoreType sumlink = accumulate(linkTraffic.begin(), linkTraffic.end(), (ScoreType)0);
    cout << maxlink << " " << sumlink << endl;

    //size_t pos = find(linkTraffic.begin(), linkTraffic.end(), x) - linkTraffic.begin();
    //cout << "Max : " << pos << endl;
    //x = max(x, *max_element(nodeTraffic.begin(), nodeTraffic.end()));
    
    return maxlink+sumlink/10;
}

void getRandomDataPlacement(DataPlacement &dp, SystemConfig &config){
    dp.places.resize(dp.range->names.size());

    random_device rdevice;
    default_random_engine generator(rdevice());
    uniform_int_distribution<NodeType> distribution(0, config.nchips-1);
    auto dice = bind ( distribution, generator );

    uint64_t cap[config.nchips];
    for(int i = 0; i < config.nchips; i++)
        cap[i] = config.MemCapacity[i];

    for(int i = 0; i < dp.places.size(); i++){
        NodeType x = dice();
        auto &r = dp.range->ranges[i];
        if(r.second - r.first > cap[x]){
            --i;
            continue;
        }
        cap[x] -= r.second - r.first;
        dp.places[i] = x;
    }
}

void getRandomThreadPlacement(ThreadPlacement &tp, SystemConfig &config){
    vector<NodeType> x;
    for(int i = 0; i < config.ncores; i++)
        x.push_back(i);
    random_shuffle(x.begin(), x.end());
    for(int i = 0; i < config.ncores; i++)
        tp.MapT2C(i, x[i]);
}

} // namespace Tapas
