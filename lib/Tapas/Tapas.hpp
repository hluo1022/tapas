#pragma once

#include <ostream>
#include <utility>
#include <set>
#include <map>
#include <vector>
#include <cstdint>

#include "Common/Common.hpp"
#include "ConfigureHelper.hpp"
#include "Common/Util.hpp"

namespace Tapas {

enum ModelType {
    GradientDescentModel = 0,
    PlainScoreModel
};

extern bool EnableProgressBar;


class RawGraph {
public:
    std::set<uint32_t> node;
    std::set<std::pair<uint32_t, uint32_t> > edge;
    void AddNode(uint32_t n);
    void AddEdge(uint32_t n1, uint32_t n2);
};

void RegisterAddressRange(uint64_t start,
                          uint64_t end);

void LocateAddressRange(uint64_t start,
                        uint64_t end,
                        uint32_t home);

void AddRawGraphNode(RawGraph &g, uint32_t n);
void AddRawGraphEdge(RawGraph &g, uint32_t n1, uint32_t n2);

void RegisterSearchModel( ModelType m );
void RegisterGraph(RawGraph &g);

void ProfileTrace(const std::string &trace, const std::string &pid, const std::vector<uint32_t> &group);

template<typename SnoopBuilder>
void ProfileTrace(const std::string &trace, const std::string &pid, const std::vector<uint32_t> &group, SnoopBuilder &x);

void DumpTraceProfile(const std::string &output);
void ReadTraceProfile(const std::string &input);

void PrintScore(std::ostream &out, const SystemConfig &config, DomainMap &plan);
ScoreType GetScore(const SystemConfig &config, DomainMap &opt);

/* main routines for searching for the optimal placement
 * SearchForOptimal: input a thread placement plan, output its score
 */
ScoreType SearchForOptimal(const SystemConfig &config, DomainMap &opt, bool incremental);
std::vector<L2DVector<ScoreType>> GetScoreOnM2MLinks(const SystemConfig &config, DomainMap &plan, std::map<uint64_t, RangeInfo> regions);
std::vector<L2DVector<ScoreType>> GetScoreOnC2MLinks(const SystemConfig &config, DomainMap &plan, std::map<uint64_t, RangeInfo> regions);
std::vector<L2DVector<ScoreType>> GetScoreOnAllLinks(const SystemConfig &config, DomainMap &plan, std::map<uint64_t, RangeInfo> regions);


void print_stat(ostream &out, Statistics &stat);
void print_plan(ostream &out, DomainMap plan);
void read_plan(string filename, DomainMap& plan);
void print_place(ostream &out, vector<RangeInfo> address_placement);
void read_place(string filename, vector<RangeInfo> &address_placement);
bool SearchForDP(vector<RangeInfo> &place, SystemConfig &config, DomainMap &plan, ScoreType &best, Statistics &stat, int iterations = 1, int FrequentDump = 0);

} // namespace Tapas
