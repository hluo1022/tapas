#include <numeric>  // std::iota
#include <iostream>
#include <iomanip>

#include "Common/Common.hpp"
#include "Traffic/POWER8/POWERSnoopNaive.hpp"

#include "Trace/TraceReader.hpp"
#include "Common/Util.hpp"

#include "Traffic/TrafficManager.hpp"
#include "Footprint/FootprintManager.hpp"
#include "Trace/TraceManager.hpp"
#include "Model/ModelManager.hpp"
#include "general.hpp"

#include "Tapas.hpp"

namespace Tapas {

FootprintManager gFpMgr; //!< Global Mgr, Manage footprint histogram
TraceManager gTraceMgr; //!< Global Mgr, read traces from files, deal with the basename, pid and tids

void ProfileTrace(POWERSnoopNaiveBuilder &SnoopBuilder, const std::string &trace, const std::string &pid) {
    std::size_t progress = 0, total;
    std::size_t threshold = 0;
    std::stringstream bar;

    TimeInt tick = 0;
    MEMREF e;
    
    /*! set basenames and suffixes */
    gTraceMgr.setSource(trace, pid);
    gTraceMgr.open();
    /*! get total number of accesses */
    total = gTraceMgr.getTotalWork();
    while( gTraceMgr.next(e) ) {
        tick = e.time - gTraceMgr.start + 1;

        AddrInt addr = ( e.address >> 6 ) << 6;
        //SnoopBuilder.onReference(addr, e.thread, tick, e.type);
        gFpMgr.refer(addr, e.thread, tick, e.type);
    }
    gTraceMgr.close();

    //SnoopBuilder.finish();
    gFpMgr.finish(tick);
}

/*! serialize the profiled trace stats to filename */
void DumpTraceProfile(POWERSnoopNaiveBuilder &SnoopBuilder, const std::string &filename) {
    std::ofstream traffic_file;
    std::ofstream footprint_file;

    traffic_file.open(string(filename) + ".traffic",
           std::ios_base::out | std::ios_base::binary);

    footprint_file.open(string(filename) + ".footprint",
           std::ios_base::out | std::ios_base::binary);

    if ( !traffic_file.is_open() || !footprint_file.is_open() ) {
        std::cerr << "[ERROR] : Can not open serialized traffic/footprint files\n";
        exit(-1);
    }

    //SnoopBuilder.serialize(traffic_file);
    gFpMgr.serialize(footprint_file);

    traffic_file.close();
    footprint_file.close();
}

/* deserialize the profiled trace stats */
void ReadTraceProfile(const std::string &filename, POWERSnoopNaiveState* &SnoopState) {

    std::ifstream traffic_file;
    std::ifstream footprint_file;

    traffic_file.open(string(filename) + ".traffic");

    footprint_file.open(string(filename) + ".footprint",
           std::ios_base::in | std::ios_base::binary);

    if ( !traffic_file.is_open() || !footprint_file.is_open() ) {
        std::cerr << "[ERROR] : Can not open serialized traffic/footprint files\n";
        exit(-1);
    }

    SnoopState = POWERSnoopNaiveState::deserialize(traffic_file);
    FootprintManager::deserialize(footprint_file, &gFpMgr);

    traffic_file.close();
    footprint_file.close();
}


ThreadPlacement ReadTP(string filename = "tp"){
    ifstream fin(filename);
    assert(fin.good() && "tp file can't open");
    ThreadPlacement tp;
    NodeType c = 0;
    NodeType x;
    while(fin >> x)
        tp.Thread2Core[x] = c++;
    return tp;
}

} // namespace Tapas
