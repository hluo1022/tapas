#pragma once

#include "Common/Common.hpp"
#include "Common/Histo.hpp"
#include "Common/Util.hpp"

//#include <iostream>
#include <unordered_map>
#include <string.h>

namespace Tapas {

//============================================================//
class FootprintState {

    friend class FootprintBuilder;
    friend class SharedFootprintBuilder;

public:

    typedef histo::Histogram<float>    Curve;

//private:
public:
    std::size_t  volume;
    std::size_t  window;
    TimeInt      length;
    Curve  fp;

public:

    FootprintState() : volume(0), window(1), length(1)
    {}

    FootprintState& operator=(const FootprintState& other) {
        volume = other.volume; 
        length = other.length; 
        window = other.window;

        fp = other.fp;

        return *this;
    }
    FootprintState& operator+=(const FootprintState& other) {
        if (length == 1 && other.length != 1) {
            length = other.length;
            window = other.window;
        }
        volume += other.volume;
        fp += other.fp;

        return *this;
    }

    FootprintState& operator-=(const FootprintState& other) {
        if (length == 1 && other.length != 1) {
            length = other.length;
            window = other.window;
        }
        volume -= other.volume;
        fp -= other.fp;

        return *this;
    }

    const FootprintState operator+(const FootprintState& other) const {
        FootprintState result = *this;
        result += other;
        return result;
    }

    const FootprintState operator-(const FootprintState& other) const {
        FootprintState result = *this;
        result -= other;
        return result;
    }


    inline float getFootprint(std::size_t index) const { return fp[index]; }
    std::size_t getWindowSize(float footprint);

    void print(std::ostream& out);

    void serialize(std::ostream& out);
    static FootprintState* deserialize(std::ifstream& in);
};

//============================================================//
class FootprintBuilder {

public:

    typedef std::unordered_map<AddrInt, TimeInt> History;

private:

    FootprintState* FS;
    History history;

    histo::Histogram<TimeInt> reuses;
    histo::Histogram<TimeInt> reuses_i;

public:
    FootprintBuilder(FootprintState* _fs) 
              : FS(_fs)
    {}
  
    void onReference(AddrInt address, TimeInt tick);
    void onExit(TimeInt tick);

    void calculateFootprint();

};

//============================================================//
class SharedFootprintBuilder {

public:

    typedef std::unordered_map<AddrInt, Record> History;

private:

    FootprintState* FS;
    History history;

    histo::Histogram<TimeInt> reuses;
    histo::Histogram<TimeInt> reuses_i;

public:

    SharedFootprintBuilder(FootprintState* _fs) 
        : FS(_fs)
    {}

    void onReference(unsigned accessor,     // accessor who makes this reference
                     AddrInt address,       // target address
                     TimeInt tick           // reference time
                     );

    void onExit(TimeInt end);
    void calculateSharedFootprint();

};

} // namespace Tapas
