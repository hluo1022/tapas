#pragma once

#include <set>
#include <unordered_map>
#include <utility>
#include <ostream>
#include <fstream>

#include "Common/Common.hpp"
#include "Common/Util.hpp"
#include "Footprint/Footprint.hpp"

namespace Tapas {

class FootprintManager {

private:

    std::unordered_map<AddrInt, Record> history;

    std::unordered_map<LinkType, FootprintState*, std::uintpairhash> mLinkState;
    std::unordered_map<NodeType, FootprintState*> mNodeState;

    std::unordered_map<LinkType, SharedFootprintBuilder*, std::uintpairhash> mLinkBuilder;
    std::unordered_map<NodeType, FootprintBuilder*> mNodeBuilder;

private:

    FootprintBuilder* getNodeBuilder(NodeType node);

    SharedFootprintBuilder* getLinkBuilder(LinkType link);

public:

    inline LinkType makelink(NodeType src, NodeType dst) {
        return src<dst ? std::make_pair(src,dst) : std::make_pair(dst,src);
    }

    ~FootprintManager();

    void serialize(std::ostream &OS);
    static void deserialize(std::ifstream &IS, FootprintManager *fm);

    /// profile
    void refer(AddrInt addr,       // target address
               unsigned accessor,  // accessor
               TimeInt tick,       // time
               bool type);         // access type 'R'/'W'
    void finish(TimeInt tick);

    /// model 
    FootprintState* getNodeState(NodeType node);
    FootprintState* getLinkState(LinkType link);

    template<typename T, typename U>
    FootprintState* getFootprint(const T &nodes, 
                                 const U &links) {
        FootprintState *fs = new FootprintState;
        for(auto n : nodes) {
            if(FootprintState *nfs = getNodeState(n)) {
                *fs += *nfs;
                
                if(n == 20){
                    for(int i = 0; i < MAX_WINDOW; i++)
                        cout << nfs->fp[i] << " ";
                    cout << endl;
                }
                
            }
        }
        for(auto l : links) {
            if(FootprintState *lfs = getLinkState(l)) {
                *fs -= *lfs;
            }
        }
        return fs;
    }

    template<typename T, typename U>
    std::size_t getFilltime(const T &nodes,
                            const U &links,
                            float capacity) {
        FootprintState* fs = getFootprint(nodes, links);
        std::size_t ft = fs->getWindowSize(capacity);
        delete fs;
        return ft;
    }

    template<typename T>
    std::size_t getFilltime(const T &nodes, float capacity){
        vector<LinkType> links;
         
        for(auto i : nodes)
            for(auto j : nodes){
                if(i < j)
                    links.push_back(LinkType(i, j));
            }
        
        return getFilltime(nodes, links, capacity);
    }

};

} // namespace Tapas
