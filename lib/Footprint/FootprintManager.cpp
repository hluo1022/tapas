#include "FootprintManager.hpp"

namespace Tapas {

FootprintManager::~FootprintManager() {
    for(auto i : mLinkState) {
        FootprintState *fs = i.second;
        delete fs;
    }
    for(auto i : mNodeState) {
        FootprintState *fs = i.second;
        delete fs;
    }
}

SharedFootprintBuilder*
FootprintManager::getLinkBuilder(LinkType link) {
    SharedFootprintBuilder *sfb;
    if (mLinkBuilder.find(link) == mLinkBuilder.end()) {
        FootprintState *fs = new FootprintState;
        mLinkState[link] = fs;
        sfb = new SharedFootprintBuilder(fs);
        mLinkBuilder[link] = sfb;
    }
    else {
        sfb = mLinkBuilder[link];
    }
    return sfb;
}

FootprintBuilder*
FootprintManager::getNodeBuilder(NodeType node) {
    FootprintBuilder *fb;
    if (mNodeBuilder.find(node) == mNodeBuilder.end()) {
        FootprintState *fs = new FootprintState;
        mNodeState[node] = fs;
        fb = new FootprintBuilder(fs);
        mNodeBuilder[node] = fb;
    }
    else {
        fb = mNodeBuilder[node];
    }
    return fb;
}

void FootprintManager::refer(AddrInt addr, unsigned accessor,
                            TimeInt tick, bool type) {

    /// update history table
    Record r = history[addr];

    if ( r.time[0] != 0 ) {
        if ( r.time[1] != 0 ) {
            // assert( accessor == r.accessor[0] || accessor == r.accessor[1] );
            LinkType link = makelink(r.accessor[0], r.accessor[1]);
            SharedFootprintBuilder *sfb = getLinkBuilder(link);
            sfb->onReference(accessor, addr, tick);
        }
        else {
            if ( r.accessor[0] == accessor ) {
                r.time[0] = tick;
            }
            else {
                LinkType link = makelink(r.accessor[0], accessor);
                SharedFootprintBuilder *sfb = getLinkBuilder(link);
                sfb->onReference(r.accessor[0], addr, r.time[0]);
                sfb->onReference(accessor, addr, tick);

                r.accessor[1] = accessor;
                r.time[1] = tick;
            }
        } 
    }
    else {
        r.accessor[0] = accessor;
        r.time[0] = tick;
    }

    history[addr] = r;

    FootprintBuilder* fb = getNodeBuilder(accessor);
    fb->onReference(addr, tick);

}

void FootprintManager::finish(TimeInt tick) {

    for(auto i : mLinkBuilder) {
        SharedFootprintBuilder *fb = i.second; 
        fb->onExit(tick);
        fb->calculateSharedFootprint();
        delete fb;
    }
    for(auto i : mNodeBuilder) {
        FootprintBuilder *sfb = i.second;
        sfb->onExit(tick);
        sfb->calculateFootprint();
        delete sfb;
    }

}

FootprintState* FootprintManager::getNodeState(NodeType node) {
    if (mNodeState.find(node) != mNodeState.end()) {
        return mNodeState[node];
    }
    return nullptr;
}

FootprintState* FootprintManager::getLinkState(LinkType link) {
    if (mLinkState.find(link) != mLinkState.end()) {
        return mLinkState[link];
    }
    return nullptr;
}

void FootprintManager::serialize(std::ostream &OS) {
    std::size_t s = mNodeState.size();
    OS.write((char*)&s, sizeof(std::size_t));
    for(auto i : mNodeState) {
        OS.write((char*)&i.first, sizeof(NodeType));
        i.second->serialize(OS);
    }
    s = mLinkState.size();
    OS.write((char*)&s, sizeof(std::size_t));
    for(auto i : mLinkState) {
        OS.write((char*)&i.first.first, sizeof(NodeType));
        OS.write((char*)&i.first.second, sizeof(NodeType));
        i.second->serialize(OS);
    }
}

void FootprintManager::deserialize(std::ifstream &IS, FootprintManager *FM) {
    std::size_t s;
    IS.read((char*)&s, sizeof(std::size_t));
    for(std::size_t i = 0; i < s; ++i) {
        NodeType n;
        IS.read((char*)&n, sizeof(NodeType));
        FootprintState *fp;
        fp = FootprintState::deserialize(IS);
        FM->mNodeState[n] = fp;
    }
    IS.read((char*)&s, sizeof(std::size_t));
    for(std::size_t i = 0; i < s; ++i) {
        LinkType l;
        IS.read((char*)&l.first, sizeof(NodeType));
        IS.read((char*)&l.second, sizeof(NodeType));
        FootprintState *fp;
        fp = FootprintState::deserialize(IS);
        FM->mLinkState[l] = fp;
    }
}

} // namespace Tapas
