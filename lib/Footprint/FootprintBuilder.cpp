#include "Footprint.hpp"

namespace Tapas {

//============================================================//
void FootprintBuilder::onReference(
                    AddrInt addr,     // target address
                    TimeInt tick      // logic time of this reference
                  ) {

    TimeInt t = history[addr]; //H.find(addr) ? H.get(addr) : 0;
    std::size_t idx;

    assert(tick > t && "[ERROR] references are reordered.");

    TimeInt dist = tick - t - 1;
    idx  = histo::sublog_value_to_index(dist);

    reuses[idx] ++;
    reuses_i[idx] += dist;

    if ( t == 0 )
        FS->volume++;

    history[addr] = tick;
}

void FootprintBuilder::onExit(TimeInt end) {

    FS->length = end;
    FS->window = histo::sublog_value_to_index(end);

    for(auto& e : history) {
        auto& t = e.second;
        TimeInt dist = end - t;
        std::size_t idx = histo::sublog_value_to_index(dist);
        reuses[idx] ++;
        reuses_i[idx] += dist;
    }
}

void FootprintBuilder::calculateFootprint() {

    TimeInt sum = 0, sum_i = 0;

    // calcuate footprint
    for(std::size_t j = FS->window; j > 0; j--) {
        std::size_t ws = histo::sublog_index_to_value(j);
        sum_i += reuses_i[j];
        sum   += reuses[j];
        float fp = 1.0 * (sum_i - sum * (ws - 1)) / (FS->length - ws + 1);
        FS->fp[j] = FS->volume - fp;
    }

}

//============================================================//
void SharedFootprintBuilder::onReference(
                   unsigned accessor,     // accessor who makes this reference
                   AddrInt addr,          // target address
                   TimeInt tick           // logic time of this reference
                  ) {

    // get the base address of addr's cacheline
    //AddrInt addr = (raw_address / granularity) * granularity;

    bool first = history.find(addr) == history.end();
    if ( !first ) {
        Record r = history[addr];
        if (r.accessor[0] != accessor) {

            if ( r.time[1] == 0) {
                FS->volume++;
            }

            TimeInt dist;
            std::size_t idx;

            /// TODO accessor[1] should be accessor
            dist = tick - r.time[1] - 1;
            idx = histo::sublog_value_to_index(dist);

            reuses[idx] ++;
            reuses_i[idx] += dist;

            dist = tick - r.time[0] - 1;
            idx = histo::sublog_value_to_index(dist);

            reuses[idx] --;
            reuses_i[idx] -= dist;

            /// update history
            r.accessor[1] = r.accessor[0];
            r.time[1] = r.time[0];
            r.accessor[0] = accessor;
            r.time[0] = tick;
        }
        else {
            r.time[0] = tick;
        }
        history[addr] = r;        
    }
    else {
        Record r;
        r.time[0] = tick;
        r.accessor[0] = accessor;
        history[addr] = r;
    }
}

void SharedFootprintBuilder::onExit(TimeInt end) {
    FS->length = end;
    FS->window = histo::sublog_value_to_index(end);

    for(auto e : history) {
        Record r = e.second;

        if ( r.time[1] != 0) {
            TimeInt dist;
            std::size_t idx;

            dist = end - r.time[1];
            idx = histo::sublog_value_to_index(dist);
            reuses[idx] ++;
            reuses_i[idx] += dist;
        }
    }
}

void SharedFootprintBuilder::calculateSharedFootprint() {
    TimeInt sum = 0;
    TimeInt sum_i = 0;

    for(std::size_t i = FS->window; i > 0; --i) {
        std::size_t ws = histo::sublog_index_to_value(i);
        sum_i += reuses_i[i];
        sum   += reuses[i];
        float fp = 1.0 * (sum_i - sum * (ws - 1)) / (FS->length - ws + 1);
        FS->fp[i] = FS->volume - fp;
    }
}


} // namespace Tapas
