/*
 * Footprint.cpp
 *
 * More detail about the footprint method can be 
 * found in LOCA tool
 *
 *   https://github.com/dcompiler/loca
 *
 * The reuse distance is implemented using map
 * which requires a compilation flag '-std=c++11'
 * Using map is also ok. The implementation maintains
 * a combination of stack and hashmap. It also has a 
 * static marker array, whose entry points to an entry
 * in the stack of a particular depth.
 *
 * Rewritten by Hao Luo
 *
 */

#include <fstream>
#include <iomanip>
#include "Footprint.hpp"
#include "Common/Histo.hpp"

//using namespace std;

namespace Tapas {

std::size_t FootprintState::getWindowSize(float target) {
    if ( target > volume ) return histo::sublog_index_to_value(window);
    if ( target <= 0 ) return 0;

    float footprint1, footprint2;

    // binary search
    std::size_t low = 0, high = window, mid = 0;
    while ( low <= high ) {
        mid = low + (high - low) / 2;
        footprint1 = fp[mid];
        footprint2 = fp[mid+1];
        if ( target + 1 >  footprint1 && 
             target + 1 <= footprint2 ) {
            break;
        }
        else if ( target + 1 <= footprint1 ) {
            high = mid - 1;
        }
        else {
            low = mid + 1;
        }
    }
    
    return histo::sublog_index_to_value(mid);
//    return fp[mid];
}

void FootprintState::print(std::ostream& OS) {

    OS << "N : " << length << "\n";
    OS << "M : " << volume << "\n";

    OS << "ws\t(s)fp\n";

    for(std::size_t i=1; i<=window; i++){
        std::size_t ws = histo::sublog_index_to_value(i);
        OS << ws;
        OS << "\t" << std::setw(10) << std::setprecision(2) << std::fixed << fp[i];
        OS << std::endl;
    }
}

void FootprintState::serialize(std::ostream& OS) {
    OS.write((char*)&length,      sizeof(TimeInt));
    OS.write((char*)&window,      sizeof(std::size_t));
    OS.write((char*)&volume,      sizeof(std::size_t));
    OS.write((char*)(fp.value),   sizeof(float) * MAX_WINDOW);
} 

FootprintState*
FootprintState::deserialize(std::ifstream& IS) {
    FootprintState* obj = new FootprintState;
    IS.read((char*)&(obj->length),    sizeof(TimeInt));
    IS.read((char*)&(obj->window),    sizeof(std::size_t));
    IS.read((char*)&(obj->volume),    sizeof(std::size_t));
    IS.read((char*)(obj->fp.value),   sizeof(float) * MAX_WINDOW);
    return obj; 
}

} // namespace Tapas

