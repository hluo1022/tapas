#pragma once

#include<cstdint>
#include<cstring>
#include<cassert>
#include<iomanip>

#include "Common.hpp"

using namespace std;
namespace histo 
{
    template<typename T>
    class Histogram {
    public:
        T value[MAX_WINDOW];
        
        static Histogram* deserialize(istream &is){
            Histogram<T> *rt = new Histogram<T>();
            is.read((char*)(rt->value), MAX_WINDOW*sizeof(T));
            return rt;
        }

        void serialize(ostream &os){
            os.write((char*)value, MAX_WINDOW*sizeof(T));
        }
        Histogram() {
            memset(value, 0, sizeof(T)*MAX_WINDOW);
        }

        T& operator[](std::size_t idx)       { return value[idx]; };
        const T& operator[](std::size_t idx) const { return value[idx]; };

        Histogram<T>& operator=(const Histogram<T>& other) {
            memcpy(value, other.value, sizeof(T)*MAX_WINDOW);
            return *this;
        }

        Histogram<T>& operator+=(const Histogram<T>& other) {
            for(std::size_t i = 0; i < MAX_WINDOW; ++i) 
                value[i] += other[i];
            return *this;
        }

        const Histogram<T> operator+(const Histogram<T>& other) const {
            Histogram<T> result = *this;
            result += other;
            return result;
        }

        Histogram<T>& operator-=(const Histogram<T>& other) {
            for(std::size_t i = 0; i < MAX_WINDOW; ++i) 
                value[i] -= other[i];
            return *this;
        }

        const Histogram<T> operator-(const Histogram<T>& other) const {
            Histogram<T> result = *this;
            result -= other;
            return result;
        }
    };

//    template<uint32_t BUCKETS, uint32_t SUBLOG_BITS>
    inline uint32_t sublog_value_to_index (uint64_t value) {
    if (value < (1<<SUBLOG_BITS))
        return value;

    unsigned index;
    int msb, shift;

    msb = 63 - __builtin_clzll(value);
    shift = msb - SUBLOG_BITS;
    index = value >> shift;
    index &= (1<<SUBLOG_BITS) - 1;
    index = ((shift + 1) << SUBLOG_BITS) + index;
    return index;
    }

    inline uint64_t sublog_index_to_value (uint32_t index) {
    uint32_t shift = index >> SUBLOG_BITS;
    uint32_t t     = index & ((1<<SUBLOG_BITS) - 1);
    
    if (!shift)
        return index;
    else 
        return (uint64_t)((1<<SUBLOG_BITS) + t) << (shift - 1);
    }

    // An example use of the histogram template:
    // 
    // const  uint32_t              SUBLOG_BITS = 8;
    // const  uint32_t              HIST_BUCKETS = (65-SUBLOG_BITS)*(1<<SUBLOG_BITS);
    // histogram<HIST_BUCKETS, sublog_value_to_index<HIST_BUCKETS, SUBLOG_BITS>, sublog_index_to_value<HIST_BUCKETS, SUBLOG_BITS> >  
    //                            reuse_time_hist;

}

