#pragma once

#include <string>
#include <cstdint>
#include <utility>
#include <functional>
#include <map>
#include <set>
#include <vector>
#include <queue>
#include <chrono>
#include <iostream>
#include <random>

#include "ConfigureHelper.hpp"
#include "type.h"

namespace std {
    struct uintpairhash {
        std::size_t operator()(const std::pair<uint32_t, uint32_t> &x) const
        {
            return (std::hash<uint32_t>()(x.first) << 32) +
                   (std::hash<uint32_t>()(x.second));
        }
    };
} // namespace std

namespace Tapas {

//====================================//
inline LinkType makelink(NodeType src, NodeType dst) {
    return std::make_pair(src, dst);
}

inline LinkType reverselink(LinkType link) {
    return std::make_pair(link.second, link.first);
}

/*! Network connections and nodes */
template<typename T, typename U>
class NetworkType {
    typedef std::map<NodeType, T> NodeMap;
    typedef std::map<LinkType, U> LinkMap;
public:
    NodeMap node; //!< map node to node
    LinkMap link; //!< Map from node to something
    void clear(){
        node.clear();
        link.clear();
    }
    void serialize(ostream &os){
        for(auto i : node)
            os << i.first << " " << i.second << "\n";
        os << "\n";

        for(auto i : link)
            os << i.first.first << " " << i.first.second << "\t" << i.second << "\n";
    }
};

class RangeInfo {
public:
    /* array name */
    std::string id;

    AddrInt start, end; //!< memory address space [start,end)

    uint32_t start_i, end_i; //!< index of the array for the space a[start_i...end_i]

    NodeType node; //!< which node this chunk is allocated to

    /* impact is the largest difference this region could have
     * by switching homes */
    ScoreType impact;
    size_t GetSize(){
        return end-start+1;
    }
};

} // namespace Tapas


using namespace Tapas;
using namespace std::chrono;
using namespace std;

namespace std {

template<>
struct less<RangeInfo>
{
    bool operator() (const RangeInfo& lhs, const RangeInfo& rhs) const
    {
        return lhs.impact >= rhs.impact;
    }
};

}

struct Statistics {

    /* total regions to be examined */
    unsigned int total;

    /* examined regions */
    std::set<unsigned long long> covered;

    /* regions examined and remapped */
    std::set<unsigned long long> remapped;

    /* time spent for each task */
    vector<TP> timer;

    /* priority queue to get a ranked list of the regions */
    std::priority_queue<RangeInfo> impact_list;
    unsigned int    impact;
};

class ScopeTraffic{
public:
    map<NodeType, uint64_t> CTraffic;
    map<NodeType, uint64_t> DTraffic;

    //<! STraffic is recording the existence of the data
    map<pair<NodeType, uint64_t>, uint64_t> STraffic;
    uint64_t boardcast;
    map<AddrInt, vector<uint64_t>> MemRequest;

    ScopeTraffic(){ boardcast = 0;}
    void clear(){
        CTraffic.clear();
        DTraffic.clear();
        STraffic.clear();
        boardcast = 0;
        MemRequest.clear();
    }
    void print(ostream &out){
        out << "CTraffic:\n";
        for(auto x : CTraffic)
            out << "\t" << x.first << " " << x.second << "\n";
        out << "DTraffic:\n";
        for(auto x : DTraffic)
            out << "\t" << x.first << " " << x.second << "\n";
        out << "STraffic:\n";
        for(auto x : STraffic)
            out << "\t" << x.first.first << "," << x.first.second << " " << x.second << "\n";
        out << "Boardcast:\n\t" << boardcast << "\n";
        out << "MemRequest:\n";
        for(auto x : MemRequest){
            out << "\t" << x.first;
            for(auto y : x.second)
                out << " " << y;
            out << "\n";
        }
    }
};

template<typename T>
uint64_t Vector2Mask(vector<T> x){
    uint64_t rt = 0;
    for(auto i : x)
        rt |= 1ull << i;
    return rt;
}

class ThreadPlacement{
public:
    map<NodeType, NodeType> Thread2Core;
    map<NodeType, NodeType> Core2Thread;

    ThreadPlacement(){}

    void MapT2C(NodeType t, NodeType c){
        Thread2Core[t] = c;
        Core2Thread[c] = t;
    }
    void serialize(ostream &out){
        string prefix = "{";
        for(auto x : Thread2Core){
            out << prefix << x.second*8 << "}";
            prefix = ",{";
        }
        out << "\n";
    }

    void serialize(string fname){
        ofstream fout(fname);
        assert(fout.good());
        serialize(fout);
        fout.close();
    }

    static ThreadPlacement* deserialize(istream &is){
        ThreadPlacement* rt = new ThreadPlacement();
        NodeType x;
        NodeType c = 0;
        while(is.ignore(256,'{')){
            if(!is.good()) break;
            is >> x;
            rt->MapT2C(c++, x/8);
        }
        return rt;
    }

    static ThreadPlacement* deserialize(string fname){
        ifstream fin(fname);
        assert(fin.good());
        ThreadPlacement* rt = ThreadPlacement::deserialize(fin);
        fin.close();
        return rt;
    }

};

class MemRange{
public:
    map<AddrInt, int> start2id;
    vector<RangeType> ranges; //memory address
    vector<string> names;
    vector<RangeType> intervals; //array id

    size_t size(){
        return ranges.size()+1;
    }

    int getID(AddrInt addr){
        auto itr = start2id.lower_bound(addr);
        if(itr == start2id.end()) return NOTFOUND;
        if(ranges[itr->second].second > addr)
            return itr->second;
        return NOTFOUND;
    }

    void serialize(ostream &os){
        for(int i = 0; i < ranges.size(); i++){
            os << names[i] << "\t" << hex << ranges[i].first << "\t" << ranges[i].second << "\t" \
                << dec << intervals[i].first << "\t" << intervals[i].second << "\n";
        }
    }

    bool deserializeEntry(istream &is){
        string name;
        if(!(is >> name)) return false;
        RangeType x;
        is >> hex >> x.first >> x.second;
        start2id[x.first] = intervals.size();
        names.push_back(name);
        ranges.push_back(x);
        is >> dec >> x.first >> x.second;
        intervals.push_back(x);
        return true;
    }

    static MemRange* deserialize(istream &is){
        MemRange *rt = new MemRange();
        string x;
        while(is >> x){
            rt->names.push_back(x);
            RangeType y;
            is >> hex >> y.first >> y.second;
            rt->ranges.push_back(y);
            is >> dec >> y.first >> y.second;
            rt->intervals.push_back(y);
        }
        for(int i = 0; i < rt->ranges.size(); i++)
            rt->start2id[rt->ranges[i].first] = i;
        return rt;
    }

    static MemRange* deserialize(string fname){
        ifstream fin(fname);
        assert(fin.good() && "MemRange file error");
        MemRange* rt = MemRange::deserialize(fin);
        fin.close();
        return rt;
    }
};

class DataPlacement{
public:
    MemRange *range;
    vector<NodeType> places;

    size_t size(){
        return places.size();
    }

    DataPlacement(MemRange *_range = NULL){
        range = _range;
        places = vector<NodeType>(range->ranges.size(), NodeINF);
    }

    NodeType getPlacement(AddrInt addr){
        int id = range->getID(addr);
        if(id == NOTFOUND) return NodeINF;
        return places[id];
    }

    static DataPlacement* deserialize(istream &is){
        MemRange *ranges = new MemRange();
        DataPlacement *rt = new DataPlacement(ranges);
        string name;
        while(is >> name){
            ranges->names.push_back(name);
            RangeType interval;

            is >> interval.first >> interval.second;
            ranges->intervals.push_back(interval);

            NodeType x;
            is >> x;
            rt->places.push_back(x);
        }
        return rt;
    }

    static DataPlacement* deserialize(string fname){
        ifstream fin(fname);
        assert(fin.good());
        DataPlacement* rt = DataPlacement::deserialize(fin);
        fin.close();
        return rt;
    }

    void serialize(ostream &os){
        for(int i = 0; i < places.size(); i++){
            os << range->names[i] << "\t" << range->intervals[i].first << "\t" << range->intervals[i].second << "\t" << places[i] << "\n";
        }
    }
    void serialize(string fname){
        ofstream fout(fname);
        assert(fout.good());
        serialize(fout);
        fout.close();
    }
};

class InternalTraffic{
public:
    map<LinkType, uint64_t> access;
    vector<uint64_t> miss[128];
    InternalTraffic(uint64_t nregions){
      for(int i = 0; i < 128; i++)
        miss[i].resize(nregions+1, 0);
    }
};

 class Record {
 public:
        AddrInt addr;
        NodeType accessor;
        TimeInt  time;
        bool type;

        void serialize(ostream &os){
            os << addr << " " << accessor << " " << time << " " << type << "\n";
        }
        void deserialize(istream &is){
            is >> addr >> accessor >> time >> type;
        }

        Record(){}
        Record(AddrInt _addr, NodeType _accessor, TimeInt _time, bool _type) : addr(_addr), accessor(_accessor), time(_time), type(_type){}
 };
