#pragma once

#include <map>
#include <vector>
#include <algorithm>

namespace Tapas {

template<typename T>
class AddressRangeTable {
public:
    std::map<AddrInt, T> data;
    std::map<AddrInt, AddrInt> end2start;

    typedef typename std::map<AddrInt, T>::iterator iterator;
    typedef typename std::map<AddrInt, T>::const_iterator const_iterator;
    void insert(AddrInt start, AddrInt end, T info) {
        data[end] = info;
        end2start[end] = start;
    }

    bool exist(AddrInt addr){
        iterator it = std::lower_bound(end2start.begin(), end2start.end(), addr);
        if(it == end2start.end()) return false;
        return it->second <= addr;

    }

    /*! get address range which contains addr */
    bool GetRange(AddrInt end, T &t, AddrInt &s, AddrInt &e) {
        s = 1;
        e = 0;
        map<AddrInt, AddrInt>::iterator it = end2start.lower_bound(end);
        if (it == end2start.end()) { return false; }
        end = it->first;
        AddrInt start = it->second;
        if (start <= end) {
        	s = start;
        	e = end;
        	t = data[end];
        	return true;
        }
        return false;
    }

    size_t GetRangeSize(AddrInt addr){
        map<AddrInt, AddrInt>::iterator it = end2start.lower_bound(addr);
        if (it == end2start.end()) { return 0; }
        AddrInt end = it->first;
        AddrInt start = it->second;
        if (start <= end) {
        	return end-start+1;
        }
        return 0;
    }

    bool GetStart(AddrInt addr, T &t, AddrInt &s){
        AddrInt x;
        return GetRange(addr, t, s, x);
    }

    bool GetState(AddrInt addr, T &t){
        AddrInt x, y;
        return GetRange(addr, t, x, y);
    }

    iterator begin() {
        return data.begin();
    }

    const iterator begin() const {
        return data.begin();
    }

    iterator end() {
        return data.end();
    }
    const iterator end() const {
        return data.end();
    }

    size_t size(){
        return data.size();
    }

};

} // namespace Tapas
