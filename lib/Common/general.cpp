#include <vector>
#include <string>
#include <algorithm>

#include "general.hpp"

using namespace std;

vector<vector<int>> getAllComb(int N, int K){
    vector<vector<int>> rt;
    string bitmask(K, 1);
    bitmask.resize(N, 0);
    do{
        vector<int> x;
        for(int i = 0; i < N; i++)
            if(bitmask[i]) x.push_back(i);
        rt.push_back(x);
    }while(prev_permutation(bitmask.begin(), bitmask.end()));
    return rt;
}

