#include <iostream>
#include <string>
#include <fstream>
#include <cassert>
#include <sstream>
#include <map>
#include <vector>

#include "type.h"
#include "ConfigureHelper.hpp"

using namespace std;

ConfigureParser::ConfigureParser(istream &is){
    ReadConfig(is);
}

template<typename T>
T ConfigureParser::ParseSingleValue(string key){
    return ParseValue<T>(entites[key][0])[0];
}

template<typename T>
vector<T> ConfigureParser::Parse1DValue(string key){
    return ParseValue<T>(entites[key][0]);
}

template<typename T>
vector<vector<T>> ConfigureParser::Parse2DValue(string key){
    vector<vector<T>> rt;
    for(auto x : entites[key])
        rt.push_back(ParseValue<T>(x));
    return rt;
}

template<typename T>
vector<T> ConfigureParser::ParseValue(string value){
    istringstream iss(value);
    vector<T> rt;
    T x;
    while(iss >> x)
        rt.push_back(x);
    return rt;
}

void ConfigureParser::ReadConfig(istream &is){
    string key;
    string value;
    bool InProcess = false;
    while(getline(is, value)){
        if(value.size() == 0){
            InProcess = false;
            continue;
        }
        if(value[0] == '"') continue; //<! ignore comment

        if(value[0] == '#'){
            key = value;
            key.erase(key.begin());
            if(entites.find(key) == entites.end())
                entites[key] = vector<string>();
            InProcess = true;
        }else
            entites[key].push_back(value);
    }
}

void ConfigureParser::ParseSystemConfig(SystemConfig &config){
    config.ncores = ParseSingleValue<uint32_t>("ncores"); 
    config.nchips = ParseSingleValue<uint32_t>("nchips");
    config.ndcms = ParseSingleValue<uint32_t>("ndcms");
    config.nnodes = config.nchips;

    //Capacity
    config.MemCapacity = Parse1DValue<uint64_t>("MemCapacity");
    config.LLCCapacity = Parse1DValue<uint64_t>("LLCCapacity");

    config.Chip2Cores = Parse2DValue<NodeType>("Chip2Cores");
    config.DCM2Chips = Parse2DValue<NodeType>("DCM2Chips");

    //Links and their id
    vector<vector<NodeType>> links = Parse2DValue<NodeType>("Link2ID");
    for(auto l : links)
        config.LinkID[make_pair(l[0], l[1])] = l[2];
    config.nlinks = config.LinkID.size();

    //TODO : read if bidirection
    config.LinkIsBidirectional = vector<bool>(config.LinkID.size(), true);
    for(auto i : config.LinkID){
        if(config.LinkIsBidirectional[i.second])
            config.LinkID[LinkType(i.first.second, i.first.first)] = i.second;
    }

    config.LinkBandwidth = Parse1DValue<double>("LinkBandwidth");
    config.LinkLatency = Parse1DValue<double>("LinkLatency");
    config.LLC2MLatency = Parse1DValue<double>("LLC2MLatency");
    config.LLC2MBandwidth = Parse1DValue<double>("LLC2MBandwidth");
}


bool SystemConfig::isChipScope(ScopeType a){
    return a < nchips;
}

bool SystemConfig::isDCMScope(ScopeType a){
    return a >= nchips && a < nchips+ndcms;
}

bool SystemConfig::isGlobalScope(ScopeType a){
    return a == NodeINF;
}

NodeType SystemConfig::getScope(ScopeType a){
    if(a < nchips) return a;
    if(a == NodeINF) return a;
    return a - nchips;
}

ScopeType SystemConfig::getScope(NodeType chip1, NodeType chip2){
    if(chip1 == chip2)
        return chip1;

    NodeType dcm1 = Chip2DCM[chip1];
    NodeType dcm2 = Chip2DCM[chip2];
    if(dcm1 == dcm2)
        return dcm1+nchips; 
    return NodeINF;
}

vector<LinkType> SystemConfig::getLinksInScope(NodeType chip1, NodeType chip2){
    ScopeType sp = getScope(chip1, chip2);
    NodeType n = getScope(sp);
    vector<LinkType> rt;
    if(isChipScope(sp)) return rt;
    if(isDCMScope(sp)){
        rt.push_back(LinkType(DCM2Chips[n][0], DCM2Chips[n][1]));
        rt.push_back(LinkType(DCM2Chips[n][1], DCM2Chips[n][0]));
    }
    else{
        for(auto i : LinkID)
            rt.push_back(i.first);
    }

    return rt;

}

inline vector<NodeType> goClockwise(NodeType src, NodeType dst){
    int next[4] = {1,3,0,2};
    
    vector<NodeType> rt;
    while(src != dst){
        rt.push_back(src);
        src = next[src];
    }
    rt.push_back(dst);
    
    vector<NodeType> y;
    return rt;
}

vector<NodeType> SystemConfig::getShortestPath(NodeType src, NodeType dst){
    auto x = goClockwise(src, dst);
    auto y = goClockwise(dst, src);
    if(x.size() < y.size()) return x;
    else if(y.size() < x.size()) return y;
    else{
        if(rand()%2) return x;
        return y;
    }
}

NodeType SystemConfig::getClosetChip(NodeType src, uint64_t mask){
    if(mask == 0) return 0;
    NodeType x = src;
    while(!(mask & (1<<x)))
            x = ++x % nchips;
    return x;
}

void SystemConfig::ShowInfo(ostream &out){
    print(out);
}

void SystemConfig::print(ostream &out){
    out << "ncores\t" << ncores << "\n";
    out << "nchips\t" << nchips << "\n";
    out << "ndcms\t" << ndcms << "\n" ;
    out << "MemCapacity\n\t";
    for(auto x : MemCapacity) out << x << " "; out << "\n";
    out << "LLCCapacity\n\t";
    for(auto x : LLCCapacity) out << x << " "; out << "\n";
    out << "Chip2Cores\n";
    for(auto x : Chip2Cores){
        out << "\t";
        for(auto y : x) out << y << " "; out << "\n";
    }
    out << "DCM2Chips\n";
    for(auto x : DCM2Chips){
        out << "\t";
        for(auto y : x) out << y << " "; out << "\n";
    }

}

SystemConfig* SystemConfig::deserialize(string filename){
    SystemConfig *rt = new SystemConfig();
    ifstream fin(filename);
    assert(fin.good());
    ConfigureParser cparser(fin);
    cparser.ParseSystemConfig(*rt);

    //<! build core2chip
    rt->Core2Chip.resize(rt->ncores);
    for(NodeType i = 0; i < rt->Chip2Cores.size(); i++)
        for(auto x : rt->Chip2Cores[i])
            rt->Core2Chip[x] = i;

    rt->Chip2DCM.resize(rt->nchips);
    for(NodeType i = 0; i < rt->DCM2Chips.size(); i++)
        for(auto x : rt->DCM2Chips[i])
            rt->Chip2DCM[x] = i;

    /*
    for(NodeType i = 0; i < rt->nchips; i++)
        for(NodeType j = 0; j < rt->nchips; j++){
            if(rt->LinkID.find(LinkType(i,j)) == rt->LinkID.end())
                rt->LinkID[LinkType(i,j)] = NodeINF;
        }
    */
    return rt;
}


vector<NodeType> SystemConfig::getOrderedChip(NodeType c){
    switch(c){
        case 0 : return {0, 1, 2, 3}; break;
        case 1 : return {1, 0, 3, 2}; break;
        case 2 : return {2, 3, 0, 1}; break;
        case 3 : return {3, 2, 1, 0}; break;
        default : assert(0); return {};
    }
}
