#pragma once

#include <ostream>
#include <iostream>
#include <vector>

#include "Common/Common.hpp"

using namespace std;

namespace Tapas {

//Locality-frindenly 2D vector
template<typename T>
class L2DVector{
private:
    size_t d2size;
    vector<T> container;
public:
    L2DVector(size_t _d2size) : d2size(_d2size){
    }
    void push_back(T x){
        container.push_back(x);
    }

    typename vector<T>::iterator operator[](const size_t s){
        return container.begin()+s*d2size;
    }
    size_t size(){
        return container.size();
    }

    size_t D1size(){
        return container.size()/d2size;
    }
    size_t D2size(){
        return d2size;
    }
};


struct Record {
    TimeInt time[2];
    unsigned accessor[2];

    Record() {
        time[0] = time[1] = 0;
        accessor[0] = accessor[1] = 0;
    }
};

template<typename T, typename U>
void MergeNetwork(NetworkType<T, U> &out, 
                  const NetworkType<T, U> &in) {
    for(auto n : in.node) {
        out.node[n.first] += n.second;
    }
    for(auto l : in.link) {
        out.link[l.first] += l.second;
    }
}

template<typename T, typename U>
void DiffNetwork(NetworkType<T, U> &out, 
                 const NetworkType<T, U> &in) {
    for(auto n : in.node) {
        out.node[n.first] -= n.second;
    }
    for(auto l : in.link) {
        out.link[l.first] -= l.second;
    }
}

template<typename T, typename U>
void PrintNetwork(const NetworkType<T, U> &network, std::ostream &out) {
    for(auto n : network.node) {
        out << n.first << " : " << n.second << "\n";
    }
    for(auto l : network.link) {
        out << "(" << l.first.first << "," << l.first.second
            << ")" << "  " << l.second << "\n"; 
    }
}

template<typename T, typename U>
void CopyNetwork(NetworkType<T, U> &out, NetworkType<T, U> &in) {
    for(auto n : in.node) {
        out.node[n.first] = n.second;
    }
    for(auto l : in.link) {
        out.link[l.first] = l.second;
    }
}

} // namespace Tapas
