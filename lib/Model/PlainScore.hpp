#pragma once

#include <vector>
#include <map>
#include "ModelBase.hpp"

namespace Tapas {

class PlainScore : public ModelBase {

    DomainMap BestPlan;

public:

    bool isOptimal(DomainMap &plan, const SystemConfig &config, ScoreType &score, bool incremental);
    bool getNext(DomainMap &current, DomainMap &next);

};

}
