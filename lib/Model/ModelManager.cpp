#include <iostream>
#include "Traffic/TrafficManager.hpp"
#include "Footprint/FootprintManager.hpp"

#include "Model/ModelManager.hpp"
#include "Model/ModelBase.hpp"

namespace Tapas {

extern TrafficManager<SourceSnoopState, SourceSnoopBuilder> gTrafficMgr;
extern FootprintManager gFpMgr;

void ModelManager::setModel(ModelBase* m) {
    M = m;
    M->setManager(this);
}

void ModelManager::setRawGraph(RawGraph &g) {
    M->setRawGraph(g);
}

/*! What is this ? */
std::size_t ModelManager::getFilltime(const std::vector<unsigned> &nodes,
                        const std::vector<LinkType> &links,
                        float capacity) {
    return gFpMgr.getFilltime(nodes, links, capacity);
}

void ModelManager::getInterconnectTraffic(
            NetworkType<uint32_t, uint64_t> &out,
            NetworkType<std::size_t, std::size_t> &filltime,
            std::map<NodeType, std::vector<unsigned> > &domain,
            bool incremental
            ) {
    return gTrafficMgr.getInterconnectTraffic(out, filltime, domain, incremental);
}

bool ModelManager::isOptimal(DomainMap &plan, const SystemConfig &config, ScoreType &score, bool incremental) {
    return M->isOptimal(plan, config, score, incremental);
}

bool ModelManager::getNext(DomainMap &current, DomainMap &next) {
    return M->getNext(current, next);
}

void ModelManager::printTraffic(ostream &out, DomainMap &plan, const SystemConfig &config) {
    NetworkType<uint32_t, uint64_t> traffic;
    NetworkType<std::size_t, std::size_t> filltime;
    M->getTraffic(plan, config, traffic, filltime, false);
    PrintNetwork(traffic, out);
    ///PrintNetwork(filltime, out);
}

void ModelManager::printScore(ostream &out, DomainMap &plan, const SystemConfig &config) {
    out << "Score : " << M->getScore(plan, config, false) << "\n";
    out << "Score : " << M->getScore(plan, config, true) << "\n";
}
} // namespace Tapas
