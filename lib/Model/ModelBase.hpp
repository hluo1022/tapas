/*
 * ModelBase: a base class of the model.
 * currently, there are two interfaces: getNext and isOptimal need to be
 * implemented, which defines the behavior the model. 
 * 
 */
#pragma once

#include "Graph.hpp"
#include "Tapas/Tapas.hpp"
#include "ModelManager.hpp"
#include <iostream>
using namespace std;
namespace Tapas {

class ModelManager;

class ModelBase {

protected:

    Graph G;
    ModelManager *Mgr;
    DomainMap Next;

    //!< fill time of node
    template<typename T, typename U>
    std::size_t getFilltime(const T &nodes,
                            const U &links,
                            float capacity) {
        return Mgr->getFilltime(nodes, links, capacity);
    }

    void getInterconnectTraffic(
                NetworkType<uint32_t, uint64_t> &out,
                NetworkType<std::size_t, std::size_t> &filltime,
                std::map<NodeType, std::vector<uint32_t> > &domain,
                bool incremental
                ) {
        return Mgr->getInterconnectTraffic(out, filltime, domain, incremental);
    }


public:
    template<typename T, typename U>
    void revertDomainMap(const DomainMap &plan,
                          std::map<NodeType, T> &cores,
                          std::map<NodeType, U> &links) {

        /// Step 1. put the threads within one domain
        /// to cores[domain]
        for(auto iter : plan) {
            unsigned thread = iter.first;
            NodeType domain = iter.second;
            cores[domain].insert(cores[domain].end(), thread); 
        }

        /// Step 2. put the links within one domain
        /// to links[domain]
        for(auto iter : cores) {
            auto threads = iter.second;
            NodeType domain = iter.first;
            for(auto i : threads) {
                for(auto j : threads) {
                    /// make sure i < j
                    if ( G.connected(i, j) && i < j ) {
                        auto link = std::make_pair(i, j);
                        links[domain].insert(links[domain].end(), link);
                    }
                }
            }
        }
    }

    ModelBase() : Mgr(nullptr)
    {}

    virtual ~ModelBase() {}

    inline void setRawGraph(RawGraph &g) { G.setRawGraph(g); }
    inline void setManager(ModelManager *m) { Mgr = m; }

    virtual bool isOptimal(DomainMap &plan, const SystemConfig &config, 
                           ScoreType &score, bool incremental) = 0;
    virtual bool getNext(DomainMap &current, DomainMap &next) = 0;
    ScoreType
    getScore(DomainMap &plan, const SystemConfig &config, bool incremental);

    void getTraffic(DomainMap &plan, const SystemConfig &config, 
              NetworkType<uint32_t, uint64_t> &traffic,
              NetworkType<std::size_t, std::size_t> &filltime,
              bool incremental);
};

} // namespace Tapas
