#include <iterator>
#include <iostream>

#include "GradientDescent.hpp"

namespace Tapas {

/*
 * an implementation of finding the optimal thread plan.
 * it first pick a random thread t, and try swapping it
 * with all other threads to see which one is the best for
 * swapping and the score under the placement
 */
bool GradientDescent::isOptimal(DomainMap &plan, const SystemConfig &config, ScoreType &score, bool incremental) {

    ScoreType best, new_score;
    unsigned target, host;

    /// pick a random host
    auto item = plan.begin();
    std::advance( item, rand() % plan.size() );
    host = item->first;
    target = host;
    /// set base score as the host's score
    best = getScore(plan, config, incremental);
#ifdef DEBUG
    unsigned long long base = best;
#endif
    for(auto t : plan) {
        DomainMap candidate = plan;

        /// if not on the same socket
        if ( t.second != plan[host] ) {
#ifdef DEBUG
            std::cout << "[PROFILE] : " << "Try swapping "
                      << host << " with " << t.first
                      << "\n";
#endif
            /// swap
            candidate[host] = t.second;
            candidate[t.first] = plan[host];

            /// get new score
            new_score = getScore(candidate, config, incremental);

#ifdef DEBUG
            std::cout << "[PROFILE] : " << "Current best score " << best
                      << "; got score " << score << std::endl;

            std::cout << "[PROFILE] : " << "Base score "
                      << base << " v.s. " << "tried score "
                      << new_score << std::endl;
#endif
            /// store new placement
            if ( new_score < best ) {
                best = new_score;
                target = t.first;
            }
        }
    }

    score = best;
    /// if no improvement, this should already be the best
    if (target == host) return true;

    /// otherwise, store the best plan as the next base
    BestPlan = plan;
    BestPlan[host] = plan[target];
    BestPlan[target] = plan[host];

    return false;
}

bool GradientDescent::getNext(DomainMap &current, DomainMap &next) {
    UNUSED_VAR(current);
    next = BestPlan;
    return true;
}

} // namespace Tapas
