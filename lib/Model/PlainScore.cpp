#include <iterator>
#include <iostream>

#include "PlainScore.hpp"

namespace Tapas {

/*
 * an implementation of finding the optimal thread plan.
 * it first find a random thread t, and try swapping it
 * with all other threads to see which one is the best for
 * swapping and the score under the placement
 */
bool PlainScore::isOptimal(DomainMap &plan, const SystemConfig &config, ScoreType &score, bool incremental) {

    /// set base score as the host's score
    score = getScore(plan, config, incremental);

    // always return true, sine this is the only plan we need to eval
    return true;
}

bool PlainScore::getNext(DomainMap &current, DomainMap &next) {
    UNUSED_VAR(current);
    UNUSED_VAR(next);
    return true;
}

} // namespace Tapas
