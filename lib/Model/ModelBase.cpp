#include <iostream>

#include "ModelBase.hpp"
#include "Common/Util.hpp"
using namespace std;
namespace Tapas {

//#include "AllLink.model"
//#include "C2MLinkFirst.model"
#include "TotalSum.model"
//#include "C2CLinkFirst.model"

/*! Input : plan, config , Output : traffic, filltime 
 *  traffic : LinkToTraffic and NodeToTraffic
 *  filltime : Node2FillTime, the network is useless
 * */
void ModelBase::getTraffic(DomainMap &plan,
                      const SystemConfig &config,
    NetworkType<uint32_t, uint64_t> &traffic,
    NetworkType<std::size_t, std::size_t> &filltime,
    bool incremental) {

    /// Step 1. translate the placing plan to a more suitable form
    std::map<NodeType, std::vector<uint32_t> > Node2Cores;
    std::map<NodeType, std::vector<LinkType> > Node2Links;

    revertDomainMap(plan, Node2Cores, Node2Links);

    /// Step 2. find the fill time of each thread  
    for(auto i : Node2Cores) {
        NodeType node = i.first;
        std::vector<uint32_t>& cores = i.second;

        auto j = config.LLCCapacity[node];
        std::size_t ft = getFilltime(cores, Node2Links[node], j);
        filltime.node[node] = ft;
    }

    /// Step 3. construct the traffic map of all threads
    getInterconnectTraffic(traffic, filltime, Node2Cores, incremental);
}

ScoreType
ModelBase::getScore(DomainMap &plan,
                    const SystemConfig &config,
                    bool incremental) {

    NetworkType<uint32_t, uint64_t> traffic;
    NetworkType<size_t, size_t> filltime;

    getTraffic(plan, config, traffic, filltime, incremental);
    // pass the traffic map and system config to 
    // Model for throughput
    return getThroughput(config, traffic);

}

}
