#pragma once

#include <set>
#include <unordered_map>

#include "./Tapas/Tapas.hpp"

namespace Tapas {

class Graph {

    RawGraph RG;

private:

    inline std::pair<unsigned, unsigned> 
    makeEdge(unsigned src, unsigned dst) {
        return std::make_pair(src, dst);
    }

public:

    inline void setRawGraph(RawGraph& raw) {
        RG = raw;
    }

    inline bool hasEdge(unsigned src, unsigned dst) {
        return RG.edge.find(makeEdge(src, dst)) != RG.edge.end();
    }

    inline bool connected(unsigned src, unsigned dst) {
        return  RG.edge.find(makeEdge(src, dst)) != RG.edge.end() ||
                RG.edge.find(makeEdge(dst, src)) != RG.edge.end();
    }

};

} // namespace Tapas
