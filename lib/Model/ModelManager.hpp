#pragma once

#include <ostream>
#include <map>
#include <vector>

#include "Common/Common.hpp"
#include "Model/Graph.hpp"

namespace Tapas {

class ModelBase;

class ModelManager {

    /* The model's base class, which defines the 
     * method getTraffic. 
     */

public:
    ModelBase *M;

    void setModel(ModelBase *model);

    /* a raw representation of graph */
    void setRawGraph(RawGraph &g);

    /* get the filltime of each cache based on its capacity */
    std::size_t getFilltime(const std::vector<unsigned> &nodes,
                            const std::vector<LinkType> &links,
                            float capacity);

    /* get the traffic on each interconnect link */
    void getInterconnectTraffic(
                NetworkType<uint32_t, uint64_t> &out,
                NetworkType<std::size_t, std::size_t> &filltime,
                std::map<NodeType, std::vector<unsigned> > &domain,
                bool incremental
                );

    /* main interfaces for the client of Model class
     * method: isOptimal, which queries whether the plan is optimal
     * method: isOptimalIncremental, which queries whether the placement change 
     * method: getNext, which get the next plan to try
     */
    bool isOptimal(DomainMap &plan, const SystemConfig &config, ScoreType &score, bool incremental);
    bool getNext(DomainMap &current, DomainMap &next);

    /* print out */
    void printTraffic(std::ostream &out, DomainMap &plan, const SystemConfig &config);
    void printScore(std::ostream &out, DomainMap &plan, const SystemConfig &config);
};

} // namespace Tapas
