#pragma once
#include <queue>
#include <string>
#include <map>
#include <sstream>
#include <vector>
#include <list>
#include <iostream>

#include <sys/stat.h>
#include <unistd.h>

#include "Common/Common.hpp"
#include "MemRef.hpp"
#include "Trace/TraceReader.hpp"

namespace std {

template<>
struct less<MEMREF>
{   
    bool operator() (const MEMREF& lhs, const MEMREF&rhs) const
    {
        return lhs.time >= rhs.time;
    }
};

}

namespace Tapas {

class TraceManager {

    std::string prefix;
    std::string pid;

    std::vector<TraceReader*> TR;
    std::priority_queue<MEMREF> workQ;
    std::map<unsigned, unsigned> trace;

public:
    unsigned long long start;
    std::size_t total; //!< used for progress bar only

public:

    TraceManager() : start(0), total(0)
    {}

    void setSource(std::string _prefix, std::string _pid) {
        prefix = _prefix;
        pid = _pid;
    }

    void setSource(std::string _prefix){
        prefix = _prefix;
        pid = "";
    }

    template<typename T>
    void open(const T &traces)
    {
        int index = 0;
        TR.reserve(traces.size());
        for(auto t : traces) {
            std::stringstream ss;
            if(pid.length())
                ss << prefix << "." << pid << "." << t;
            else
                ss << prefix << "." << t;
            TraceReader *tr = new TraceReader;
            if( tr->open(ss.str().c_str()) ) {
                total += tr->getLength<T>();
                MEMREF m;
                if(tr->next(m)) {
                    workQ.push(m);
                    if (start == 0 || m.time < start ) {
                        start = m.time;
                    }
                }
                TR.push_back(tr);
                trace[t] = index;
                index++;
            }
        }
    }

    void open(){
        vector<uint32_t> tids;
        for(uint32_t i = 0; i < 32; i++){
            //verify if the file exist
            string filename;
            if(pid.length())
                filename = prefix + "." + pid + "." + to_string(i);
            else
                filename = prefix + "." + to_string(i);
            struct stat buffer;
            if(stat(filename.c_str(), &buffer) == 0)
                tids.push_back(i);
        }
        cout << "Found traces of threads: " << endl;
        cout << "\t";
        for(auto i : tids)
            cout << i << " ";
        cout << endl;
        open<vector<uint32_t>>(tids);
    }

    std::size_t getTotalWork() {
        return total;
    }

    bool next(MEMREF &ref) {
        if( !workQ.empty() ) {
            ref = workQ.top();
            workQ.pop();
            //std::cout << ref.address << " " << ref.thread << " " << ref.time << " " << ref.type << std::endl;

            MEMREF next_ref;
            if(TR[trace[ref.thread]]->next(next_ref)) {
                workQ.push(next_ref);
            }

            /// XXX once any trace is finished, finish them all
            if (workQ.size() < TR.size()) {
                while(!workQ.empty())
                    workQ.pop();
                return false;
            }
            return true;
        }
        return false;
    }

    void close() {
        for(auto tr : TR) {
            tr->close();
        }
    }

    ~TraceManager() {
        for(auto tr : TR) {
            delete tr;
        }
    }

};

} // namespace Tapas
