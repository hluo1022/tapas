#pragma once

#include <fstream>
#include <string>
#include <cstring>

#include "Common/Common.hpp"
#include "MemRef.hpp"

namespace Tapas {

class TraceReader {

    std::ifstream inFile;
    char *content;
    size_t bsize;
    size_t curpos;
public:

    bool open(const char* filename);
    void close();

    TraceReader(){
        bsize = sizeof(MEMREF)*(1<<20);
        content = new char[bsize]; //buffer size, 256M
        curpos = bsize;
    }
    template<typename T>
    bool next(T &ref) {
        auto left = bsize-curpos;
        if(left < sizeof(T)){
            if(!inFile) return false;
            
            //move left data to front
            if(curpos != bsize)
                memcpy(content, content+curpos, left);
            inFile.read(content+left, curpos);
            bsize = left+inFile.gcount();
            curpos = 0;
            return next(ref);
        }else{
            ref = *(T*)(content+curpos);
            curpos += sizeof(T);
        }
        return true;
        /*
        if ( !inFile.eof() ) {
            inFile.read((char*)&ref, sizeof(T));

            // check for incomplete read
            if (!inFile) return false;

            return true;
        }
        return false;
        */
    }

    template<typename T>
    std::size_t getLength() {
        if (inFile.is_open()) {
            auto cur = inFile.tellg();
            inFile.seekg(0, std::ios::beg);
            auto beg = inFile.tellg();
            inFile.seekg(0, std::ios::end);
            auto end = inFile.tellg();
            inFile.seekg(cur, std::ios::beg);
            return (end - beg) / sizeof(T);
        }
        return 0;
    }

    void reset();
};

} // namespace Tapas

