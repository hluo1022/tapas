// g++ SumTraceGenerator.cpp --std=c++11 -I ../ -I ../../include/ -O3
#include "TraceWriter.hpp"
#include "../../include/MemRef.hpp"
#include <vector>
#include <iostream>

using namespace std;
using namespace Tapas;

int nthread = 20;
size_t asize = (1<<16)*20000/sizeof(size_t);

int main(){
    //cout << asize << endl;
    vector<TraceWriter*> tws;
    for(int i = 0; i < nthread; i++){
        char c[128];
        sprintf(c, "trace.1.%d", i);
        tws.push_back(new TraceWriter());
        tws.back()->open(c);
    }
    
    size_t chunksize = asize / nthread;
    
    MEMREF x;
    x.type = 0;

    for(int ii = 0; ii < 5; ii++){

        for(size_t i = 0; i < chunksize; i += 256){
            for(size_t j = 0; j < nthread; j++){
                x.thread = j;
                x.address = (chunksize*j+i)*sizeof(size_t);
                x.time++;
                tws[j]->write(x);
            }
            for(size_t j = 0; j < nthread; j++){
                x.thread = j;
                x.address = ((chunksize*j+i+asize/2)%asize)*sizeof(size_t);
                x.time++;
                tws[j]->write(x);
            }
        }
    }

    for(auto i : tws)
        i->close();
    return 0;
}
