#pragma once

#include <fstream>

#include "Common/Common.hpp"
#include "MemRef.hpp"

namespace Tapas {

    class TraceWriter {

        std::ofstream outFile;

        public:

        bool open(char* filename){
            outFile.open(filename, std::ios_base::out | std::ios_base::binary);
            if ( !outFile ) {
                //std::cerr << "[ERROR] Unable to open file " << filename << " for reading trace" << std::endl;
                return false;
            }
            return true;
        }
        void close(){
            if (outFile.is_open()) {
                outFile.close();
            }
        }

        template<typename T>
            bool write(T &ref) {
                outFile.write((char*)&ref, sizeof(T));
                return true;
            }

    };

} // namespace Tapas

