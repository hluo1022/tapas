#include <iostream>

#include "TraceReader.hpp"

using namespace std;

namespace Tapas {

bool TraceReader::open(const char* filename) {
  inFile.open(filename, std::ios_base::in | std::ios_base::binary);
  if ( !inFile ) {
    //std::cerr << "[ERROR] Unable to open file " << filename << " for reading trace" << std::endl;
    return false;
  }
  return true;
}

void TraceReader::close() { 
  if (inFile.is_open()) {
    inFile.close();
  }
}
 
void TraceReader::reset() {
  if (inFile.is_open()) {
    inFile.seekg(0, ios::beg);
  }
}

} // namespace Tapas    

