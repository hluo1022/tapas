library(ggplot2)
#library(reshape)
library(data.table)
library("gridExtra")
library("cowplot")
require(scales)


events <- read.table("data_sum1.txt", header=TRUE)

bench = unique(events["Benchmark"])
plots = c()

for( i in bench[,1]){
    print(i)
    x_lab = paste0(i, " : Events")
    events_i <- events[which(events$Benchmark == i), ]

    plot_bench <- ggplot(events_i) +
    geom_bar(aes(Events, NormalizedValue, group=Property,
                 fill=factor(Property)),
             position = "dodge",
             stat="identity") +
            scale_y_continuous(labels=percent)+
            theme_gray() +
            theme(text = element_text(size=10),
              plot.margin=unit(c(0.5,.1,1,.1), "cm")) +
            theme(axis.text.x = element_text(angle = 90)) +
            labs(x=x_lab, y="NormalizedValue") +
            guides(fill=guide_legend(title = NULL))
    plot(plot_bench)
}
