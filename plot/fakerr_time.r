library(ggplot2)
library(data.table)
library(scales)

fakerr <- read.table("fakerr_time.dat", header = TRUE)


fakerr$Time<-fakerr[order(fakerr$Time),]

gap <- max(fakerr$Time)-min(fakerr$Time)
mid <- gap/2+min(fakerr$Time)

fakerrPlot<- ggplot(fakerr)+
                #geom_density(aes(Time))
                geom_point(aes(x=seq(1,nrow(fakerr)), y=Time), size=4, shape=4, color="darkred")+
                labs(x = "Different thread placements", y = "Time in seconds")+
                geom_hline(yintercept = max(fakerr$Time))+
                geom_hline(yintercept = min(fakerr$Time))+
                geom_label(aes(x=50,y=mid-gap/25,label=(paste(round(gap, digits=2), "seconds"))), size=10)+
                geom_segment(aes(x=50, y=mid+gap*0/25, xend=50, yend=max(fakerr$Time)-gap/40), arrow=arrow(length = unit(0.02, "npc")))+
                geom_segment(aes(x=50, y=mid-gap*2/25, xend=50, yend=min(fakerr$Time)+gap/40), arrow=arrow(length = unit(0.02, "npc")))+
                theme(text = element_text(size=28))
plot(fakerrPlot)
