library(ggplot2)
library(data.table)
library(scales)

sharerData <- read.table("sharerData.dat", header = TRUE, row.names=1)
sharerDataMelt <- melt(cbind(sharerData, idx = rownames(sharerData)))
sharerDataMelt$idx <- factor(sharerDataMelt$idx, levels=c("1","2","3-5","6-10","11-15","16-18","19","20"))

sharerDataPlot <- ggplot(sharerDataMelt)+
                  geom_bar(aes(x = variable, y = value, fill = idx), position = "fill", stat = "identity") +
                  scale_y_continuous(labels=percent_format()) +
                  guides(fill = guide_legend(title="Number of sharers", title.position = "left", label.position = "bottom", 
                         title.theme = element_text(angle=90))) +
                  labs(x = "Benchmarks", y = "Decomposition of data")

sharerAccess <- read.table("sharerAccesses.dat", header = TRUE, row.names=1)
sharerAccessMelt <- melt(cbind(sharerAccess, idx = rownames(sharerAccess)))
sharerAccessMelt$idx <- factor(sharerAccessMelt$idx, levels=c("1","2","3-5","6-10","11-15","16-18","19","20"))

sharerAccessPlot <- ggplot(sharerAccessMelt)+
                  geom_bar(aes(x = variable, y = value, fill = idx), position = "fill", stat = "identity") +
                  scale_y_continuous(labels=percent_format()) +
                  guides(fill = guide_legend(title="Number of sharers", title.position = "left", label.position = "bottom", 
                         title.theme = element_text(angle=90))) +
                  labs(x = "Benchmarks", y = "Decomposition of accesses")

plot(sharerDataPlot)
plot(sharerAccessPlot)
#print(sharerData)

#head(mpg)
