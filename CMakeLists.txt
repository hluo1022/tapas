cmake_minimum_required(VERSION 2.8)

project("Tapas")

set(TAPAS_MAJOR_VERSION 0)
set(TAPAS_MINOR_VERSION 1)
set(TAPAS_PATCH_VERSION 0)
set(TAPAS_VERSION
    ${TAPAS_MAJOR_VERSION}.${TAPAS_MINOR_VERSION}.${TAPAS_PATCH_VERSION})

# XXX add FindModule.cmake routine
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")


set(CMAKE_CXX_COMPILER "icc")

#set(CMAKE_C_FLAGS "-g -O0 -Wall -Wextra -fomit-frame-pointer -fno-strict-aliasing -fno-stack-protector -fPIC")
set(CMAKE_C_FLAGS "-g -O3 -fomit-frame-pointer -fno-strict-aliasing -fno-stack-protector -fPIC -fopenmp")
#set(CMAKE_C_FLAGS "-g -O0 -fomit-frame-pointer -fno-strict-aliasing -fno-stack-protector -fPIC")
set(CMAKE_CXX_FLAGS "${CMAKE_C_FLAGS} --std=c++11")

# swig
#find_package(SWIG REQUIRED)
#include(${SWIG_USE_FILE})

# python
#find_package(PythonLibs) # if use system python, uncomment this line
# use my own Python
#set(PYTHON_INCLUDE_PATH "/opt/local/Library/Frameworks/Python.framework/Versions/2.7/include/python2.7/")
#set(PYTHON_LIBRARY "/opt/local/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/config/libpython2.7.a")
#include_directories(${PYTHON_INCLUDE_PATH})
#set(CMAKE_CXX_COMPILER "g++")
#set(CMAKE_C_FLAGS "-g -O0 -Wall -Wextra -fomit-frame-pointer -fno-strict-aliasing -fno-stack-protector -fPIC")
set(CMAKE_C_FLAGS "-g -O3 -fomit-frame-pointer -fno-strict-aliasing -fno-stack-protector -fPIC -fopenmp ")
#set(CMAKE_C_FLAGS "-g -O0 -fomit-frame-pointer -fno-strict-aliasing -fno-stack-protector -fPIC")
set(CMAKE_CXX_FLAGS "${CMAKE_C_FLAGS} --std=c++11")

# swig
#find_package(SWIG REQUIRED)
#include(${SWIG_USE_FILE})

# python
#find_package(PythonLibs) # if use system python, uncomment this line
# use my own Python
#set(PYTHON_INCLUDE_PATH "/opt/local/Library/Frameworks/Python.framework/Versions/2.7/include/python2.7/")
#set(PYTHON_LIBRARY "/opt/local/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/config/libpython2.7.a")
#include_directories(${PYTHON_INCLUDE_PATH})

# libnuma
find_package(LibNUMA)
#include_directories(${LIBNUMA_INCLUDE_DIRS})
#set(LIBS ${LIBS} ${LIBNUMA_LIBRARIES})

# PinTracer
# Note: only support for linux
if(${CMAKE_SYSTEM} MATCHES "Linux")
    set(BUILD_PIN_TRACER ON CACHE STRING "Build Pin based tracer?" FORCE)
    set(PIN_ROOT "$ENV{PIN_ROOT}")
endif()

# include path
include_directories(${CMAKE_SOURCE_DIR}/lib)
include_directories(${CMAKE_SOURCE_DIR}/include)
# test 
enable_testing()

# source files
add_subdirectory(bin)
add_subdirectory(lib)
add_subdirectory(tests)
add_subdirectory(other)
add_subdirectory(runtime)
add_subdirectory( benchmark)
