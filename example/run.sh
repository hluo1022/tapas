#!/bin/bash

if [ $# -ne 2]
then 
	echo "[Usage] : run.sh [mode]"
	exit -1
fi

TAPAS_BIN=../../build/bin/

if [ $1 == "baseline" ]
then
	# run baseline workload
	./spmv 8192
elif [ $1 == "profile" ]
then
	# run profile version
	$TAPAS_BIN/GenTrace
	./PerfGenTrace ./spmv_prof 8192
elif [ $1 == "model" ]
then
	# model
	#   XXX is the process id of the profiled workload
	$TAPAS_BIN/TraceProfile -a ./address -o out -i XXX -t trace
	$TAPAS_BIN/OptDataPlace -i 1 -f 300 -a ./address -p interleave -t out -c config
elif [ $1 == "optimized" ]
then
	# enforce placement
	# assume the thread placement was stored in 'plan'
	export OMP_PLACES="sockets"
	export GOMP_CPU_AFFINITY=`cat plan`
	./spmv_opt 8192
else
	echo "[ERROR] : Unrecognized running mode"
	exit -1
fi
