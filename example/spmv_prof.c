#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "omp.h"

// Tapas:
//   header file for Tapas profiling
#include "prof.h"

static double duration;

void zAx(double ** z, double * data, long * colind, long * row_ptr, double * x, int N)
{
    double start = omp_get_wtime();
	long i, j, ckey;

    // Tapas:
    //   Markers for beginning of ROI(region of interest)
    roi_begin();

    { 
        #pragma omp for private(ckey,j,i) schedule(static)
        for (i=0; i<N; i++ ){ 
            (*z)[i]=0;
            for (ckey=row_ptr[i]; ckey<row_ptr[i+1]; ckey++) {
                j = colind[ckey];
                (*z)[i] += data[ckey]*x[j];
            }              
        }
    }

    // Tapas:
    //   Markers for end of ROI(region of interest)
    roi_end();

    double end = omp_get_wtime();
    duration = end - start;
}

void usage() {
    printf("spmv N\n\n");
    printf("Option:\n");
    printf("  N: size of matrix (NxN)\n");
}

int main(int argc, char* argv[]) {

    int i, j;

    if (argc != 2) {
        usage();
        return 0;
    }

    int M = atoi(argv[1]);
    if ( M < 16 ) {
        printf("[WARN] : M must be larger than 16\n");
        return 0;
    }

    printf("Setup...\n");
    // setup for SpMV computation
    int init_nonzeros = M * M / 10;

    double * x = malloc(sizeof(double)*M);
    long * row_ptr = malloc(sizeof(long)*(M+1));
    long * colind = malloc(sizeof(long)*init_nonzeros);
    double * result = malloc(sizeof(double)*M);
    double * z = malloc(sizeof(double)*M);

    for(i = 0; i < M; i++) {
        x[i] = 1.0;
        result[i] = 0.0;
    }

    int nonzeros = 0;
    int ci = 0;
    srand(time(NULL));
    for(i = 0; i < M; ++i) {
        row_ptr[i] = nonzeros;
        for(j = 0; j < M; ++j) {
            if (rand() % 10 < 1) {
                nonzeros++;
                if (ci >= init_nonzeros) {
                    colind = realloc(colind, sizeof(long) * init_nonzeros * 3 / 2);
                    init_nonzeros = init_nonzeros * 3 / 2;
                }
                colind[ci++] = j;
                result[i]++;
            }
        }
    }
    row_ptr[M] = nonzeros;


    double * data = malloc(sizeof(double)*nonzeros);
    for(int i = 0; i < nonzeros; ++i) {
        data[i] = 1.0;
    }
    colind = realloc(colind, sizeof(long)*nonzeros);

    /* Tapas API
     *
     *   1. open_address_range_file: open file that is to record the target address range
     *   2. close_address_range_file: close address range file
     *   3. register_address_range_prof(id, base, size, units, unit);
     *
     */  
    open_address_range_file("address");
    // record array x's address. Each element of x is a double 
    register_address_range_prof("x", x, sizeof(double)*M, 8, sizeof(double));
    close_address_range_file();

    printf("Computing...\n");
    zAx(&z, data, colind, row_ptr, x, M);

    printf("Verifying...\n");
    for(i = 0; i < M; ++i) {
        if (z[i] != result[i]) {
            fprintf(stderr, "[ERROR]: calculated wrong result\n");
            return -1;
        }
    }
    
    printf("Done\n");
    printf("Duration : %f seconds\n", duration);
    return 0;

}
