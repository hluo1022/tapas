/* This file generates two scripts for collecting
 * memory traces
 */

#include <iostream>
#include <fstream>

#include <sys/types.h>
#include <sys/stat.h> // chmod

using namespace std;

int main() {

    // generator of perf-based trace generator
    ofstream perfgen;
    perfgen.open("PerfGenTrace", ios::out);
    perfgen << "#!/bin/sh\n\n";
    perfgen << "if [ $# -lt 1 ]\n";
    perfgen << "then\n";
    perfgen << "\techo \"Usage : PerfGenTrace [cmd]\"\n";
    perfgen << "\texit -1\n";
    perfgen << "fi\n\n";
    perfgen << "perf record -d -e cpu/event=0xd0,umask=0x81/p -e cpu/event=0xd0,umask=0x82/p $@\n";
    perfgen << "perf script -s "
            << CMAKE_TRACER_PERF_DIR
            << "/Tapas.py\n";
    perfgen.close();
    chmod("PerfGenTrace", S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH);

    // generator of pin-based trace generator
    ofstream pingen;
    pingen.open("PinGenTrace", ios::out);
    pingen << "#!/bin/sh\n\n";
    pingen << "if [ $# -lt 1 ]\n";
    pingen << "then\n";
    pingen << "\techo \"Usage : PinGenTrace [cmd]\"\n";
    pingen << "\texit -1\n";
    pingen << "fi\n\n";
    pingen << PIN_ROOT
           << "/pin -t "
           << CMAKE_TRACER_PIN_DIR
           << "/obj-intel64/Tracer.so -o trace -phase phase -emit "
           << "-sample -sample:warmup `echo 2*2^30 | bc -l` " 
           << "-sample:len `echo 128*2^20 | bc -l` -sample:frequency 0.01 " 
           << "-- $@";
    pingen.close();
    chmod("PinGenTrace", S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH);

    return 0;
}
