#include "Tapas/TapasPOWER8.hpp"
#include "ConfigureHelper.hpp"
#include "CmdHelper.hpp"

using namespace std;
using namespace Tapas;
static void Usage() {
	std::cerr << "Usage : TraceProfile [options]\n";
	std::cerr << "Options:\n"
		<< "  -a <file>          Address file\n"
		<< "  -o <file>          Output trace profiles\n"
		<< "  -p <file>          Base name of raw profile files\n"
        << "  -s <file>          system configuration file\n"
        << "  -t <file>          TP\n"
        << "  -d <file>          DP\n"
        ;
	std::cerr << std::endl;
}

vector<vector<uint64_t>> rtime(20, vector<uint64_t>(MAX_WINDOW, 0));
void ReadReuseTime(char* file){
    ifstream fin(file);
    assert(fin.good());
    for(int i = 0; i < 20; i++)
        for(int j = 0; j < MAX_WINDOW; j++){
            uint64_t x;
            fin >> x;
            rtime[i][j] = x;
        }
}

uint64_t getOffchipAccess(ThreadPlacement &tp, vector<uint64_t> fts, SystemConfig &config){
    uint64_t rt = 0;
    for(int i = 0; i < 20; i++){
        NodeType core = tp.Thread2Core[i];
        NodeType chip = config.Core2Chip[core];
        uint64_t ft = sublog_value_to_index(fts[chip]);
        for(int j = ft; j < MAX_WINDOW; j++)
            rt += rtime[i][j];
    }
    return rt;
}

//command : command sysconfig 
int main(int argc, char **argv){
	char* address_file= getCmdOption(argv, argv + argc, "-a");
	char* output_file= getCmdOption(argv, argv + argc, "-o");
	char* profile_file= getCmdOption(argv, argv + argc, "-p");
	char* tp_file = getCmdOption(argv, argv + argc, "-t");
	char* dp_file = getCmdOption(argv, argv + argc, "-d");
    char* config_file = getCmdOption(argv, argv + argc, "-s");
    
    char* reuse_file = getCmdOption(argv, argv+argc, "-r");

    if(!profile_file) profile_file = (char*)"traffic.out";


    if(!profile_file || !tp_file || !dp_file){
        Usage();
        return 0;
    }
    
    MemRange &range = *MemRange::deserialize(address_file);
    DataPlacement &dp = *DataPlacement::deserialize(dp_file);
    dp.range = &range;
    ThreadPlacement &tp = *ThreadPlacement::deserialize(tp_file);
    SystemConfig &config = *SystemConfig::deserialize(config_file);
    SnoopState &psns = *ReadTraceProfile(profile_file);

    vector<uint64_t> filltime = getFilltime(tp, config);
    //ReadReuseTime(reuse_file);
    //uint64_t offchips = getOffchipAccess(tp, filltime, config);
    //cout << "Off CHIP Accesses : " << offchips << endl;
    //return 0;

    //ScoreType s = getScore(psns, tp, dp, config); 
    ScoreType s =getAccessScore(psns, tp, dp, config);
    cout << "Score : " << s << endl;
    
    cout << "\tFT : ";
    for(auto i : filltime)
        cout << sublog_value_to_index(i) << " ";
    cout << endl;
    return 0;
    auto accesses = getAccess(psns, tp, dp, config);
    accesses.serialize(cout);
    cout << endl;

    return 0;
}
