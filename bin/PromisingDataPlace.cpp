#include <cstring>
#include <vector>
#include <chrono>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <map>

#include "ConfigHelper.hpp"
#include "CmdHelper.hpp"
#include "Tapas.hpp"

using namespace Tapas;
using namespace std;
using namespace std::chrono;

typedef time_point<high_resolution_clock> TP;

std::map<std::string, unsigned long long> ArrayBase;
std::map<std::string, unsigned long> ArrayUnit;

template<typename T, typename V>
static void read_address_ranges(const char* file,
                                T &units,
                                V addresses) {
    ifstream myfile(file);
    /*
    if (myfile.is_open())
    {
        string line;
        while ( getline (myfile,line) )
        {
            unsigned long long start, end;
            unsigned long start_i, end_i;
            string id;
            string token;

            istringstream iss(line);
            getline(iss, id, '\t');

            getline(iss, token, '\t');
            start = strtoull(token.c_str(), nullptr, 16);

            getline(iss, token, '\t');
            end = strtoull(token.c_str(), nullptr, 16) - 1;

            getline(iss, token, '\t');
            start_i = strtoul(token.c_str(), nullptr, 10);

            getline(iss, token, '\t');
            end_i = strtoul(token.c_str(), nullptr, 10);

            units[start] = {id, start, end, start_i, end_i};
            if ( ArrayBase.find(id) == ArrayBase.end() ) {
                ArrayBase[id] = start;
                ArrayUnit[id] = (1 + end - start) / (end_i - start_i);
            }
            *addresses++ = start;
        }
        myfile.close();
    }
    */
        string id;
        while (myfile>>id)
        {
            unsigned long long start, end;
            unsigned long start_i, end_i;
            myfile >> hex >> start >> end;
            myfile >> dec >> start_i >> end_i;

            end--;
            units[start] = {id, start, end, start_i, end_i};
            if ( ArrayBase.find(id) == ArrayBase.end() ) {
                ArrayBase[id] = start;
                ArrayUnit[id] = (1 + end - start) / (end_i - start_i);
            }
            *addresses++ = start;
        }
        myfile.close();
}

static void print_timing(ostream &out, const vector<TP> &timer) {
    out << "[PROFILE] : Showing place score takes  "
        << duration_cast<milliseconds>(timer[1] - timer[0]).count()
        << " millisecond(s).\n";
}

static void read_plan(const char *file, DomainMap &plan) {
    ifstream myfile(file);

    /*
    if (myfile.is_open())
    {
        string line, token;
        while ( getline (myfile,line) )
        {
            unsigned long thread, socket;
            istringstream iss(line);

            getline(iss, token, ' ');
            thread = strtoul(token.c_str(), nullptr, 10);

            getline(iss, token, ' ');

            getline(iss, token, ' ');
            socket = strtoul(token.c_str(), nullptr, 10);
            plan[thread] = socket;
        }
        myfile.close();
    }
    */
    int t;
    int cnt = 0;
    while(myfile >> t){
        plan[t] = cnt++ / 10; //NOTE : 10 is the number of threads per socket. To implemented
    }
    myfile.close();
}

static void read_place(const char *file,
                       map<unsigned long long, unsigned> &place) {
    ifstream myfile(file);

    /*
    if (myfile.is_open())
    {
        string line, token;
        while ( getline (myfile,line) )
        {
            string id;
            unsigned long socket, start_i;
            istringstream iss(line);
            getline(iss, token, '\t');
            id = token;

            getline(iss, token, '\t');
            start_i = strtoul(token.c_str(), nullptr, 10);

            getline(iss, token, '\t');

            getline(iss, token, '\t');
            socket = strtoul(token.c_str(), nullptr, 10);

            place[ArrayBase[id] + start_i * ArrayUnit[id]] = socket;
        }

        myfile.close();
    }
    */
        string id;
        while (myfile>>id)
        {
            unsigned long start_i, end_i;
            int socket;
            myfile >> start_i >> end_i;
            myfile >> socket;
            place[ArrayBase[id] + start_i * ArrayUnit[id]] = socket;
        }
        myfile.close();
}

template<typename T, typename U, typename V>
static void print_place(ostream &out,
        T &address,
        U &place,
        V &units) {
    for(auto p : address) {
        out << units[p].id << "\t"
            << units[p].start_i << "\t"
            << units[p].end_i << "\t"
            << place[p] << endl;
    }
}

static void Usage() {
    std::cerr << "Usage : DataPlaceDump [options]\n";
    std::cerr << "Options:\n"
              << "  -c <file>          System config file\n"
              << "  -a <file>          Address file generated at profiling\n"
              << "  -p <file>          The file with data placement recorded\n"
              << "  -n <file>          The file with thread placement recorded\n"
              << "  -t <file>          Trace profiles generated by TraceProfile\n";
    std::cerr << std::endl;
}

int main(int argc, char* argv[]) {

    vector<TP> timer;

    char* address_file = getCmdOption(argv, argv + argc, "-a");
    char* place_file = getCmdOption(argv, argv + argc, "-p");
    char* plan_file = getCmdOption(argv, argv + argc, "-n");
    char* trace_file = getCmdOption(argv, argv + argc, "-t");
    char* syscfg_file = getCmdOption(argv, argv + argc, "-c");
    if (!address_file || !plan_file || !place_file || !trace_file || !syscfg_file) {
        Usage();
        return -1;
    }

    map<unsigned long long, RangeInfo> units;
    vector<unsigned long long> addresses;
    read_address_ranges(address_file, units, back_inserter(addresses));

    for(int i = 0, e = addresses.size(); i < e; ++i) {
        RegisterAddressRange(addresses[i], units[addresses[i]].end);
    }
    ReadTraceProfile(trace_file);

    // setup place file
    map<unsigned long long, unsigned> place;
    read_place(place_file, place);
    for(auto i : place) {
        LocateAddressRange(i.first, units[i.first].end, i.second);
    }

    // setup plan file
    DomainMap plan;
    read_plan(plan_file, plan);

    // setup graph structure, program property
    RawGraph g;

    SystemConfig config;

    // setup system configuration
    Config syscfg(syscfg_file);
    unsigned cores = syscfg.getValueOfKey<unsigned>("cores", 24);
    unsigned nodes = syscfg.getValueOfKey<unsigned>("nodes", 2);
    config.nodes = nodes;
    config.cores = cores;
    // setup graph structure, program property
    for(unsigned i = 0; i < cores; ++i) {
        AddRawGraphNode(g, i);
    }

    for(NodeType i = 0; i < nodes; ++i) {
        stringstream ss;
        ss << "capacity:" << i;
        config.capacity[i] = syscfg.getValueOfKey<unsigned long>(ss.str(), 12*1024*1024);
    }

    for(NodeType i = 0; i < nodes; ++i) {
        stringstream ss;
        ss << "neighbor:" << i;
        string neighbors = syscfg.getValueOfKey<std::string>(ss.str(), "");
        istringstream iss(neighbors);
        string num;
        while(getline(iss, num, ' ')) {
            if (num != "") {
                NodeType n = atoi(num.c_str());
                if (i != n) {
                    config.links.insert(std::make_pair(i, n));
                }
            }
        }
    }

    config.C2C_BW = syscfg.getValueOfKey<unsigned long>("bandwidth:c2c", 25);   /// approximately 25 Gb/s
    config.C2M_BW = syscfg.getValueOfKey<unsigned long>("bandwidth:c2m", 64);   /// approximately 64 Gb/s

    config.C2C_DL = syscfg.getValueOfKey<unsigned long>("latency:c2c", 1);    /// assume 1s
    config.C2M_DL = syscfg.getValueOfKey<unsigned long>("latency:c2m", 1);    /// assume 1s

    RegisterSearchModel( GradientDescentModel );
    RegisterGraph(g);

    timer.push_back(high_resolution_clock::now());

    PrintScore(std::cout, config, plan);
    vector<L2DVector<ScoreType>> traffics = GetScoreOnM2MLinks(config, plan, units);

    vector<ScoreType> ScoreOnLinks(config.links.size()/2, 0);
    vector<NodeType> rt;
    int MaxLinkID = 0;
    for(int i = 0; i < traffics.size(); i++){
        int MinID = 0;
        for(int j = 1; j < config.nodes; j++){
            if(traffics[i][j][MaxLinkID] < traffics[i][MinID][MaxLinkID])
                MinID = j;
        }
        rt.push_back(MinID);
        for(int j = 0; j < traffics[0].D2size(); j++){
            ScoreOnLinks[j] += traffics[i][MinID][j];
            if(ScoreOnLinks[j] > MaxLinkID)
                MaxLinkID =j;
        }
    }

    int cnt = 0;
    for(auto i : units){
        LocateAddressRange(i.first, i.second.end, rt[cnt]);
        place[i.first] = rt[cnt];
        cnt++;
    }
    PrintScore(cout, config, plan);

    ofstream place_ofile("place");
    print_place(place_ofile, addresses, place, units);
    place_ofile.close();
    /*
    for(auto i : rt)
        cout << i << " ";
    cout << endl;
    */


    timer.push_back(high_resolution_clock::now());

    print_timing(cout, timer);

    return 0;
}
