#include <iostream>
#include <fstream>

#include "Tapas/TapasPOWER8.hpp"
#include "ConfigureHelper.hpp"
#include "CmdHelper.hpp"

using namespace std;
using namespace Tapas;
static void Usage() {
	std::cerr << "Usage : TraceProfile [options]\n";
	std::cerr << "Options:\n"
		<< "  -a <file>          Address files\n"
		<< "  -p <file>          Base name of raw profile files\n"
        << "  -o <file>          Base name of raw place files\n"
        << "  -s <file>          Name of system configuration file\n";
	std::cerr << std::endl;
}

//command : command sysconfig 
int main(int argc, char **argv){
	char* address_file = getCmdOption(argv, argv + argc, "-a");
	char* profile_file = getCmdOption(argv, argv + argc, "-p");
	char* output_file = getCmdOption(argv, argv + argc, "-o");
    char* system_file = getCmdOption(argv, argv + argc, "-s");
    char* score_file = getCmdOption(argv, argv + argc, "-c");
    if(!score_file) score_file = "output";

    if(!(address_file && profile_file && output_file && system_file)){
        Usage();
        return 0;
    }

    MemRange &memranges = *MemRange::deserialize(address_file);
    SnoopState &psns = *ReadTraceProfile(profile_file);
    SystemConfig &config = *SystemConfig::deserialize(string(system_file));

    ThreadPlacement tp;
    DataPlacement dp(&memranges);
    ofstream fout(score_file);

    for(int itr = 0; itr < 100; itr++){
        getRandomThreadPlacement(tp, config);
        getRandomDataPlacement(dp, config);

        ScoreType s = getScore(psns, tp, dp, config);
        cout << "Score " << itr << " : " << s << endl;

        fout << itr << " : " << s << "\n\t";
        vector<uint64_t> filltime = getFilltime(tp, config);
        for(auto i : filltime)
            fout << i << " ";
        fout << "\n";
        auto accesses = getAccess(psns, tp, dp, config);
        accesses.serialize(fout);
        fout << "\n";
        
        string basename = string(output_file) + to_string(itr);
        tp.serialize(basename+".tp");
        dp.serialize(basename+".dp");
    }
    return 0;
}
