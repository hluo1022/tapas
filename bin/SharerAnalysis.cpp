#include <iostream>
#include <fstream>
#include <cassert>
#include <string>
#include "Tapas/TapasPOWER8.hpp"
#include "ConfigureHelper.hpp"
#include "CmdHelper.hpp"
#include "Trace/TraceManager.hpp"

#include <map>

using namespace std;
using namespace Tapas;


map<AddrInt, uint64_t> sharers;
map<AddrInt, uint64_t> acount;
map<AddrInt, uint64_t> writes;

void CountSharer(MEMREF &e){
    AddrInt addr = e.address>>6;
    if(sharers.find(addr) == sharers.end()){
        acount[addr] = 0;
        sharers[addr] = 0;
        writes[addr] = 0;
    }
    if(e.type == MEMWRITE)
        writes[addr]++;
    acount[addr]++;
    auto &stat = sharers[addr];
    stat |= 1<<e.thread;
}

void SummarySharer(ostream &os = cout){
    uint64_t cnt[128] = {0};
    uint64_t acnt[128] = {0};
    uint64_t wcnt[128] = {0};
    uint64_t total = 0;
    for(auto x : sharers){
        int c = __builtin_popcountll(x.second);
        cnt[c]++;
        acnt[c] += acount[x.first];
        wcnt[c] += writes[x.first];
        total += acount[x.first];
    }
    os << "Total work : " << total << endl;
    os << "Shares # Data # Accesses # Writes\n";
    for(int i = 0; i < 128; i++){
        if(cnt[i])
            os << i << " : " << cnt[i] << " " << acnt[i] << " " << wcnt[i] << "\n";
    }
}


map<AddrInt, TimeInt> previous[128];
vector<vector<uint64_t>> reuses(128, vector<uint64_t>(MAX_WINDOW, 0));
void CountReuse(MEMREF e){
    AddrInt addr = e.address>>6;
    auto &prev = previous[e.thread];
    uint64_t rtime = TimeMax;
    if(prev.find(addr) != prev.end())
        rtime = e.time-prev[addr];
    reuses[e.thread][sublog_value_to_index(rtime)]++;
    prev[addr] = e.time;
}

void SummaryReuse(ostream &out){
    for(int i = 0; i < 20; i++){
        for(int j = 0; j < MAX_WINDOW; j++)
            out << reuses[i][j] << " ";
        out << "\n";
    }
}

int main(int argc, char **argv){
    char* address_file = getCmdOption(argv, argv + argc, "-a");
    char* trace_file = getCmdOption(argv, argv + argc, "-t");
    char* output_file = getCmdOption(argv, argv + argc, "-o");

    assert(trace_file);
    TraceManager tm;
    tm.setSource(string(trace_file));
    tm.open();
    uint64_t total = tm.getTotalWork();
    cout << "Total Work : " << total << endl;

    MEMREF e;
    while(tm.next(e)){
        CountSharer(e);        
        CountReuse(e);
    }
    
    ofstream sharerout(string(output_file)+".sharer");
    SummarySharer(sharerout);
    sharerout.close();

    ofstream reuseout(string(output_file)+".reuse");
    SummaryReuse(reuseout);
    reuseout.close();
    
    return 0;
}
