#include <cstring>
#include <vector>
#include <chrono>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <map>
#include <queue>

#include "Tapas.hpp"
#include "CmdHelper.hpp"
#include "ConfigHelper.hpp"

#define NumNodes 2

using namespace Tapas;
using namespace std;
using namespace std::chrono;

typedef time_point<high_resolution_clock> TP;


namespace std {

template<>
struct less<RangeInfo>
{
    bool operator() (const RangeInfo& lhs, const RangeInfo& rhs) const
    {
        return lhs.impact >= rhs.impact;
    }
};

}

struct Statistics {

    /* total regions to be examined */
    unsigned int total; 

    /* examined regions */
    std::set<unsigned long long> covered; 

    /* regions examined and remapped */
    std::set<unsigned long long> remapped;

    /* time spent for each task */
    vector<TP> timer;

    /* priority queue to get a ranked list of the regions */
    std::priority_queue<RangeInfo> impact_list;

};

static void print_stat(ostream &out, Statistics &stat) {

    while(!stat.impact_list.empty()) {
        RangeInfo ri = stat.impact_list.top();
        std::cout << ri.id 
                  << "[" << ri.start_i  
                  << ","
                  << ri.end_i << "]"
                  << " " << ri.impact
                  << std::endl;

        stat.impact_list.pop();
    }

    const vector<TP> &timer = stat.timer;
    out << "[PROFILE] : Registering address range takes " 
        << duration_cast<milliseconds>(timer[1] - timer[0]).count() 
        << " millisecond(s).\n";
    out << "[PROFILE] : Reading trace profile takes     " 
        << duration_cast<milliseconds>(timer[2] - timer[1]).count() 
        << " millisecond(s).\n";
    out << "[PROFILE] : Locating address range takes    " 
        << duration_cast<milliseconds>(timer[3] - timer[2]).count() 
        << " millisecond(s).\n"; 
    out << "[PROFILE] : Searching for optimality takes  " 
        << duration_cast<milliseconds>(timer[4] - timer[3]).count() 
        << " millisecond(s).\n"; 
}

static void print_plan(ostream &out, DomainMap &plan) {
    std::vector<unsigned> thd;
    std::map<NodeType, unsigned> counters;
    std::set<NodeType> sockets;
    thd.reserve(plan.size());
    thd.resize(plan.size());

    for(auto t : plan) {
        cout << t.first << " " << t.second << std::endl;
        sockets.insert(t.second);
    }

    for(auto t : plan) {
       // FIXME change to Power8's core topology
       // thd[t.first] = t.second + sockets.size() * (counters[t.second]++);
       thd[t.first] = t.second * 10 + (counters[t.second]++);
    }
    for(auto core : thd) {
        out << core * 8 << " ";
    }
}

template<typename T, typename U, typename V>
static void print_place(ostream &out, 
                        T &address,
                        U &place,
                        V &units) {
    for(auto p : address) {
        out << units[p].id << "\t"
            << units[p].start_i << "\t"
            << units[p].end_i << "\t"
            << place[p] << endl;
    }
}

template<typename T, typename V>
static void read_init_place(const char* init_place, 
                            T &units,
                            V &addresses,
                            int nodes) {
    int addr_idx = 0;
    NodeType LastBestNode = rand() % nodes;
    ifstream myfile(init_place);
    if (myfile.is_open()) {
        string line;
        while ( getline (myfile, line) ) {
            std::map<int, int> vote;
            string id;
            string token;
            
            istringstream iss(line);
            getline(iss, id, '\t');

            while (getline(iss, token, '\t')) {
                int node = strtol(token.c_str(), nullptr, 10);
                if ( node >= 0 && node < nodes) {
                    vote[node]++;
                }
            }
            int MaxVote = 0;

            // assign to the last assigned node
            // to account for locality
            NodeType BestNode = LastBestNode;
            for(auto i : vote) {
                if (i.second > MaxVote) {
                    MaxVote = i.second;
                    BestNode = i.first;
                }
            }

            // record the most recently assigned node
            LastBestNode = BestNode;
            units[addresses[addr_idx]].init_node = BestNode;
            if (units[addresses[addr_idx]].id != id) {
                std::cerr << units[addresses[addr_idx]].id << " " << id << std::endl;
                std::cerr << "[ERROR] : Pagemap's partitions does not match "
                          << "address file's partition\n";
                exit(-1);
            }
            addr_idx++;
        }
    }
}

template<typename T, typename V>
static void read_address_ranges(const char* file, 
                                T &units, 
                                V addresses) {
    ifstream myfile(file);
    if (myfile.is_open())
    {
        string line;
        NodeType lines = 0;
        string id;
        while (myfile>>id)
        {
            unsigned long long start, end;
            unsigned long start_i, end_i;
            myfile >> hex >> start >> end;
            myfile >> dec >> start_i >> end_i;
            
            end--;
            units[start] = {id, start, end, start_i, end_i, lines % 2, 0};
            *addresses++ = start;
            lines++;
        }
        myfile.close();
    }
}

// parse system config from read config file
void  parse_system_config(const Config &syscfg, SystemConfig &config) {

    unsigned cores = syscfg.getValueOfKey<unsigned>("cores", 24);
    unsigned nodes = syscfg.getValueOfKey<unsigned>("nodes", 2);
  
    config.cores = cores;
    config.nodes = nodes;

    for(NodeType i = 0; i < nodes; ++i) {
        stringstream ss;
        ss << "capacity:" << i;
        config.capacity[i] = syscfg.getValueOfKey<unsigned long>(ss.str(), 12*1024*1024);
    }

    for(NodeType i = 0; i < nodes; ++i) {
        stringstream ss;
        ss << "neighbor:" << i;
        string neighbors = syscfg.getValueOfKey<std::string>(ss.str(), "");
        istringstream iss(neighbors);
        string num;
        while(getline(iss, num, ' ')) {
            if (num != "") {
                NodeType n = atoi(num.c_str());
                if (i != n) {
                    config.links.insert(std::make_pair(i, n));
                }
            }
        }
    }
    
    config.C2C_BW = syscfg.getValueOfKey<unsigned long>("bandwidth:c2c", 25);   /// approximately 25 Gb/s
    config.C2M_BW = syscfg.getValueOfKey<unsigned long>("bandwidth:c2m", 64);   /// approximately 64 Gb/s

    config.C2C_DL = syscfg.getValueOfKey<unsigned long>("latency:c2c", 1);    /// assume 1s
    config.C2M_DL = syscfg.getValueOfKey<unsigned long>("latency:c2m", 1);    /// assume 1s

}

static void Usage() {
    std::cerr << "Usage : OptDataPlace [options]\n";
    std::cerr << "Options:\n"
              << "  -c <file>          System config file\n"
              << "  -i <number>        Number of iterations for searching\n"
              << "  -f <number>        If specified, the result is dumped periodically\n"
              << "  -a <file>          Address file generated at profiling\n"
              << "  -p [string|file]   Initial page placement\n"
              << "                     random: Randomly generate initial page placement\n"
              << "                     interleave: Generate interleaved page placement\n"
              << "                     <file>: An initial page placement file\n"
              << "  -t <file>          Trace profiles generated by TraceProfile\n";
    std::cerr << std::endl;
}

int main(int argc, char* argv[]) {

    // system configuration
    SystemConfig config;

    // thread placement plan
    DomainMap plan;

    // a raw graph representing the network topology
    RawGraph g;

    // statistics
    Statistics stat;

    int iterations;
    unsigned int FrequentDump = 0;

    char* iter_str = getCmdOption(argv, argv + argc, "-i");
    char* address_file = getCmdOption(argv, argv + argc, "-a");
    char* init_place = getCmdOption(argv, argv + argc, "-p");
    char* trace_file = getCmdOption(argv, argv + argc, "-t");
    char* syscfg_file = getCmdOption(argv, argv + argc, "-c");
    if (iter_str && address_file && init_place && trace_file && syscfg_file) {
        char* p;
        iterations = strtol(iter_str, &p, 10);
        if (iterations == 0) {
            Usage();
            return -1;
        }
    } else {
        Usage();
        return -1;
    }
    char* freq_dump = getCmdOption(argv, argv + argc, "-f");
    if (freq_dump) {
        FrequentDump = atoi(freq_dump);
    }

    // setup system configuration
    Config syscfg(syscfg_file);
    parse_system_config(syscfg, config);
    for(unsigned i = 0; i < config.cores; ++i) {
        AddRawGraphNode(g, i);

        // the initial plan is to group threads in blocks
        plan[i] = i / (config.cores/config.nodes);
    }


    RegisterSearchModel( PlainScoreModel );
    RegisterGraph(g);

    // data placement, mapping from starting address to node
    map<unsigned long long, NodeType> place;

    // address range's properties, including boundary, mapped node and weight.
    map<unsigned long long, RangeInfo> units;
    vector<unsigned long long> addresses;

    stat.timer.push_back(high_resolution_clock::now());

    // read in address ranges from address file
    read_address_ranges(address_file, units, back_inserter(addresses));
    stat.total = addresses.size();
    // set initial placement
    if ( string("interleave") == init_place ) {
        for(int i = 0, e = addresses.size(); i < e; i++)
            units[addresses[i]].init_node = i % 2;
    }
    else if ( string("random") == init_place ) {
        for(int i = 0, e = addresses.size(); i < e; i++)
            units[addresses[i]].init_node = rand() % 2;
    }
    else {
        read_init_place(init_place, units, addresses, 2);
    }
    stat.timer.push_back(high_resolution_clock::now());
    ReadTraceProfile(trace_file);
    
    for(int i = 0, e = addresses.size(); i < e; ++i) {
        place[addresses[i]] = units[addresses[i]].init_node;
        // No need to register address ranges, since it has been done 
        // in ReadTraceProfile
        RegisterAddressRange(addresses[i], units[addresses[i]].end);
    }
    
    stat.timer.push_back(high_resolution_clock::now());
    for(auto i : place) {
        LocateAddressRange(i.first, units[i.first].end, i.second);
    }
    stat.timer.push_back(high_resolution_clock::now());

    int trials = 0;
    ScoreType best = SearchForOptimal(config, plan, false);
    DomainMap best_plan = plan;
    map<unsigned long long, NodeType> best_place = place;
    
    for(trials = 0; trials < iterations; trials++) {
        
        // best is the score for this thread plan
        ScoreType score = SearchForOptimal(config, plan, false);
        cout << "Starting " << trials << "th iteration\n";

        for ( auto p : place ) {
            unsigned long long addr = p.first;
            NodeType current_home = p.second;
            map<NodeType, ScoreType> scores;
            scores[current_home] = score;

            for ( NodeType j = 0; j < NumNodes; ++j) {
                if ( j != current_home ) {

                    LocateAddressRange(addr, units[addr].end, j);
                    score = SearchForOptimal(config, plan, true);
                    scores[j] = score;
                }
            }

            ScoreType local_best = scores[0];
            NodeType local_best_place = 0;
            ScoreType hi = local_best, lo = local_best;
            
            for(NodeType n = 1; n < NumNodes; ++n) {
                if ( scores[n] < local_best ) {
                    local_best = scores[n];
                    local_best_place = n;
                }
                if ( scores[n] > hi )
                    hi = scores[n];
                if ( scores[n] < lo )
                    lo = scores[n];
            }

            if ( hi == lo ) {
                local_best_place = 0xffff;
            }
            else {
                // take the absolute score change as the impact of this region
                if ( abs(hi - lo) > units[addr].impact ) {
                    units[addr].impact = abs(hi - lo);
                }

                //if ( i == iterations - 1 ) {
                //    stat.impact += units[addr].impact;
                //    stat.impact_list.push(units[addr]);
                //}
            }

            LocateAddressRange(addr, units[addr].end, local_best_place);
            place[addr] = local_best_place;
            score = local_best;
/*
            cout << "[PROFILE] : Try mapping " 
                 << units[addr].id 
                 << "[" << units[addr].start_i << ","
                 << units[addr].end_i << ")" 
                 << " to " << local_best_place << "\n"; 
            cout << "[PROFILE] : " << "best " 
                 << best;

            if ( local_best < best ) {
                 cout << " - " << best - local_best;
            }
            else {
                 cout << " + " << local_best - best;
            } 
            cout << "\n";
*/
            if ( local_best < best ) {
                best = local_best;
                best_plan = plan;
                best_place = place;
            }
        }

        // frequently dump the placement
        if ( FrequentDump > 0 && 
             trials % FrequentDump == (FrequentDump-1) ) {

            ofstream plan_profile("plan");
            print_plan(plan_profile, best_plan);
            plan_profile.close();

            ofstream place_profile("place");
            print_place(place_profile, addresses, best_place, units);
            place_profile.close();
        }

        // TODO randomly swap a pair of threads
        // first pick two random nodes
        int pc = plan.size();
        unsigned p1, p2;
        do {
            p1 = rand() % pc;
          
            if ( pc == 2 ) {
                p2 = 1 - p1;
            }
            else {
                p2 = (p1 + rand() % (pc - 2) + 1) % pc;
            }
        } while( plan[p1] == plan[p2] );

        NodeType n1 = plan[p1];
        plan[p1] = plan[p2];
        plan[p2] = n1;
        
    }

    ofstream plan_profile("plan");
    print_plan(plan_profile, best_plan);
    plan_profile.close();

    ofstream place_profile("place");
    print_place(place_profile, addresses, best_place, units);
    place_profile.close();

    stat.timer.push_back(high_resolution_clock::now());
    print_stat(cout, stat);

    return 0;
}
