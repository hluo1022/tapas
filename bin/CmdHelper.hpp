#pragma once

#include <vector>
#include <stdlib.h>
#include <algorithm>

char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}

bool parseAccessorGroup(const char *input, std::vector<unsigned> &group) {

  const char *curr = input;
  char *end;
  unsigned accessor_beg, accessor_end, accessor_stride;

  if ( *curr == '\0' ) return false;

  do {

    while (*curr == ' ' || *curr == '\t')
      curr++;

    accessor_beg = strtoul(curr, &end, 0);
    accessor_end = accessor_beg;
    accessor_stride = 1;
    if (curr == end || accessor_beg >= 65536) {
      goto invalid;
    }

    curr = end;
    if (*curr == '-') {
      accessor_end = strtoul (++curr, &end, 0);
      if (curr == end || accessor_end >= 65536 || accessor_end < accessor_beg) {
        goto invalid;
      }

      curr = end;
      if (*curr == ':') {
        accessor_stride = strtoul (++curr, &end, 0);
        if (curr == end || accessor_stride == 0 || accessor_stride >= 65536) {
          goto invalid;
        }

        curr = end;
      }
    }

    for(unsigned i = accessor_beg; i <= accessor_end; i += accessor_stride)
      group.push_back(i);

    while(*curr == ' ' || *curr == '\t')
      curr++;
    if ( *curr == ',' )
      curr++;
    else if ( *curr == '\0' )
      break;
   } while (1);

   return true;

invalid:

   return false;
}
