#include "Tapas/TapasPOWER8.hpp"
#include "ConfigureHelper.hpp"
#include "CmdHelper.hpp"

using namespace std;
using namespace Tapas;
static void Usage() {
	std::cerr << "Usage : TraceProfile [options]\n";
	std::cerr << "Options:\n"
		<< "  -a <file>          Addres file generated at profiling\n"
		<< "  -o <file>          Output trace profiles\n"
		<< "  -i <number>        Trace ID (PID)\n"
		<< "  -t <file>          Base name of raw trace files\n";
	std::cerr << std::endl;
}

//command : command sysconfig 
int main(int argc, char **argv){
	char* address_file = getCmdOption(argv, argv + argc, "-a");
	char* profile_file = getCmdOption(argv, argv + argc, "-o");
	char* trace_file = getCmdOption(argv, argv + argc, "-t");
    if(!profile_file) profile_file = "traffic.out";
    if(!address_file || !trace_file){
        Usage();
        return 0;
    }

    MemRange *memranges = MemRange::deserialize(address_file);
    SnoopState psns;
    //SnoopBuilder psnb(&psns, memranges, 20, 3);
    SnoopBuilder psnb(&psns, memranges, 20);

	ProfileTrace(psnb, trace_file);
    DumpTraceProfile(psnb, string(profile_file));
    return 0;
}
