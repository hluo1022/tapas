#pragma once

#include <typeinfo>
#include <string>
#include <sstream>
#include <fstream>
#include <map>
#include <cassert>
#include <algorithm>
#include <iterator>
#include "Tapas/Tapas.hpp"

using namespace std;

namespace Convert {
    const char STREND = '\0';
	// Convert T, which should be a primitive, to a std::string.
	template<typename T>
	std::string T_to_string(T const &val) {
		std::ostringstream oss;
		oss << val;
		return oss.str();
	}

	template<typename T>
	T string_to_T(std::string const &val) {
		std::istringstream iss(val);
		T retval;

		if (!(iss >> retval)) {
			std::cerr << "[ERROR] : Not a valid " <<
			          (std::string)typeid(T).name() <<
			          " received!\n";
			exit(-1);
		}
		return retval;
	}

	template<>
	std::string string_to_T(std::string const &val) {
		return val;
	}

    template<typename T>
    vector<T> string_to_vector(const string &key){
        vector<T> rt;
        istringstream iss(key);
        T x;
        while(iss >> x)
            rt.push_back(x);
        return rt;
    }

    char* ltrim(char *str){
        while(*str != STREND && isspace(*str))
            str++;
        return str;
    }
    void rtrim(char *str, int len){
        char *end = str+len-1;
        while(end >= str && isspace(*end))
            end--;
        *(++end) = STREND;
    }

    char* trim(char *str, int len){
        char *rt = ltrim(str);
        rtrim(str, len);
        return rt;
    }
}

class Config {
private:
	std::map<std::string, std::string> contents;
	std::string fName;

public:
	Config(const std::string &name) : fName(name) {
        GetKeyValuePairs();
	}
    
    void GetKeyValuePairs(){
        ifstream fin(fName);
        char line[256];
        while(fin.getline(line, 256)){
            char *p = line;
            p = Convert::ltrim(p);
            if(*p == '\0') continue; //<! empty line
            if(*p == '#') continue; //<! this is a comment line
            istringstream iss(p);
            string key;
            string equal_symbol;
            char value[256];
            iss >> key;
            iss >> equal_symbol;
            iss.getline(value, 255);
            assert(equal_symbol == "=" && "Formation error");
            assert(contents.find(key) == contents.end() && "Key has been seted");
            contents[key] = string(value);
        }
    }

    bool KeyExist(string key){
        return contents.find(key) != contents.end();
    }

	template<typename ValueType>
	ValueType getValueOfKey(const std::string &key, const ValueType &defaultValue = ValueType()){
		if (!KeyExist(key)) {
			return defaultValue;
		}
		return Convert::string_to_T<ValueType>(contents.find(key)->second);
	}

};

void parse_sys_config(char* syscfg_file, SystemConfig &config){
    Config syscfg(syscfg_file);

    uint32_t cores = syscfg.getValueOfKey<uint32_t>("ncores", 24);
    uint32_t nodes = syscfg.getValueOfKey<uint32_t>("nnodes", 2);
    config.ncores = cores;
    config.nnodes = nodes;

    config.LLC2MLatency.push_back(syscfg.getValueOfKey<double>("latency:c2m", 1.0));
    config.LLC2MBandwidth.push_back(syscfg.getValueOfKey<double>("bandwidth:c2m", 1.0));
    
    for(NodeType i = 0; i < nodes; ++i) {
        //<! read capacity
        stringstream ss;
        ss << "capacity:" << i;
        config.MemCapacity[i] = syscfg.getValueOfKey<uint64_t>(ss.str(), 12*1024*1024);
        
        //<! read core 2 node mapping
        ss.str("");
        ss << "node:" << i; 
        string node2cores = syscfg.getValueOfKey<string>(ss.str(), "");
        config.Chip2Cores[i] = Convert::string_to_vector<NodeType>(node2cores);
    }

    //<! read links
    for(NodeType i = 0; i < nodes; ++i) {
        for (NodeType j = 0; j < nodes; ++j){
            stringstream ss;
            ss << "link:" << i << "," << j;
            string link = ss.str();
            if(!syscfg.KeyExist(link)) continue;

            config.LinkID[LinkType(i,j)] = config.LinkID.size();
            link = syscfg.getValueOfKey<std::string>(ss.str(), "");
            
            istringstream iss(link);
            bool isBidirectional;
            double bandwidth;
            double latency;
            iss >> isBidirectional >> bandwidth >> latency;
            config.LinkIsBidirectional.push_back(isBidirectional);
            config.LinkBandwidth.push_back(bandwidth);
            config.LinkLatency.push_back(latency);

            if(isBidirectional)
                config.LinkID[LinkType(j,i)] = config.LinkID[LinkType(i,j)];
        }
    }

    //<! read routing
    for(NodeType i = 0; i < nodes; ++i) {
        for (NodeType j = 0; j < nodes; ++j){
            stringstream ss;
            ss << "path:" << i << "," << j;
            string path = ss.str();
            if(!syscfg.KeyExist(path)) continue;
            path = syscfg.getValueOfKey<string>(path);
            //config.Path[i][j] = Convert::string_to_vector<NodeType>(path);
        }
    }

}


void read_address_ranges(const char* file, vector<RangeInfo> &addresses) {
    ifstream myfile(file);
    if (myfile.is_open())
    {
        string id;
        while (myfile>>id)
        {
            AddrInt start, end;
            uint32_t start_i, end_i;
            myfile >> hex >> start >> end;
            myfile >> dec >> start_i >> end_i;

            end--;
            addresses.push_back({id, start, end, start_i, end_i, 0, 0});
        }
        myfile.close();
    }
}


void read_address_placement(const char* file, vector<RangeInfo> &addresses) {
    ifstream myfile(file);
    if (myfile.is_open())
    {
        NodeType node = 0;
        string id;
        while (myfile>>id)
        {
            AddrInt start, end;
            uint32_t start_i, end_i;
            myfile >> hex >> start >> end;
            myfile >> dec >> start_i >> end_i;
            myfile >> dec >> node;
            end--;
            addresses.push_back({id, start, end, start_i, end_i, node, 0});
        }
        myfile.close();
    }
}

void gather_address_placement(vector<RangeInfo> addresses, vector<RangeInfo> &place){
    map<string, uint32_t> stringid;
    map<uint64_t, int> ids;
    for(auto &i : addresses){
        if(stringid.find(i.id) == stringid.end())
            stringid[i.id] = stringid.size();
        uint64_t id = stringid[i.id];
        id <<= 32;
        id |= i.start_i;
        ids[id] = &i - &addresses[0];
    }

    //!< may have error when address ranges don't match placement
    for(auto &i : place){
        uint64_t id = stringid[i.id];
        id <<= 32;
        id |= i.start_i;
        int x = ids[id];
        i.start = addresses[x].start;
        i.end = addresses[x].end;
    }
}


void read_plan(const char* plan_file, DomainMap &plan) {
    ifstream myfile(plan_file);
    assert(!myfile.is_open() && "Open Failed");
    uint32_t core;
    uint32_t thread = 0;
    while(myfile >> core){
        plan[thread++] = core;
    }
}

/*
template<typename T, typename V>
void read_place(const char* place_file,
                            T &units,
                            V &addresses,
                            int nodes) {
    int addr_idx = 0;
    NodeType LastBestNode = rand() % nodes;
    ifstream myfile(place_file);
    if (myfile.is_open()) {
        string line;
        string id;
        while ( myfile >> id ) {
            std::map<int, int> vote;
            string token;

            //istringstream iss(line);
            //getline(iss, id, '\t');
            int a, b, node;
            myfile >> a >> b >> node;
                if ( node >= 0 && node < nodes) {
                    vote[node]++;
                }
            int MaxVote = 0;

            // assign to the last assigned node
            // to account for locality
            NodeType BestNode = LastBestNode;
            for(auto i : vote) {
                if (i.second > MaxVote) {
                    MaxVote = i.second;
                    BestNode = i.first;
                }
            }

            // record the most recently assigned node
            LastBestNode = BestNode;
            units[addresses[addr_idx]].init_node = BestNode;
            if (units[addresses[addr_idx]].id != id) {
                std::cerr << units[addresses[addr_idx]].id << " " << id << std::endl;
                std::cerr << "[ERROR] : Pagemap's partitions does not match "
                          << "address file's partition\n";
                exit(-1);
            }
            addr_idx++;
        }
    }
}
*/

void print_timing(ostream &out, const vector<TP> &timer) {
    out << "[PROFILE] : Showing place score takes  "
        << duration_cast<milliseconds>(timer[1] - timer[0]).count()
        << " millisecond(s).\n";
}
