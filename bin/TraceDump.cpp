#include <iostream>

#include "CmdHelper.hpp"
#include "Trace/TraceReader.hpp"
#include "MemRef.hpp"

using namespace Tapas;
using namespace std;

int main(int argc, char **argv){
    bool IsDump = cmdOptionExists(argv, argv+argc, "-d");
    char* trace_file = getCmdOption(argv, argv+argc, "-t");

    TraceReader tr;
    tr.open(trace_file);

    cout << "Trace Length : " << tr.getLength<MEMREF>() << endl;

    if(IsDump){
        MEMREF m;
        while(tr.next(m))
            m.print(cout);
    }
    return 0;
}
