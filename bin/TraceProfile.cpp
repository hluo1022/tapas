#include <cstring>
#include <vector>
#include <chrono>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <map>

#include "Tapas/Tapas.hpp"
#include "CmdHelper.hpp"
#include "ConfigHelper.hpp"

using namespace Tapas;
using namespace std;
using namespace std::chrono;

typedef time_point<high_resolution_clock> TP;

static void Usage() {
    std::cerr << "Usage : TraceProfile [options]\n";
    std::cerr << "Options:\n" 
              << "  -a <file>          Addres file generated at profiling\n"
              << "  -o <file>          Output trace profiles\n"
              << "  -i <number>        Trace ID (PID)\n"
              << "  -t <file>          Base name of raw trace files\n"
              << "  -g <string>        String representing a group of threads\n";
    std::cerr << std::endl;
}

int main(int argc, char* argv[]) {

    vector<TP> timers;

    char* address_file = getCmdOption(argv, argv + argc, "-a");
    char* profile_file = getCmdOption(argv, argv + argc, "-o");
    char* trace_file = getCmdOption(argv, argv + argc, "-t");
    char* trace_id = getCmdOption(argv, argv + argc, "-i");
    char* group_str = getCmdOption(argv, argv + argc, "-g");
    if (!address_file || !profile_file || !trace_file || !trace_id) {
        Usage();
        return -1;
    }

    vector<unsigned> thread_group;
    if (group_str) {
        parseAccessorGroup(group_str, thread_group);
    }
    else {
        for(unsigned i = 0; i < 256; ++i) {
            thread_group.push_back(i);
        }
    }
    // enable progress bar print out
    EnableProgressBar = true;

    timers.push_back(high_resolution_clock::now());

    vector<RangeInfo> units; 
    read_address_ranges(address_file, units); 
    for(auto i : units){
        RegisterAddressRange(i.start, i.end);
    }
    
    timers.push_back(high_resolution_clock::now());
    ProfileTrace(trace_file, trace_id, thread_group);
    
    timers.push_back(high_resolution_clock::now());
    DumpTraceProfile(profile_file);

    timers.push_back(high_resolution_clock::now());
    print_timing(cout, timers);

    return 0;
}
