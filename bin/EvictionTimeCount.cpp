#include <iostream>
#include <fstream>
#include <cassert>
#include <string>
#include "Tapas/TapasPOWER8.hpp"
#include "ConfigureHelper.hpp"
#include "CmdHelper.hpp"
#include "Trace/TraceManager.hpp"
#include "type.h"

#include <map>

using namespace std;
using namespace Tapas;


map<AddrInt, vector<uint64_t>> previous;
map<uint64_t, uint64_t> cnt;

void CountEvictionTime(MEMREF &e){
    AddrInt addr = e.address>>6;
    if(previous.find(addr) == previous.end()){
        previous[addr] = vector<uint64_t>(20, TimeMax);
        return;
    }
    auto &prev = previous[addr];
    for(auto i : prev)
        cnt[sublog_value_to_index(e.time-i)]++;
    previous[addr][e.thread] = e.time;
}

void Summary(ostream &os = cout){
    os << "Number of time : " << cnt.size() << "\n";
    for(auto i : cnt)
        os << "\t" << i.first << " " << i.second << "\n";
}

int main(int argc, char **argv){
    //char* address_file = getCmdOption(argv, argv + argc, "-a");
    char* trace_file = getCmdOption(argv, argv + argc, "-t");
    char* trace_id = getCmdOption(argv, argv + argc, "-i");
    char* output_file = getCmdOption(argv, argv + argc, "-o");

    assert(trace_file && trace_id);
    TraceManager tm;
    tm.setSource(string(trace_file), string(trace_id));
    tm.open();
    uint64_t total = tm.getTotalWork();
    cout << "Total Work : " << total << endl;

    MEMREF e;
    while(tm.next(e)){
        CountEvictionTime(e);        
    }
    
    ofstream fout(output_file);
    Summary(fout);
    fout.close();
    return 0;
}
