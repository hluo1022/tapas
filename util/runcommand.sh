#!/bin/bash

ROOT=/home/cye/tapas
benchs=("bt" "cg" "ft" "mg" "sp" "sum1")
pids=(2211 4327 2312 4783 2884 25757)

for i in {1..5};  do
#    ./TraceProfile -a $ROOT/${benchs[i]}/address -o ./${benchs[i]} -i ${pids[i]} -t $ROOT/${benchs[i]}/trace 
    #echo ./TraceProfile -a $ROOT/${benchs[i]}/address -o ./${benchs[i]} -i ${pids[i]} -t $ROOT/${benchs[i]}/trace  
    cd $ROOT/traces/${benchs[i]} 
    echo $ROOT/build/bin/OptDataPlace -c $ROOT/example/power8_config -i 1 -a ./address -t ./${benchs[i]} 
    time $ROOT/build/bin/OptDataPlace -c $ROOT/example/power8_config -i 1 -a ./address -t ./${benchs[i]} &
    #mv ./${benchs[i]}.traffic $ROOT/${benchs[i]}/${benchs[i]}.traffic
    #mv ./${benchs[i]}.footprint $ROOT/${benchs[i]}/${benchs[i]}.footprint
done

exit

for i in {0..5}; do 
    diff ./${benchs[i]}.traffic $ROOT/${benchs[i]}/${benchs[i]}.traffic
    diff ./${benchs[i]}.footprint $ROOT/${benchs[i]}/${benchs[i]}.footprint
done
