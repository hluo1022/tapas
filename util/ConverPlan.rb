#!/usr/bin/ruby

str = "./plan"
if(ARGV.size >= 1)
    str = ARGV[0]
end
plan = File.readlines(ARGV[0]).map{|x| x.strip}
plan = plan[0].split.map{|x| x.to_i}

output = "{" + plan.join("},{") + "}"
dir = File.dirname(str)
puts output
File.write("#{dir}/plan_omp_places", "#{output}")

