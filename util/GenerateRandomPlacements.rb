#!/usr/bin/ruby

if(ARGV.size != 1)
    puts "Usage : command place"
    exit
end


#generate plan
nodes = (0..19).to_a
nodes.map!{|a| a*8}
nodes.shuffle!
File.open("plan", 'w') {|f| f.write(nodes.join(" "))}
File.open("plan_omp_places", 'w') { |file| 
    file.write("{")
    file.write(nodes.join("},{"))
    file.write("}")
}

#generate place
address = File.readlines(ARGV[0]).map{|x| x.split}
place = File.open("place", 'w')
for i in address
    i[3] = rand(2) * 2
    place.puts(i.join("\t"))
end
place.close
