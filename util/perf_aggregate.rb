#!/usr/bin/ruby

if(ARGV.length != 1)
#    puts "Usage : command file"
#    exit
    infile = "./tmp"
else
    infile = ARGV[0]
end

names = `grep "Benchmark C" #{infile}`.split("\n").map{|x| x=x.split}
times = `grep "Time in seconds\\|ROI" #{infile}`.split("\n").map{|x| x=x.split}
#times = `grep "ROI" #{infile}`.split("\n").map{|x| x=x.split}
pm = `grep -e "[[:space:]]r[0-9|a-f]\\+ " #{infile}`.split("\n").map{|x| x=x.split}
property = `grep "Properties " #{infile}`.split("\n").map{|x| x.split}

def GetColumn(table, col)
    if not col.is_a? Array
        col = [] << col
    end

    rt = []
    table.each{|x|
        y = []
        col.each{|id|
            y << x[id]
        }
        rt << y
    }
    return rt
end

def Aggregate(arrays)
    rt = []
    for i in 0...arrays[0].length
        tmp = []
        for j in arrays
            tmp = tmp.concat(j[i])
        end
        rt << tmp
    end
    return rt
end

#for others
#names = GetColumn(names, 0)
#times = GetColumn(times,4)

#for sum1
names = Array.new(times.length, "sum1")
times = GetColumn(times, 2)

pm = GetColumn(pm, [0,1])
pm.map{|x| x[0]=x[0].delete(',')}

property = GetColumn(property, [1,2])
property = property.map{|x| x = x.join("_").chomp('_')}

table = []
for i in 0...times.length
    table << [names[i], property[i], "Time", times[i]]
end

num_pm = pm.length/names.length
for i in 0...pm.length
    table << [names[i/num_pm], property[i/num_pm], pm[i][1], pm[i][0]]
end

puts "Write to intermedia file data.txt"
fout = File.open("data.txt", 'w')
table.each{|x| fout.puts x.join(" ")}
fout.close
system("Rscript ./aggregate.r")
puts "Write results to table" 
system("./ReplaceEventsWithAlias.rb")
system("./DerivedEvents.rb")
