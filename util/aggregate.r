data <- read.table("data.txt", header=FALSE, sep=" ")
agdata <- aggregate(data, by=list(data$V1,data$V2, data$V3), FUN=mean)
agdata <- agdata[order(agdata$Group.1, agdata$Group.3, agdata$Group.2),]
agdata <- subset(agdata, select = -c(V1,V2,V3))
options(scipen=999)
#print(agdata)
write.table(agdata, "table", quote=FALSE, row.names=FALSE, col.names=FALSE, sep='\t')
