#pragma once

#include <stdio.h>

FILE* roi_fp;

void open_address_range_file() {
    roi_fp = fopen("address", "w+");
}

void close_address_range_file() {
    fclose(roi_fp);
}

void register_address_range(const char* name, void* base, size_t size, unsigned long units, size_t unit) {
    unsigned long i;
    //printf("in entry, %p, %u, %u\n", base, size, units);
    for(i = 0; i < units; ++i) {
        fprintf(roi_fp, "%s\t%p\t%p\t%lu\t%lu\n", name,
                                base + size / units * i, 
                                base + size / units * (i+1),
                                (size / unit) / units * i,
                                (size / unit) / units * (i + 1)
               );
    }
}

void __attribute__ ((noinline)) roi_begin() {
   printf("ROI begins\n");
}

void __attribute__ ((noinline)) roi_end() {
   printf("ROI ends\n");
}
