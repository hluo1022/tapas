#!/usr/bin/ruby

data = File.readlines("./place").map{|x| x.split}

n0 = 0.0
n1 = 0.0
for i in data
    if i[3] == "0"
        n0 = n0 + i[2].to_f-i[1].to_f
    else
        n1 = n1 + i[2].to_f-i[1].to_f
    end
end

puts (n0-n1)/(n0+n1)
