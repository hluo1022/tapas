#!/bin/bash

ToClean="Yes"
#Randomize="Yes"

export OMP_NUM_THREADS="20"

#Events on Power8
RemoteAccessPower8="PM_DATA_ALL_FROM_OFF_CHIP_CACHE,PM_DATA_ALL_FROM_RL4,PM_DATA_ALL_FROM_RMEM,PM_LD_CMPL,PM_ST_CMPL,PM_DATA_ALL_FROM_RL2L3_MOD,PM_DATA_ALL_FROM_RL2L3_SHR"
MissesPower8="PM_DATA_ALL_FROM_LMEM,PM_DATA_ALL_FROM_RMEM,PM_DATA_ALL_FROM_LL4,PM_DATA_ALL_FROM_RL4,PM_DATA_ALL_FROM_ON_CHIP_CACHE,PM_DATA_ALL_FROM_LL4"
PerfEvents="r1002e,r20016,r1c042,r4c042,r1c048,r1c04c,r2c048,r4c04a,r2c04a,r3c04a,r1c046,r2c044,r2c046"

CleanTPDP(){
    rm -f ./plan
    rm -f ./plan_omp_places
    rm -f ./place
    rm -f ./place0
    rm -f ./place1
}

SetupEnv(){

    rm -f ./plan*
    rm -f ./place*

    tmp=""
    if [ "$#" -eq 3 ] ; then
        tmp="_"$3
    fi
    
    if [ $1 == "tpdp" ] ; then
        #cp /localdisk/cye/tapas/traces/$2/plan .
        cp /localdisk/cye/tapas/traces/$2/plan_omp_places$tmp ./plan_omp_places
        cp /localdisk/cye/tapas/traces/$2/place$tmp ./place
    elif [ $1 == "binddata" ] ; then
        cp /localdisk/cye/tapas/traces/$2/place$tmp ./place
    elif [ $1 == "bindthread" ]  ; then
        #cp /localdisk/cye/tapas/traces/$2/plan .
        cp /localdisk/cye/tapas/traces/$2/plan_omp_places$tmp ./plan_omp_places
    fi

    export OMP_SCHEDULE="STATIC"
    export OMP_DISPLAY_ENV="True"

    if [ $1 == "tpdp" ] ; then
        export OMP_PROC_BIND="True"
        export OMP_PLACES=`cat plan_omp_places`
#        export GOMP_CPU_AFFINITY=`cat plan`
    elif [ $1 == "binddata" ] ; then     
        unset OMP_PROC_BIND
        unset OMP_PLACES
        unset GOMP_CPU_AFFINITY
        unset XLSMPOPTS
    elif [ $1 == "bindthread" ] ; then
        export OMP_PROC_BIND="True"
        export OMP_PLACES=`cat plan_omp_places`
#        export GOMP_CPU_AFFINITY=`cat plan`
    elif [ $1 == "default" ] ; then
        unset OMP_PLACES
        unset OMP_PROC_BIND
        unset GOMP_CPU_AFFINITY
        unset XLSMPOPTS
    else
        echo "Error"
    fi
    printf "Plan "
    cat plan_omp_places
    echo
}

Compile(){
    echo $1 $2
    make clean
    if [ $1 == "tpdp" ]
    then
        make MyCFLAGS="-DPOWER8 -DBIND_DATA=1" $2
    elif [ $1 == "binddata" ]
    then
        make MyCFLAGS="-DPOWER8 -DBIND_DATA=1" $2
    elif [ $1 == "bindthread" ]
    then
        make MyCFLAGS="-DPOWER8" $2
    elif [ $1 == "default" ]
    then
        make MyCFLAGS="-DPOWER8" $2
    else
        echo "[Error] : Unrecognized build version"
        exit -1
    fi
}

for i1 in "bt" "cg" "ft" "mg" "sp"; do
    for i0 in "default" "bindthread" "binddata" "tpdp" ; do
    #for i1 in "mg"; do
    #if false ; then
        #Compile $i0 $i1
        SetupEnv $i0 $i1
        for j in {1..100} ; do
            echo "Properties $i0 $i1"
            #ocount -e $MissesPower8,$RemoteAccessPower8 ./bin/$i1.C.x 
            #ocount -e $MissesPower8,$RemoteAccessPower8 ./bin/$i1.C.x
            perf stat -e $PerfEvents ./bin/$i1.C.x

            #| grep "Time in seconds" -A 50
        done
        CleanTPDP
    done
    #fi
    continue
    for i0 in "tpdp" ; do
        for k in "default" "maximal" "cache" "promising" ; do
#        for k in "default"; do
            SetupEnv $i0 $i1 $k
            for j in {1..3} ; do
                echo "Properties $i0 $i1 $k"
                #ocount -e $MissesPower8,$RemoteAccessPower8 ./bin/$i1.C.x 
                perf stat -e $PerfEvents ./bin/$i1.C.x
            done
            CleanTPDP
        done
    done
done


CleanTPDP
