#!/bin/sh

if [ $# -ne 3 ]
then
	echo "[Usage] : perf-comp.sh [opt | block | interleave | none] [bench] [size]"
	exit -1
fi

#export OMP_PLACES="{0,2,4,6,8,10,12,14,16,18,20,22},{1,3,5,7,9,11,13,15,17,19,21,23}"
if [ $1 == "opt" ]
then
	export OMP_PLACES="sockets"
	export GOMP_CPU_AFFINITY=`cat plan`
	make clean ; make MyCFLAGS=-DBIND_DATA=1 $2
elif [ $1 == "block" ]
then
	export OMP_PLACES="sockets"
	export GOMP_CPU_AFFINITY="0 2 4 6 8 10 12 14 16 18 20 22 1 3 5 7 9 11 13 15 17 19 21 23"
	make clean ; make MyCFLAGS=-DRECORD_PAGEMAP=1 $2
elif [ $1 == "interleave" ]
then
	export OMP_PLACES="sockets"
	export GOMP_CPU_AFFINITY="0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23"
	make clean ; make MyCFLAGS=-DRECORD_PAGEMAP=1 $2
elif [ $1 == "none" ]
then
	make clean ; make MyCFLAGS=-DRECORD_PAGEMAP=1 $2
else
	echo "[Error] : Unrecognized build version"
	exit -1
fi

# MEM_LOAD_UOPS
#perf stat -e cpu/event=0xD3,umask=0x01,name=local_dram/ -e cpu/event=0xD3,umask=0x04,name=remote_dram/ -e cpu/event=0xD3,umask=0x30,name=cache_hits/ ./bin/$2.$3.x

#OFFCORE_RESPONSE
#perf stat -e cpu/config=0x5301b7,config1=0x10091,name=offcore_responses/ ./bin/$2.$3.x

#Events on Power8
RemoteAccessPower8="PM_DATA_ALL_FROM_OFF_CHIP_CACHE,PM_DATA_ALL_FROM_RL4,PM_DATA_ALL_FROM_RMEM"
ocount -e $RemoteAccessPower8 ./bin/$2.$3.x
