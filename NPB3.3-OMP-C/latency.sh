#!/bin/sh

if [ $# -ne 3 ]
then
	echo "[Usage] : perf-comp.sh [opt | block | interleave] [bench] [size]"
	exit -1
fi

#export OMP_PLACES="{0,2,4,6,8,10,12,14,16,18,20,22},{1,3,5,7,9,11,13,15,17,19,21,23}"
export OMP_PLACES="{0:12:2}:2"
if [ $1 == "opt" ]
then
	export GOMP_CPU_AFFINITY=`cat plan`
	make clean ; make MyCFLAGS=-DBIND_DATA=1 $2
	#make clean ; make $2
elif [ $1 == "block" ]
then
	export GOMP_CPU_AFFINITY="0 2 4 6 8 10 12 14 16 18 20 22 1 3 5 7 9 11 13 15 17 19 21 23"
	make clean ; make $2
elif [ $1 == "interleave" ]
then
	export GOMP_CPU_AFFINITY="0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23"
	make clean ; make $2
elif [ $1 == "none" ]
then
	make clean ; make $2
else
	echo "[Error] : Unrecognized build version"
	exit -1
fi

perf stat -e cpu/event=0xCD,umask=0x01,ldlat=0x8/pp -e cpu/event=0xCD,umask=0x01,ldlat=0x10/pp -e cpu/event=0xCD,umask=0x01,ldlat=0x20/pp -e cpu/event=0xCD,umask=0x01,ldlat=0x40/pp -e cpu/event=0xCD,umask=0x01,ldlat=0x80/pp -e cpu/event=0xCD,umask=0x01,ldlat=0x100/pp -e cpu/event=0xCD,umask=0x01,ldlat=0x200/pp -o $1.lat ./bin/$2.$3.x
cat $1.lat
