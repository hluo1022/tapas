#!/usr/bin/python

import sys

def parse_row(row):
	name = row[0]
	start = int(row[1], 0)
	end   = int(row[2], 0)
	start_i = int(row[3])
        end_i   = int(row[4])
	return [name, start, end]

def verify(addr):
	for k1 in addr:
		for k2 in addr:
			if not k1 == k2:
				if ( not addr[k1][1] <= addr[k2][0] ) \
					and (not addr[k1][0] >= addr[k2][1] ):
					print k1 + "\t[" + hex(addr[k1][0]) + "," + \
						hex(addr[k1][1]) + "] overlaps with "
					print k2 + "\t[" + hex(addr[k2][0]) + "," + \
						hex(addr[k2][1]) + "]"
					return



if __name__ == '__main__':
	if len(sys.argv) <= 1:
		print 'verify_address.py [address]'
		sys.exit()
	filename = sys.argv[1]
	rec = {}

	with open(filename) as f:
		content = f.readlines()
		for line in content:
			row = line.rstrip('\n').split()
			[array, start, end] = parse_row(row)
			if rec.has_key(array):
				rec[array][1] = end
			else:
				rec[array] = [start, end]
	verify(rec)
