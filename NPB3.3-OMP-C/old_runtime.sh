#!/bin/bash

ToClean="Yes"
#Randomize="Yes"

export OMP_NUM_THREADS="20"

#Events on Power8
RemoteAccessPower8="PM_DATA_ALL_FROM_OFF_CHIP_CACHE,PM_DATA_ALL_FROM_RL4,PM_DATA_ALL_FROM_RMEM,PM_LD_CMPL,PM_ST_CMPL"
MissesPower8="PM_DATA_ALL_FROM_MEMORY,PM_L3_PREF_ALL"

if [ `uname -m` = "ppc64le" ]
then
#    Power8Flags="-DPOWER8 -DRECORD_PAGEMAP=1"
    Power8Flags="-DPOWER8"
    #Power8Flags=""
else
    Power8Flags=""
fi

if [ $# -ne 3 ]
then
    echo "[Usage] : runtime.sh [tpdp | binddata | bindthread | default] [bench] [size]"
    exit -1
fi

TPRandomize(){
    ../../tapas/traces/TPRandomizer ../../tapas/traces/$1/plan ./plan
}

DPRandomize(){
    ../../tapas/traces/DPRandomizer ../../tapas/traces/$1/place 2 ./place
}

CleanTPDP(){
    rm -f ./plan
    rm -f ./plan_omp_places
    rm -f ./place
    rm -f ./place0
    rm -f ./place1
}

SetupEnv(){
    if [ -v Randomize ] ; then
        if [ $1 == "tpdp" ] ; then
            TPRandomize $2
            DPRandomize $2
        elif [ $1 == "binddata" ] ; then
            DPRandomize $2
        elif [ $1 == "bindthread" ]  ; then
            TPRandomize $2
        fi
    else
        if [ $1 == "tpdp" ] ; then
            cp /localdisk/cye/tapas/traces/$2/plan .
            cp /localdisk/cye/tapas/traces/$2/plan_omp_places .
            cp /localdisk/cye/tapas/traces/$2/place .
        elif [ $1 == "binddata" ] ; then
            cp /localdisk/cye/tapas/traces/$2/place .
            rm -rf ./plan
            rm -rf ./plan_omp_places
        elif [ $1 == "bindthread" ]  ; then
            cp /localdisk/cye/tapas/traces/$2/plan .
            cp /localdisk/cye/tapas/traces/$2/plan_omp_places .
            rm -rf ./place
        fi
    fi


    export OMP_SCHEDULE="STATIC"
    
    if [ $1 == "tpdp" ] ; then
        export OMP_DISPLAY_ENV="True"
        export OMP_PLACES="{0},{8},{16},{24},{32},{40},{48},{56},{64},{72},{80},{88},{96},{104},{112},{120},{128},{136},{144},{152}"
        #export OMP_PLACES="{0},{144},{16},{24},{32},{40},{48},{56},{128},{104},{80},{88},{96},{72},{112},{120},{64},{136},{8},{152}"
        #export OMP_PLACES="{0:10:8},{0:10:8},{0:10:8},{0:10:8},{0:10:8},{0:10:8},{0:10:8},{0:10:8},{0:10:8},{0:10:8},{80:10:8},{80:10:8},{80:10:8},{80:10:8},{80:10:8},{80:10:8},{80:10:8},{80:10:8},{80:10:8},{80:10:8}"
        #export OMP_PLACES="{0,8,144,24,32,128,96,56,64,72},{80,88,48,104,112,120,40,136,16,152}"
        export OMP_PROC_BIND="True"
	    export OMP_PLACES=`cat plan_omp_places`
        export GOMP_CPU_AFFINITY=`cat plan`
    elif [ $1 == "binddata" ] ; then     
        #export OMP_PLACES="sockets"
        unset OMP_PROC_BIND
        unset OMP_PLACES
        unset GOMP_CPU_AFFINITY
        unset XLSMPOPTS
    elif [ $1 == "bindthread" ] ; then
        export OMP_DISPLAY_ENV="True"
        export OMP_PLACES="{0},{8},{16},{24},{32},{40},{48},{56},{64},{72},{80},{88},{96},{104},{112},{120},{128},{136},{144},{152}"
        #export OMP_PLACES="{0},{144},{16},{24},{32},{40},{48},{56},{128},{104},{80},{88},{96},{72},{112},{120},{64},{136},{8},{152}"
        export OMP_PROC_BIND="True"
        export OMP_PLACES=`cat plan_omp_places`
        export GOMP_CPU_AFFINITY=`cat plan`
    elif [ $1 == "default" ] ; then
        unset OMP_PLACES
        unset OMP_PROC_BIND
        unset GOMP_CPU_AFFINITY
        unset XLSMPOPTS
    else
        echo "Error"
    fi
    printf "Plan "
    cat plan
}

# set environment variable
make clean
if [ $1 == "tpdp" ]
then
    make MyCFLAGS="$Power8Flags -DBIND_DATA=1" $2
elif [ $1 == "binddata" ]
then
    make MyCFLAGS="$Power8Flags -DBIND_DATA=1" $2
elif [ $1 == "bindthread" ]
then
    make MyCFLAGS="$Power8Flags" $2
elif [ $1 == "default" ]
then
    make MyCFLAGS="$Power8Flags" $2
else
    echo "[Error] : Unrecognized build version"
    exit -1
fi

#perf mem -t load record ./bin/$2.$3.x
#./bin/$2.$3.x


#for i0 in {"default", "bindthread", "binddata", "tpdp"} ; do
#    for i1 in {"bt", "cg", "ft", "mg", "sp"}; do
#        SetupEnv $i0 $i1
        SetupEnv $1 $2
        for j in {1..5} ; do
            ocount -e $MissesPower8,$RemoteAccessPower8 ./bin/$2.$3.x 
            #| grep "Time in seconds" -A 50
        done
#    done
#done


# following code is to test random thread placement's performances
#for j in {1..10}
#do
    #../../tapas/traces/DPRandomizer ../../tapas/traces/$2/place 2 ./place
    #cp ../../tapas/traces/$2/plan ./plan
    #cp ../../tapas/traces/$2/place ./place
#    for i in {1..3}
#    do
#        ocount -e $MissesPower8,$RemoteAccessPower8 ./bin/$2.$3.x | grep "Time in seconds" -A 0
    #./bin/$2.$3.x
#    done
#done

CleanTPDP
