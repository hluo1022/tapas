#!/usr/bin/python

import sys
from collections import Counter

max_num_nodes = 64

def parse_row(row):
	name = row[0]
	if len(row) == 4:
		if int(row[2]) >= max_num_nodes:
			# if a row has four columns
			# and the 2nd column is very
			# large, it is a placement
			# plan and the 3rd column
			# is the node of the region
			node = row[3]
		else:
			# if the 2nd column is small
			# it is likely to be a recorded
			# placement
			nodes = row[1:]
			node = Counter(nodes).most_common(1)[0][0]
			
	else:
		# if the row does not have 4 columns
		if len(row) == 1:
			node = "-1"
		else:
			nodes = row[1:]
			node = Counter(nodes).most_common(1)[0][0]
	return [name, node]

def count_differences(place_table):
	summary = {}
	nrows = len(place_table[0])
	nfiles = len(place_table)
	diff = 0
	uncomparable = 0
	for i in range(0, nrows):
		for j in range(0, nfiles):
			name = place_table[j][i][0]
			place0 = place_table[0][i][1]
			place1 = place_table[j][i][1]
			if not place0 == place1:
				if not place0 == "-1" and not place1 == "-1":
					# this condition is a hack for IBM Power8
					if not ( (place0 == "1" and place1 == "2") or (place0 == "2" and place1 == "1")):
						diff += 1
						if summary.has_key(name):
							summary[name] += 1
						else:
							summary[name] = 1
						print name + " " + str(i) + " " + place0 + " " + place1
				else:
					uncomparable += 1
				break
	return [diff, uncomparable, summary]


if __name__ == '__main__':
	if len(sys.argv) <= 1:
		print 'compare_place.py [place0] [place1] ...'
		sys.exit()
	filenames = sys.argv[1:]
	place_table = []
	summary = {}
	total = {}

	for filename in filenames:
		plan = []
		with open(filename) as f:
			content = f.readlines()
			for line in content:
				row = line.rstrip('\n').split()
				[array, node] = parse_row(row)
				if total.has_key(array):
					total[array] += 1
				else:
					total[array] = 1
				plan.append([array, node])
		place_table.append(plan)

	[diff, uncomparable, summary] = count_differences(place_table)

	for name in summary:
		print name + "\t" + str(summary[name]) + "/" + str(total[name])
	
	print "Different places: " + str(diff) 
	print "Uncomparable places: " + str(uncomparable)

	#unrecd = count_unrecord(place_table)
	#print unrecd

