#!/bin/bash
PerfEvents="r1002e,r20016,r1c042,r4c042,r1c048,r1c04c,r2c048,r4c04a,r2c04a,r3c04a,r1c046,r2c044,r2c046"

export OMP_DISPLAY_ENV="True"
if [ -e "plan_omp_places" ] ; then
    export OMP_SCHEDULE="STATIC"
    export OMP_PROC_BIND="True"
    export OMP_PLACES=`cat plan_omp_places`
fi

echo $1
perf stat -e $PerfEvents $1
