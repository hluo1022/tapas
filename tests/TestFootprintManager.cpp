#include <iostream>

#include "Footprint/FootprintManager.hpp"

using namespace std;
using namespace Tapas;

int main() {
    TimeInt tick = 0;

    FootprintManager fm;
    fm.refer(0x0,  0, ++tick, 'R');
    fm.refer(0x0,  0, ++tick, 'R');
    fm.refer(0x0,  1, ++tick, 'R');
    fm.refer(0x0,  1, ++tick, 'R');
    fm.refer(0x8,  0, ++tick, 'R');
    fm.refer(0x16, 1, ++tick, 'W');
    fm.finish(tick);

    FootprintState* fs1 = fm.getNodeState(0);
    fs1->print(std::cout);
    FootprintState* fs2 = fm.getNodeState(1);
    fs2->print(std::cout);
    FootprintState* fs3 = fm.getLinkState(fm.makelink(0, 1));
    fs3->print(std::cout);

    std::set<NodeType> nodes;
    nodes.insert(0); nodes.insert(1);
    std::set<LinkType> links;
    links.insert(fm.makelink(0,1));
    FootprintState* fs4 = fm.getFootprint(nodes, links);
    fs4->print(std::cout);
    delete fs4;
    
    return 0;
}
