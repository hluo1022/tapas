#include <iostream>

#include "Common/Histo.hpp"

using namespace std;

int main() {
    for(std::size_t i = 1; i < MAX_WINDOW; ++i) {
        std::size_t value = histo::sublog_index_to_value(i);
        std::size_t index = histo::sublog_value_to_index(value);
        std::cout << index << " : " << value << "\n";
    }
    std::cout << std::endl;
}
