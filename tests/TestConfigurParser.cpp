#include <iostream>
#include <fstream>

#include "Common/Common.hpp"
#include "ConfigureHelper.hpp"

int main(){
    string filename = "POWER8Config";

    SystemConfig &config = *SystemConfig::deserialize(filename);

    config.ShowInfo(cout);
    return 0;

}
