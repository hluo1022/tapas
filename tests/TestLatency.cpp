#include <iostream>
#include <stdint.h>
#include <chrono>

#include "randomArray.h"

using namespace std;

int main(){
    uint32_t len = 256 << 20;
    
    cout << "Generate Random Array" << endl;
    uint32_t *a = RandomIdx(len);
    cout << "Generation Finished" << endl;

    typedef std::chrono::high_resolution_clock clock;
    auto t1 = clock::now();
    uint64_t sum = 0;
    for(int ii = 0; ii < 10; ii++)
        for(uint32_t i = 0; i < len; i++){
            sum += a[a[i]];
        }
    auto t2 = clock::now();
    cout << sum << endl;
    std::chrono::duration<double, std::nano> diff = t2-t1;
    cout << diff.count()  << " nanosecond" << endl;
    return 0;
}
