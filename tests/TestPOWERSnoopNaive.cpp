#include "Traffic/POWER8/POWERSnoopNaive.hpp"
#include "ConfigureHelper.hpp"
using namespace std;

int main(){
    POWERSnoopNaiveState state;
    POWERSnoopNaiveBuilder builder(&state);
    TimeInt tick = 0;

    builder.onReference(0x0, 0, ++tick, 0);
    builder.onReference(0x0, 1, ++tick, 0);
    builder.onReference(0x0, 2, ++tick, 0);
    builder.onReference(0x0, 3, ++tick, 1);
    builder.onReference(0x0, 0, ++tick, 0);
    builder.onReference(0x0, 1, ++tick, 1);
    
    map<NodeType, uint64_t> filltime;
    filltime[0] = 1;
    filltime[1] = 1;
    filltime[2] = 1;
    filltime[3] = 1;

    ThreadPlacement tp;
    tp.Thread2Core[0] = 0;
    tp.Thread2Core[1] = 1;
    tp.Thread2Core[2] = 2;
    tp.Thread2Core[3] = 3;

    SystemConfig &config = *SystemConfig::deserialize("/home/cye/tapas/example/ToyPOWER8Config");
    state.getScopeTraffic(filltime, tp, config);

    state.print(cout);
    return 0;
}
