#ifndef __randomArray__
#define __randomArray__

#include <random>
#include <list>
#include <iostream>
#include <algorithm>
using namespace std;

/*
inplace random array, access it as next = a[next]
len1 = length of array, len2 = maximal value of content

Algorithm: break circle and merge them
*/
uint32_t* InplaceRandomArray(uint32_t len, bool fixedSeed = true){
  static random_device rd;
  static mt19937 gen(rd());
  if(fixedSeed)
    gen.seed(19937);    //make sure generate same idx in each execution
  static uniform_int_distribution<> dis;

  uint32_t *rt = new uint32_t[len];
  for(uint32_t i = 0; i < len; i++)
    rt[i] = i;

  //random permutation : Knuth shuffles
  for(uint32_t i = 0; i < len; i++){
    uint32_t j = (dis(gen) % (len - i))+i;
    swap(rt[i], rt[j]);
  }

  bool *visited = new bool[len];
  fill(visited, visited+len, false);
  int pos = 0;
  int cnt = 0;
  int pnt = 0;
  while(++cnt != len){
    visited[pos] = true;
    pos = rt[pos];
    if(visited[pos] == true){
      while(visited[pnt])
        pnt++;
      swap(rt[pnt], rt[pos]);
      pos = rt[pos];
    }
  }
  delete visited;
  return rt;
}

void verifyCircle(uint32_t *rt, int len){
  //verification
  bool *visited = new bool[len];
  int pos = 0;
  fill(visited, visited+len, false);
  for(int i = 0; i < len; i++){
    if(visited[pos] == true){
      cerr << "error : " << pos << "\t" << i << endl;
      return;
    }
    visited[pos] = true;
    pos = rt[pos];
  }
  cerr << "OK : " << len << endl;
  //for(int i = 0; i < len; i++)
    //cout << i << " : " << rt[i] << endl;
}

uint32_t* RandomIdx(uint32_t len, uint32_t *A = NULL){
  static random_device rd;
  static mt19937 gen(rd());
  gen.seed(19937);    //make sure generate same idx in each execution
  static uniform_int_distribution<> dis(0, len-1);  //keep 0->0 as a circle

  if(A == NULL)
    A = new uint32_t[len];
  while(len--)
    A[len] = dis(gen);
  return A;
}

inline uint32_t GetRandomNumber(){
  static random_device rd;
  static mt19937 gen(rd());
  gen.seed(19937);    //make sure generate same idx in each execution
  static uniform_int_distribution<> dis;
  return dis(gen);
}

#endif
