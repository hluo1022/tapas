#include <iostream>

#include "Traffic/QPI/SourceSnoop.hpp"

using namespace std;
using namespace Tapas;

int main() {
    TimeInt tick = 0;

    SourceSnoopState state;
    SourceSnoopBuilder builder(&state);

    builder.onReference(0x0, 0, ++tick, 'R');
    builder.onReference(0x0, 1, ++tick, 'R');
    builder.onReference(0x0, 2, ++tick, 'R');
    builder.onReference(0x0, 3, ++tick, 'W');
    builder.onReference(0x0, 0, ++tick, 'R');
    builder.onReference(0x0, 1, ++tick, 'W');

    state.setHome(0);
    //state.print(std::cout);

    std::map<NodeType, std::vector<unsigned> > domain;
    domain[0] = {0, 1};
    domain[1] = {2, 3};
    
    NetworkType<uint32_t, uint64_t> out;
    NetworkType<unsigned long, std::size_t> in;
    // fake fill time
    in.node[0] = in.node[1] = in.node[2] = in.node[3] = 7;
    state.getInterconnectTraffic(out, in, domain);
    for(auto n : out.node) {
        std::cout << n.first << " : " << n.second << std::endl;
    }
    for(auto l : out.link) {
        std::cout << "(" << l.first.first << "," << l.first.second
                  << ")" << "  " << l.second << std::endl; 
    }

    return 0;
}
