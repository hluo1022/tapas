#!/usr/bin/ruby

system("g++ TestLatency.cpp --std=c++11 -O3")

puts "Local IO : Run on node 0, allocate on node 0"
system("numactl -m 0 taskset -c 0 ./a.out")

puts "Same chip IO : Run on node 1, allocate on node 0"
system("numactl -m 0 taskset -c 40 ./a.out")

puts "Remote IO : Run on node 2, allocate on node 0"
system("numactl -m 0 taskset -c 80 ./a.out")

puts "Verify Remote IO : Run on node 3, allocate on node 0"
system("numactl -m 0 taskset -c 120 ./a.out")
