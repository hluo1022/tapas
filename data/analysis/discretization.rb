#!/usr/bin/ruby

data = File.readlines(ARGV[0]).map{|x| x.strip.gsub(",","").to_i}

max = data.max
min = data.min
step = (max-min)/20


data=data.map{|x|
    x = (x-min)/step*step+min
}

puts data
