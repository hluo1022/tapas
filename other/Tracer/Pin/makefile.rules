##############################################################
#
# This file includes all the test targets as well as all the
# non-default build rules and test recipes.
#
##############################################################


##############################################################
#
# Test targets
#
##############################################################

###### Place all generic definitions here ######

PIN_ROOT?=$(shell pwd | sed '/extras.*/s///g')

#PINPLAY_HOME=$(PIN_ROOT)/extras/pinplay/

OPT?=-O2
#COMPRESS?=bzip2 # can be 'none', or 'bzip2', or 'gzip'
OBJEXT = o

#PINPLAY_INCLUDE_HOME=$(PINPLAY_HOME)/include
#PINPLAY_LIB_HOME=$(PINPLAY_HOME)/lib/$(TARGET)
#EXT_LIB_HOME=$(PINPLAY_HOME)/lib-ext/$(TARGET)

CXXFLAGS = -std=c++11 -D_FILE_OFFSET_BITS=64 -I$(PIN_ROOT)/source/tools/InstLib 

#CXXFLAGS += -I$(PINPLAY_INCLUDE_HOME) -I$(PIN_ROOT)/source/tools/PinPoints

CXXFLAGS += ${WARNINGS} $(DBG) $(OPT) ${DEPENDENCYFLAG} 

CXXFLAGS += -I$(TRACER_ROOT)/include

TOOLNAMES=Tracer 

TOOLS=${TOOLNAMES:%=$(OBJDIR)/$(PINTOOL_PREFIX)%$(PINTOOL_SUFFIX)}

# This defines tests which run tools of the same name.  This is simply for convenience to avoid
# defining the test name twice (once in TOOL_ROOTS and again in TEST_ROOTS).
# Tests defined here should not be defined in TOOL_ROOTS and TEST_ROOTS.
TEST_TOOL_ROOTS := ${TOOL_NAMES} 

# This defines the tests to be run that were not already defined in TEST_TOOL_ROOTS.
TEST_ROOTS :=

# This defines a list of tests that should run in the "short" sanity. Tests in this list must also
# appear either in the TEST_TOOL_ROOTS or the TEST_ROOTS list.
# If the entire directory should be tested in sanity, assign TEST_TOOL_ROOTS and TEST_ROOTS to the
# SANITY_SUBSET variable in the tests section below (see example in makefile.rules.tmpl).
SANITY_SUBSET :=

# This defines the tools which will be run during the the tests, and were not already defined in
# TEST_TOOL_ROOTS.
TOOL_ROOTS :=

# This defines the static analysis tools which will be run during the the tests. They should not
# be defined in TEST_TOOL_ROOTS. If a test with the same name exists, it should be defined in
# TEST_ROOTS.
# Note: Static analysis tools are in fact executables linked with the Pin Static Analysis Library.
# This library provides a subset of the Pin APIs which allows the tool to perform static analysis
# of an application or dll. Pin itself is not used when this tool runs.
SA_TOOL_ROOTS :=

# This defines all the applications that will be run during the tests.
APP_ROOTS :=

# This defines any additional object files that need to be compiled.
OBJECT_ROOTS := atomic_increment_$(TARGET)

# This defines any additional dlls (shared objects), other than the pintools, that need to be compiled.
DLL_ROOTS :=

# This defines any static libraries (archives), that need to be built.
LIB_ROOTS :=


##############################################################
#
# Test recipes
#
##############################################################

# This section contains recipes for tests other than the default.
# See makefile.default.rules for the default test rules.
# All tests in this section should adhere to the naming convention: <testname>.test


##############################################################
#
# Build rules
#
##############################################################

# This section contains the build rules for all binaries that have special build rules.
# See makefile.default.rules for the default build rules.

GXXVERSIONGTEQ5 := $(shell expr `g++ -dumpversion | cut -f1 -d.` \>= 5)
ifeq "$(GXXVERSIONGTEQ5)" "1"
	# gcc v5.0 uses new abi, which creates trouble for using pinplay
	CXXFLAGS += -D_GLIBCXX_USE_CXX11_ABI=0
endif

all: tools input test

tools: $(TOOLS)

test: vanilla

input: 
	@echo ""
	@echo "*********************************"
	@echo "Building pthread matrix multiplication"
	$(CC) -g -o mm Tests/pthread_matrix_multiplication.c -lpthread
	@echo ""

vanilla: ${TOOLS}
	@echo ""
	@echo "*********************************"
	@echo "Testing Tracer.so"
	@echo ""
	$(PIN_ROOT)/pin -t $(OBJDIR)/Tracer.so -emit 1 -o trace -phase config -sample -sample:warmup 8192 -sample:len 65536 -sample:frequency 0.1 -- ./mm 8

${OBJDIR}/%.${OBJEXT}: %.cpp
	$(CXX) ${MYDEFINES} ${COPT} $(CXXFLAGS) $(TOOL_INCLUDES) $(TOOL_CXXFLAGS) $(PIN_CXXFLAGS) ${COMP_OBJ}$@ $<

${OBJDIR}/Tracer.so:  ${OBJDIR}/Tracer.${OBJEXT} $(CONTROLLERLIB)
	@echo ""
	@echo "*********************************"
	@echo "Linking Tracer.so"
	$(LINKER) $(TOOL_LDFLAGS) $(LINK_EXE)$@ $^ $(TOOL_LPATHS) $(TOOL_LIBS) $(MYLIBS) $(EXTRA_LIBS) $(PIN_LIBS) $(DBG)   

## cleaning
instclean: 
	-rm -r -f mm *.${OBJEXT} *.out *.d pin.log obj-* $(PIN_ROOT)/source/tools/InstLib/obj-*
#clean: 
#	-rm -r -f mm *.${OBJEXT} $(PINPLAY_HOME)/PinPoints/scripts/*.pyc *.out pinball *.d pin.log obj-* $(PIN_ROOT)/source/tools/InstLib/obj-*
