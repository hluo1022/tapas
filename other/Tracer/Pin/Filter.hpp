#pragma once

#include <cstdint>
#include <set>
#include <vector>

using namespace std;
//============================================================//

/*!
 *  Configuration for sampling phase
 *
 *  The pair of pointers specifies the PC addresses of the begin
 *  and end of a phase.
 */
class PhaseFilterConfig {

    vector<pair<uintptr_t, uintptr_t> > impl; 

    unsigned phase;

public:

    PhaseFilterConfig() 
             : phase(0),
               entry(0),
               exit(0),
               on(true)
    {}

    /*! 
     * Add a pair of addresses as boundaries of phases.
     * param [in]  entry_     the address of phase entry
     * param [in]  exit_      the address of phase exit
     */
    inline void add(uintptr_t entry_, uintptr_t exit_) {
       impl.push_back(make_pair(entry_, exit_));
        if ( !entry ) entry = entry_;
        if ( !exit  ) exit  = exit_; 
    }

    /*! 
     * Prepare for the next phase
     * When the current phase finishes, the information
     * of the next phase is prepared.
     */
    inline void next() { 
        phase = (phase + 1) % impl.size(); 
        entry = impl[phase].first;
        exit  = impl[phase].second;
    }

    /*! 
     * Number of phases
     */
    inline int count() const {
        return impl.size();
    }

    uintptr_t entry;
    uintptr_t exit;
    volatile bool on;

};

