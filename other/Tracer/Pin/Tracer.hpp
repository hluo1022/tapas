#pragma once

#define NUM_BUF_PAGES 1024

#include "MLOG.hpp"
#include "Filter.hpp"

static BUFFER_ID bufId;
static TLS_KEY mlog_key;

extern PhaseFilterConfig PFC;

class TRACER {

public:

    BOOL Activate() {

        // Initialize the memory reference buffer
        bufId = PIN_DefineTraceBuffer(sizeof(struct MEMREF), NUM_BUF_PAGES,
                                      BufferFull, 0);

        if(bufId == BUFFER_ID_INVALID)
        {
            return false;
        }

        // Initialize thread-specific data not handled by buffering api.
        mlog_key = PIN_CreateThreadDataKey(0);

        PIN_AddThreadStartFunction(ThreadStart, 0);
        PIN_AddThreadFiniFunction(ThreadFini, 0);    
        TRACE_AddInstrumentFunction(Trace, 0);

        return true;
    }

private:

    static VOID * BufferFull(BUFFER_ID id, THREADID tid, 
                    const CONTEXT *ctxt, VOID *buf,
                    UINT64 numElements, VOID *v);
    static VOID ThreadStart(THREADID tid, CONTEXT *ctxt, 
                            INT32 flags, VOID *v);
    static VOID ThreadFini(THREADID tid, const CONTEXT *ctxt, 
                            INT32 code, VOID *v);
    static VOID Trace(TRACE trace, VOID *v);


};

/*!
 * Called when a buffer fills up, or the thread exits, so we can process it or pass it off
 * as we see fit.
 * @param[in] id        buffer handle
 * @param[in] tid       id of owning thread
 * @param[in] ctxt      application context
 * @param[in] buf       actual pointer to buffer
 * @param[in] numElements   number of records
 * @param[in] v         callback value
 * @return  A pointer to the buffer to resume filling.
 */
VOID * TRACER::BufferFull(BUFFER_ID id, THREADID tid, const CONTEXT *ctxt, VOID *buf,
                  UINT64 numElements, VOID *v)
{
    if ( ! KnobEmitTrace )
        return buf;

    struct MEMREF * reference=(struct MEMREF*)buf;
    
    MLOG * mlog = static_cast<MLOG*>( PIN_GetThreadData( mlog_key, tid ) );

    mlog->DumpBufferToFile( reference, numElements, tid );
    
    return buf;
}

VOID TRACER::ThreadStart(THREADID tid, CONTEXT *ctxt, INT32 flags, VOID *v)
{   
    // There is a new MLOG for every thread.  Opens the output file.
    MLOG * mlog = new MLOG(tid);
    
    // A thread will need to look up its MLOG, so save pointer in TLS
    PIN_SetThreadData(mlog_key, mlog, tid); 

}

VOID TRACER::ThreadFini(THREADID tid, const CONTEXT *ctxt, INT32 code, VOID *v)
{   
    MLOG * mlog = static_cast<MLOG*>(PIN_GetThreadData(mlog_key, tid));

    delete mlog;

    PIN_SetThreadData(mlog_key, 0, tid);
}

VOID TRACER::Trace(TRACE trace, VOID *v) {

    if(!PFC.on) { return; }
    
    // Insert a call to record the effective address.
    for(BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl=BBL_Next(bbl))
    {
        for(INS ins = BBL_InsHead(bbl); INS_Valid(ins); ins=INS_Next(ins))
        {
            if(INS_IsMemoryRead(ins) && INS_IsStandardMemop(ins))
            {
                INS_InsertFillBuffer(ins, IPOINT_BEFORE, bufId,
                                     IARG_MEMORYREAD_EA, offsetof(struct MEMREF, address),
                                     IARG_BOOL, false, offsetof(struct MEMREF, type),
                                     IARG_TSC, offsetof(struct MEMREF, time),
                                     IARG_THREAD_ID, offsetof(struct MEMREF, thread),
                                     IARG_END);
            }

            if (INS_HasMemoryRead2(ins) && INS_IsStandardMemop(ins))
            {
                INS_InsertFillBuffer(ins, IPOINT_BEFORE, bufId,
                                     IARG_MEMORYREAD2_EA, offsetof(struct MEMREF, address),
                                     IARG_BOOL, false, offsetof(struct MEMREF, type),
                                     IARG_TSC, offsetof(struct MEMREF, time),
                                     IARG_THREAD_ID, offsetof(struct MEMREF, thread),
                                     IARG_END);
            }

            if(INS_IsMemoryWrite(ins) && INS_IsStandardMemop(ins))
            {
                INS_InsertFillBuffer(ins, IPOINT_BEFORE, bufId,
                                     IARG_MEMORYWRITE_EA, offsetof(struct MEMREF, address),
                                     IARG_BOOL, true, offsetof(struct MEMREF, type),
                                     IARG_TSC, offsetof(struct MEMREF, time),
                                     IARG_THREAD_ID, offsetof(struct MEMREF, thread),
                                     IARG_END);
            }
        }
    }
}
