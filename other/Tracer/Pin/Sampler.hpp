#pragma once

#include <math.h>

extern KNOB<UINT32> KnobSamplerWarmup;
extern KNOB<UINT32> KnobSamplerLen;
extern KNOB<float> KnobSamplerTemporal;
extern KNOB<float> KnobSamplerSpatial;

/*!
 * Address filter is the default routine that
 * defines whether an address is to be filtered
 */
bool DefaultAddressFilter(ADDRINT address, std::size_t frequency) {
    std::hash<ADDRINT> hash_fn;
    return ( hash_fn( address ) % frequency ) == 0;
}

/*!
 * A sampler based on index bit selection
 */
template<std::size_t Frequency, std::size_t IndexBitMask>
bool IndexBitsAddressFilter(unsigned long long address) {
    return ( ( address & IndexBitMask ) % Frequency ) == 0;
}

class SAMPLER {


    bool on;

    UINT32 warmup;
    UINT32 length;
    UINT32 temporal;
    std::size_t spatial;

    bool (*addrfilter)(ADDRINT, std::size_t freq);

public:

    SAMPLER() : on(false), warmup(0),
                length(0), temporal(0),
                spatial(1), addrfilter(nullptr)
    {}

    VOID Activate() {
        on = true;
        warmup = KnobSamplerWarmup.Value();

        length = KnobSamplerLen.Value();
        temporal = roundf(length / KnobSamplerTemporal.Value());

        spatial = roundf(1 / KnobSamplerSpatial.Value());


        addrfilter = DefaultAddressFilter;
    }

    BOOL Sample(ADDRINT addr, UINT32 count) {

        // not activated
        if(!on) return true;

        // spatial check not pass
        if(spatial != 1) {
            if (!addrfilter(addr, spatial)) return false;
        }

        // warmup phase
        if (count < warmup) return false;
        count -= warmup;

        // sleep phase
        if (count % temporal > length) return false;

        return true;
    }

};
