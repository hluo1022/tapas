#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stddef.h>
#include <cstdint>

#include "pin.H"
#include "instlib.H"
#include "portability.H"

#include "MemRef.hpp"
#include "Sampler.hpp"
#include "Tracer.hpp"
#include "PinPhaseFilter.hpp"

using namespace std;
using namespace INSTLIB;

#define KNOB_LOG_NAME  "log"
#define KNOB_REPLAY_NAME "replay"
#define KNOB_OUTPUT_NAME "o"
#define KNOB_EMIT_NAME "emit"
#define KNOB_PHASE_FILE "phase"

#define KNOB_SAMPLE_NAME "sample"
#define KNOB_SAMPLE_WARMUP "sample:warmup"
#define KNOB_SAMPLE_LEN "sample:len"
#define KNOB_SAMPLE_FREQ "sample:frequency"
#define KNOB_SAMPLE_ADDRESS "sample:address"

#define KNOB_FAMILY "pintool"

TRACER Tracer;
PHASEFILTER Filter;
SAMPLER Sampler;

// ========================= //
// PinPlay related knobs     //
// ========================= //
KNOB<BOOL> KnobLogger(KNOB_MODE_WRITEONCE,
	                KNOB_FAMILY, KNOB_LOG_NAME, "0",
	                "Activate the pinplay logger");
KNOB<BOOL> KnobReplayer(KNOB_MODE_WRITEONCE,
                    KNOB_FAMILY, KNOB_REPLAY_NAME, "0",
                    "Activate the pinplay replayer");

// ========================= //
// Pintool related knobs     //
// ========================= //
KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE,  
                    KNOB_FAMILY, KNOB_OUTPUT_NAME, "trace",
                    "Name of the output trace base file.");
KNOB<BOOL> KnobEmitTrace(KNOB_MODE_WRITEONCE,
                    KNOB_FAMILY, KNOB_EMIT_NAME, "0",
                    "Emit the trace");
KNOB<string> KnobPhaseFile(KNOB_MODE_WRITEONCE,  
                    KNOB_FAMILY, KNOB_PHASE_FILE, "", 
                    "a file of function name pairs, where each "
                    "function pair represents a phase's boundary "
                    "e.g. __parsec_roi_begin and __parsec_roi_end in PARSEC.");

// ========================= //
// Sampler related knobs     //
// ========================= //
KNOB<BOOL> KnobSampler(KNOB_MODE_WRITEONCE,
                    KNOB_FAMILY, KNOB_SAMPLE_NAME, "0",
                    "Activate the sampler");
KNOB<UINT32> KnobSamplerWarmup(KNOB_MODE_WRITEONCE,
                    KNOB_FAMILY, KNOB_SAMPLE_WARMUP, "0",
                    "Sampler warmup length");
KNOB<UINT32> KnobSamplerLen(KNOB_MODE_WRITEONCE,
                    KNOB_FAMILY, KNOB_SAMPLE_LEN, "10000000",
                    "Sampler sample length");
KNOB<float> KnobSamplerTemporal(KNOB_MODE_WRITEONCE,
                    KNOB_FAMILY, KNOB_SAMPLE_FREQ, "0.01",
                    "Sampler temporal sampling frequency");
KNOB<float> KnobSamplerSpatial(KNOB_MODE_WRITEONCE,
                    KNOB_FAMILY, KNOB_SAMPLE_ADDRESS, "1",
                    "Sampler spatial sampling frequency");

INT32 Usage()
{
    cerr <<
        "This pin tool is a Pin-enabled Membuffer-based tracer for Tapas module \n"
        "\n";

    cerr << KNOB_BASE::StringKnobSummary() << endl;
    return -1;
}

int main(int argc, char *argv[]) {

    if( PIN_Init(argc,argv) )
    {
        return Usage();
    }
    PIN_InitSymbols();

    if (!Tracer.Activate()) {
        cerr << "Error: could not allocate initial buffer" << endl;
        return -1;
    }

    if (KnobSampler.Value()) {
        Sampler.Activate();
    }

    Filter.Activate();

    PIN_StartProgram();
}
