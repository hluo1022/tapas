#include "pin.H"

#include <fstream>
#include <sstream>
#include <iostream>

#include "Filter.hpp"

/* ===================================================================== */
// Phase Filter
/* ===================================================================== */

PhaseFilterConfig PFC;

/* forward declaration */
VOID PIN_FAST_ANALYSIS_CALL onPhaseEntry();
VOID PIN_FAST_ANALYSIS_CALL onPhaseExit(ADDRINT);

/*!
 * Helper function.
 * This function performs the insertion of callbacks 
 * at the phase entry and exit.
 */
LOCALFUN VOID InsertCallatPhaseBoundary() {

    /* instrument the phase entry function */
    PIN_LockClient();
    RTN entry_rtn = RTN_FindByAddress(PFC.entry);
    PIN_UnlockClient();

    if ( RTN_Valid(entry_rtn) ) {
        LOG("inserted entry call\n");
    } 

    RTN_Open(entry_rtn);
    RTN_InsertCall(entry_rtn,                // routine
                   IPOINT_BEFORE,            // position
                   AFUNPTR(onPhaseEntry),    // callback
                   IARG_FAST_ANALYSIS_CALL,
                   IARG_END);
    RTN_Close(entry_rtn);

    /* instrument the phase exit function */
    PIN_LockClient();
    RTN exit_rtn = RTN_FindByAddress(PFC.exit);
    PIN_UnlockClient();

    if ( RTN_Valid(exit_rtn) ) {
        LOG("inserted exit call\n");
    } 

    RTN_Open(exit_rtn);
    RTN_InsertCall(exit_rtn,                  // routine
                   IPOINT_BEFORE,             // position
                   AFUNPTR(onPhaseExit),      // callback
                   IARG_FAST_ANALYSIS_CALL,
                   IARG_INST_PTR,    // routine address
                   IARG_END);
    RTN_Close(exit_rtn);
}

/*!
 * Callbacks at the phase boundary
 * This function is called at the phase entry. It reenables
 * instrumentations.
 *
 * @param[in]   addr    address used to verify whether this
 *                      routine is current phase's boundary
 */
VOID PIN_FAST_ANALYSIS_CALL onPhaseEntry() {
     LOG("In phase entry\n");
     PFC.on = true;
     CODECACHE_FlushCache(); 
}

/*!
 * Callbacks at the phase boundary
 * This function is called at the phase exit. It disables
 * instrumentations and setup the marks for the next phase.
 *
 * @param[in]   addr    address used to verify whether this
 *                      routine is current phase's boundary
 */
VOID PIN_FAST_ANALYSIS_CALL onPhaseExit(ADDRINT addr) {
     LOG("In phase exit\n");
     if ( PFC.exit == addr ) {
         PFC.next();
         PFC.on = false;
         CODECACHE_FlushCache();
     }
}

static bool nonempty = false;

/*!
 * Setup all predefined phase boundaries.
 * This function is called at image loading. 
 *
 * @param[in]   image     image object used to search for the
 *                        phase boundaries' address
 */
void InstrumentPhaseBoundary(IMG image, const string &filename) {

    if ( filename.empty() || nonempty ) {
        /*
         * no phase is specified, therefore
         * we choose whole-program profiling
         */
        return;
    }

    std::ifstream phase_file;
    phase_file.open(filename.c_str(), std::ios_base::in);
    if ( !phase_file.good() ) {
        LOG(std::string("Can't open file ") + filename + "\n");
        return;
    }

    std::string entry_name, exit_name;
    std::string line;

    /// TODO handle phase in different images
    //getline(phase_file, line);
    //int count = atoi(line.c_str());
    //if ( count == PFC.count() ) return;

    while( getline(phase_file, line) )
    {
        std::stringstream sstr(line);
        getline(sstr, entry_name, '\t');
        getline(sstr, exit_name, '\t');

        RTN entry = RTN_FindByName(image, entry_name.c_str());
        RTN exit  = RTN_FindByName(image, exit_name.c_str());
        if ( RTN_Valid(entry) && RTN_Valid(exit) ) 
        {
            nonempty = true;
            PFC.add(RTN_Address(entry), RTN_Address(exit));
        }
    }
    phase_file.close();

    if (nonempty) {
        LOG("Inserted boundary call\n");
        PFC.on = false;
        InsertCallatPhaseBoundary();
        LOG(decstr(PFC.entry) + " " + decstr(PFC.exit));
    }
}

extern KNOB<string> KnobPhaseFile;

class PHASEFILTER {

public:

    VOID Activate() {
        // Register function to be called to intrument images
        IMG_AddInstrumentFunction(Image, 0);
    }

private:

    static VOID Image(IMG img, VOID *v);

};


VOID PHASEFILTER::Image(IMG img, VOID *v) {

    InstrumentPhaseBoundary(img, KnobPhaseFile.Value());

}
