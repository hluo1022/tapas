#pragma once

extern KNOB<string> KnobOutputFile;
extern KNOB<BOOL> KnobEmitTrace;

extern SAMPLER Sampler;

/*
 * MLOG - thread specific data that is not handled by the buffering API.
 */
class MLOG {   

public:
    MLOG(THREADID tid);
    ~MLOG();

    VOID DumpBufferToFile( struct MEMREF * reference, UINT64 numElements, THREADID tid );
          
private: 
    struct MEMREF buffer[NUM_BUF_PAGES * 4096 / sizeof(MEMREF) ];
    ofstream _ofile;
    UINT64 nElements;
};

MLOG::MLOG(THREADID tid)
{
    nElements = 0;
    if (KnobEmitTrace)
    {
        //string filename = KnobOutputFile.Value() + "." + decstr(getpid_portable()) + "." + decstr(tid);
        string filename = KnobOutputFile.Value() + "." + decstr(tid);
        _ofile.open(filename.c_str(), std::ios_base::out | 
                                      std::ios_base::binary |
                                      std::ios_base::app);

        if ( ! _ofile )
        {
            cerr << "Error: could not open output file." << endl;
            exit(1);
        }

    }

}

MLOG::~MLOG()
{
    if (KnobEmitTrace)
    {
        _ofile.close();
    }
}


VOID MLOG::DumpBufferToFile( struct MEMREF * reference, UINT64 numElements, THREADID tid )
{
    UINT64 j, i;
    for(i = 0, j = 0; i < numElements; ++i) {
        if (Sampler.Sample( reference[i].address, nElements + i)) {
            buffer[j++] = reference[i]; 
        }
    }
    _ofile.write((char *)buffer, j * sizeof(struct MEMREF));
    nElements += numElements;
}
