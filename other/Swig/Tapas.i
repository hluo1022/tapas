/* File : Tapas.i */

%module Tapas
#include "std_set.i"
#include "std_string.h"
%include "std_map.i"
%include "std_pair.i"

%{
#define SWIG_FILE_WITH_INIT
#include "Tapas.hpp"
%}

namespace Tapas {
	typedef unsigned NodeType;
	typedef std::pair<NodeType, NodeType> LinkType;
	typedef std::map<unsigned, NodeType> DomainMap;
}

using namespace Tapas;

%include "Tapas.hpp"

%template(LinkType) std::pair<NodeType, NodeType>;
%template(DomainMap) std::map<NodeType, NodeType>;
