set_source_files_properties(Tapas.i PROPERTIES CPLUSPLUS ON)
set_source_files_properties(Tapas.i PROPERTIES SWIG_FLAGS "")

swig_add_module(Tapas python Tapas.i)
swig_link_libraries(Tapas ${PYTHON_LIBRARY} Tapas_Static)