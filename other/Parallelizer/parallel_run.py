#!/usr/bin/python

import argparse
import os
import errno
import operator

#from collections import Counter

import subprocess

parser = argparse.ArgumentParser(description='A wrapper of OptDataPlace, which parallelizes the execution of OptDataPlace')

parser.add_argument('-n', action='store', dest='n', help='degree of parallelism')
parser.add_argument('-a', action='store', dest='addr', help='address file')
parser.add_argument('-p', action='store', dest='page', help='initial page map file')
parser.add_argument('-c', action='store', dest='comm', help='command to run')

TotalNodes = 2
TotalCores = 20

# actual file partitioning
def partition_file(n, src, dst, lines):

	# partition address file
	fd = open(src, 'r')
	content = fd.readlines()
	stored = 0
	for id in range(n):
		directory = "tmp" + str(id)
		sub = open(directory + '/' + dst, 'w')
		for l in range(stored, stored + lines):		
			sub.write(content[l])
		stored += lines
		if id == n-1:
			for l in range(stored, n):
				sub.write(content[l])
	
# set up the files for running
def setup(n, addr, pagemap, dirs):

	# read the address file
	total_lines = sum(1 for line in open(addr))
	each_lines = total_lines / n
	
	# creating directories
	for d in dirs:
		if not os.path.exists(d):
			os.makedirs(d)

	# partition address file
	partition_file(n, addr, 'address', each_lines)

	if os.path.exists(pagemap):
		# partition pagemap file
		partition_file(n, pagemap, 'place0', each_lines)

# normalize the command to be executed by changing the relative paths
# to absolute paths
def normalize_exec_cmd(comm):
	cmd = comm.split()
	
	# identify files and find their relative dir paths
        for i in range(len(cmd)):
		if os.path.exists(cmd[i]) or os.path.exists(cmd[i] + ".traffic"):
			# change to absolute file path
			cmd[i] = os.path.abspath(cmd[i])

	# special handling of address file, replace them with the 
	# one inside each tmpX directory
	for i in range(len(cmd)):
		if cmd[i] == "-a":
			cmd[i+1] = "./address"
		if cmd[i] == "-p" and os.path.exists(cmd[i+1]):
			cmd[i+1] = "./place0"
	if not "-a" in cmd:
		cmd.append("-a")
		cmd.append("./address")

	return cmd

# return the node id from a core id
def get_node(c):

	CoresPerNode = TotalCores / TotalNodes
	
	return c / CoresPerNode
	
def most_significant(nodes, weights):
	print nodes
	print weights

	h = {}
	for i in range(len(nodes)):
		if not nodes[i] in h:
			h[nodes[i]] = weights[i]
		else:
			h[nodes[i]] += weights[i]
	return max(h.iteritems(), key=operator.itemgetter(1))[0]

def resolve_plans(plans, plan_weight):
	result = [None] * len(plans[0])
	
	placement = {}
	for p in plans:
		# for each plan 
		for i in range(len(p)):
			n = get_node(p[i])
			if not i in placement:
				placement[i] = [n]
			else:
				placement[i].append(n)

	print placement

	# vote for the most common place
	for c, n in placement.iteritems():
		p = most_significant(n, plan_weight)
		result[c] = p

	free_cores = [0, 80]

	for i in range(len(result)):
		node = result[i] / 10
		result[i] = free_cores[node]
		free_cores[node] += 8

	print result
	return result

if __name__ == '__main__':

	args = parser.parse_args()

	# we need four arguments:
	#   1. the parallelsim of the execution
	#   2. the address file that is to be partitioned
	#   3. the initial place file, if there is any
	#   4. the command line to be executed

	parallelism = int(args.n)
	addr = args.addr
	page = args.page
	cmd = args.comm

	# setup the directories
	# set up directories to store results
	dirs = []
	for id in range(parallelism):
		name = "tmp" + str(id)
		dirs.append(name)

	setup(parallelism, addr, page, dirs)

	# process the command to be executed
	cmd = normalize_exec_cmd(cmd)
	print cmd

	# launch tasks and wait for them to finish
	processes = []
        output = []
	for i in range(parallelism):
		os.chdir(dirs[i])
		f = open('output', 'w')
		output.append(f)
		p = subprocess.Popen(cmd, stdout=f)
		os.chdir("..")
		processes.append(p)

	i = 0
	for p in processes:
		p.wait()
		output[i].flush()
		i += 1
	
	# process the results
	place_file = open("place", 'w')
	plans = []
	plan_weight = []
	for i in range(parallelism):
		os.chdir(dirs[i])
		
		# open each address file and copy to out_addr
		with open("place", "r") as f:
			for l in f.readlines():
				place_file.write(l)
		
		# open each plan file and resolve to one plan
		with open("plan", "r") as f:
			l = [int(i) for i in f.readlines()[0].split()]
			plans.append(l)
			
		with open("output", "r") as f:
			for l in f.readlines():
				if "Total_Impact" in l.split():
					plan_weight.append(int(l.split()[1]))
					break
		os.chdir("..")

	# resolve plans and output to file
	result = resolve_plans(plans, plan_weight)
	plan_file = open("plan", 'w')
	plan_file.write(' '.join([str(i) for i in result]))

