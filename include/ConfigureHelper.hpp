#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <cassert>
#include <sstream>

#include "type.h"

using namespace std;

class SystemConfig {
public:
    uint32_t ncores;
    uint32_t nnodes;
    
    uint32_t nchips;
    uint32_t ndcms;

    uint32_t nlinks; //the number of links, bidirectional links counted once

    // topology model
    vector<uint64_t> MemCapacity;
    vector<uint64_t> LLCCapacity;
    vector<vector<NodeType>> Chip2Cores; //chip2cores 
    vector<vector<NodeType>> DCM2Chips; //DCM2chips
    vector<NodeType> Core2Chip;
    vector<NodeType> Chip2DCM;
    
    map<LinkType, uint32_t> LinkID; ///< Links and its id, bidirectional links have the same id for all direction
    vector<bool> LinkIsBidirectional;
    vector<double> LinkBandwidth; ///< Bandwidth of links
    vector<double> LinkLatency; ///< Latency on links
    
    vector<double> LLC2MLatency; //<! latency of each chip
    vector<double> LLC2MBandwidth; //<! bw of each chip

    bool isChipScope(ScopeType a);
    bool isDCMScope(ScopeType a);
    bool isGlobalScope(ScopeType a);
    ScopeType getScope(NodeType core1, NodeType core2);
    NodeType getScope(ScopeType a);
    vector<NodeType> getShortestPath(NodeType src, NodeType dst); //<! shortest path from a chip to another
    NodeType getClosetChip(NodeType src, uint64_t mask);
    vector<LinkType> getLinksInScope(NodeType src, NodeType dst); //get links in the scope, the scope is determined by two cores 
    vector<NodeType> getOrderedChip(NodeType c);
    void ShowInfo(ostream &out = std::cout);
    void print(ostream &out = cout);
    static SystemConfig* deserialize(string filename);
    SystemConfig(){};
};

class ConfigureParser{
private:
    map<string, vector<string>> entites;
    template<typename T> T ParseSingleValue(string key);
    template<typename T> vector<T> Parse1DValue(string key); 
    template<typename T> vector<vector<T>> Parse2DValue(string key);
    template<typename T> vector<T> ParseValue(string value); //<! return a vector of value

public:
    ConfigureParser(istream &is);
    void ReadConfig(istream &is);
    void ParseSystemConfig(SystemConfig &config);
};

