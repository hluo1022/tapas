#pragma once

#include <numaif.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <numa.h>

FILE* roi_fp;

void open_address_range_file() {
    roi_fp = fopen("address", "w+");
}

void close_address_range_file() {
    fclose(roi_fp);
}

char names[1024][1024];
void* arrays[1024];
size_t sizes[1024];
int cnt = 0;

uint64_t page_size;
uint64_t page_count;

int* GetPageMapping(){
    int i,j,p;
    int *ret;
    void** ptrs;
    page_size = getpagesize();
    page_count = 0;
    for(i = 0; i < cnt; i++)
        page_count += sizes[i]/page_size;

    ret = (int*)malloc(page_count*sizeof(int));
    ptrs = (void**)malloc(page_count*sizeof(void*));
    
    for(i = 0, p = 0; i < cnt; i++){
        for(j = 0; j < sizes[i]; j += page_size)
            ptrs[p++] = arrays[i]+j;
    }

    if(move_pages(0, page_count, ptrs, NULL, ret, 0))
        perror("error : ");
    
    return ret;
}

void CheckPageStat(int* stat){
    int i;
    int numa_nodes = numa_max_node();

    for(i = 0; i < page_count; i++){
        if(stat[i] < 0 || stat[i] >= numa_nodes)
            printf("%dth page with status %d\n", i, stat[i]);
    }
}

void OutputStats(int* stat){
    int i;
    for(i = 0; i < cnt; i++)
        printf("%s %llu\n", names[i], sizes[i]/page_size);
    
    for(i = 0; i < page_count; i++)
        printf("%d\n", stat[i]);
}

void GetAllPageMapping(){
    int *stat = GetPageMapping();
    OutputStats(stat);
}

void CheckOverlap(char* name, void* base, size_t size){
    int i = 0;
    for(i = 0; i < cnt; i++){
        if(arrays[i] <= base && arrays[i]+sizes[i] > base)
            printf("\tOverlap 1: %s %s\n", names[i], name);
        if(arrays[i] < base+size && arrays[i]+sizes[i] > base+size)
            printf("\tOverlap 2: %s %s\n", names[i], name);
        if(arrays[i] >= base && arrays[i] < base+size)
            printf("\tOverlap 3: %s %s\n", names[i], name);
    }

    strcpy(names[cnt], name);
    arrays[cnt] = base;
    sizes[cnt] = size;
    cnt++;
}


void register_address_range(const char* name, void* base, size_t size, unsigned long units, size_t unit) {
    unsigned long i;
    //printf("%s %llu %lu\n", name, base, size);
    CheckOverlap(name, base, size);
    //printf("in entry, %p, %u, %u\n", base, size, units);
    for(i = 0; i < units; ++i) {
        fprintf(roi_fp, "%s\t%p\t%p\t%lu\t%lu\n", name,
                                base + size / units * i, 
                                base + size / units * (i+1),
                                (size / unit) / units * i,
                                (size / unit) / units * (i + 1)
               );
    }
}

void __attribute__ ((noinline)) roi_begin() {
    printf("ROI begins\n");
}

void __attribute__ ((noinline)) roi_end() {
    printf("ROI ends\n");
}
