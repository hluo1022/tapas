#pragma once

#include <ostream>

struct MEMREF {

    /* the target address */
    unsigned long long address;

    /* access type (R/W) */
    unsigned type;

    /* the accessing thread */
    unsigned thread;

    /* instruction counter */
    unsigned long long time;

    void print(std::ostream &out) {
        out << std::hex << "0x" << address << std::dec << " ";
        out << type    << " ";
        out << thread  << " ";
        out << time    << " ";
        out << "\n";
    }

};
