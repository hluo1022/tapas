#include <iostream>
#include <fstream>
#include <vector>
#include <assert.h>
using namespace std;

class ThreadPlacement{
public:
    vector<int> place;
    ThreadPlacement(string f);
    ThreadPlacement();
    int GetNThreads();
    void OutputToFile(string f);
    void Output();
};

class DataPlacement{
public:
    vector<string> arrays;
    vector<int> start;
    vector<int> end;
    vector<int> place;

    DataPlacement(string f);
    DataPlacement();
    void OutputToFile(string f);
    void Output();
};

ThreadPlacement::ThreadPlacement(){
}

ThreadPlacement::ThreadPlacement(string f){
    ifstream fin(f);
    assert(fin.good() && "Open file failed");
    int a;
    while(fin >> a)
        place.push_back(a);
    fin.close();
}

int ThreadPlacement::GetNThreads(){
    return place.size();
}

void ThreadPlacement::OutputToFile(string f){
    ofstream fout(f);
    for(int i = 0; i < place.size(); i++)
        fout << place[i] << " ";
    fout << "\n";
    fout.close();
}

void ThreadPlacement::Output(){
    for(int i= 0; i < place.size(); i++)
        cout << place[i] << " ";
    cout << endl;
}

DataPlacement::DataPlacement(){
}

DataPlacement::DataPlacement(string f){
    ifstream fin(f);
    assert(fin.good() && "Open file failed");
    string x;
    int offset1;
    int offset2;
    int myplace;

    while(fin >> x){
        fin >> offset1 >> offset2 >> myplace;
        arrays.push_back(x);
        start.push_back(offset1);
        end.push_back(offset2);
        place.push_back(myplace);
    }

    fin.close();
}

void DataPlacement::OutputToFile(string f){
    ofstream fout(f);
    assert(fout.good() && "Open file failed");
    for(int i = 0; i < arrays.size(); i++)
        fout << arrays[i] << " " << start[i] << " " << end[i] << " " << place[i] << "\n";
    fout.close();
}

void DataPlacement::Output(){
    for(int i = 0; i < arrays.size(); i++)
        cout << arrays[i] << " " << start[i] << " " << end[i] << " " << place[i] << endl;
}

