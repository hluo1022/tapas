#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#ifndef POWER8
#define POWER8
#endif

#define MAX_CORES 160
#define MAX_NODES 160
#define CORES_PER_SOCKET 1

#ifdef POWER8
#define core_to_node(x) ((x) / CORES_PER_SOCKET)
#else
#define core_to_node(x) ((x) % (MAX_CORES / CORES_PER_SOCKET))
#endif

static cpu_set_t cpusets[MAX_NODES];
static unsigned short* plan;

static int cpusets_len = 0;

static int status = 0;

// routine that parses the thread binding environment variable
static int parse_affinity_env() {
  char *env, *end;
  unsigned long cpu_beg, cpu_end, cpu_stride;
  unsigned short *cpus = NULL;
  size_t allocated = 0, used = 0, needed;

  env = getenv ("GOMP_CPU_AFFINITY");
  if (env == NULL) {
    printf("No GOMP_CPU_AFFINITY set\n");
    status = -1;
    return -1;
  }

  do
    {
      while (*env == ' ' || *env == '\t')
	env++;

      cpu_beg = strtoul (env, &end, 0);
      cpu_end = cpu_beg;
      cpu_stride = 1;
      if (env == end || cpu_beg >= 65536)
	goto invalid;

      env = end;
      if (*env == '-')
	{
	  cpu_end = strtoul (++env, &end, 0);
	  if (env == end || cpu_end >= 65536 || cpu_end < cpu_beg)
	    goto invalid;

	  env = end;
	  if (*env == ':')
	    {
	      cpu_stride = strtoul (++env, &end, 0);
	      if (env == end || cpu_stride == 0 || cpu_stride >= 65536)
		goto invalid;

	      env = end;
	    }
	}

      needed = (cpu_end - cpu_beg) / cpu_stride + 1;
      if (used + needed >= allocated)
	{
	  unsigned short *new_cpus;

	  if (allocated < 64)
	    allocated = 64;
	  if (allocated > needed)
	    allocated <<= 1;
	  else
	    allocated += 2 * needed;
	  new_cpus = (short unsigned int*)realloc (cpus, allocated * sizeof (unsigned short));
	  if (new_cpus == NULL)
	    {
	      free (cpus);
	      printf("not enough memory to store GOMP_CPU_AFFINITY list\n");
	      return -1;
	    }

	  cpus = new_cpus;
	}

      while (needed--)
	{
	  cpus[used++] = cpu_beg;
	  cpu_beg += cpu_stride;
	}

      while (*env == ' ' || *env == '\t')
	env++;

      if (*env == ',')
	env++;
      else if (*env == '\0')
	break;
    }
  while (1);

  plan = cpus;
  return used;

invalid:
  printf("Invalid value for enviroment variable GOMP_CPU_AFFINITY");
  return -1;
}

// routine that initialize the thread binding facility
int init_planner() {

    cpusets_len = parse_affinity_env();
    if ( cpusets_len <= 0 ) {
        return -1;
    }

    for(int i = 0; i < MAX_NODES; ++i) {
        CPU_ZERO(&cpusets[i]);
    }

    for(int i = 0; i < MAX_CORES; ++i) {
        CPU_SET(i, &cpusets[core_to_node(i)]);
    }
    return 0;
}

// routine that actually binds threads
int place_thread(unsigned int id) {
    if ( status < 0 ) return -1;
    printf("id %d, cpusets_len %d\n", id, cpusets_len);
    if (id < 0 || id >= cpusets_len) {
        printf("Invalid thread id %d in place_thread\n", id);
        return -1;
    }

    int node = core_to_node(plan[id]);
    cpu_set_t cs = cpusets[node];
    pthread_setaffinity_np(pthread_self(), sizeof(cs), &cs);
    return 0;
}
