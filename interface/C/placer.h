#pragma once

#include <unistd.h>
#include <assert.h>
#include <stdio.h>
#include <numaif.h>
#include <numa.h>

//#include "general.h"

#ifdef POWER8
#define POWER8NODE 2
#else
#define POWER8NODE 1
#endif 

static FILE* place_fp = NULL;

unsigned long PAGE_SIZE;
#define PAGE_BASE(x) (((x) / PAGE_SIZE) * PAGE_SIZE)

struct address_info {
    void* ptr;
    char id[128];
    size_t unit;
};

struct address_name_map {
    struct address_info data[64];
    unsigned long size;
} address_names;

void open_place_file() {
    place_fp = fopen("place", "r");
    if ( place_fp == NULL ) {
        fprintf(stderr, "cannot open place file : turn into free-for-all mode\n");
    }
}

void close_place_file() {
    if(place_fp == NULL) return;
    fclose(place_fp);
}

void register_named_address(const char* id, void* base, size_t unit) {
    if(place_fp == NULL) return;
    address_names.data[address_names.size].ptr = base;
    strncpy(address_names.data[address_names.size].id, id, 128);
    address_names.data[address_names.size].unit = unit;
    address_names.size++;
}

void place_address_range() {
    unsigned long i;
    char buf[2048];
    char id[2048];
    
    if(place_fp == NULL) return;

    #ifdef BIND_DATA
    printf("Data binding enabled\n");
    #endif

    int lines = 0;
    PAGE_SIZE = sysconf(_SC_PAGESIZE);
    while(fgets(buf, 2048, place_fp)) {
        unsigned long start_idx, end_idx;
        int node;
        sscanf(buf, "%s\t%lu\t%lu\t%d", id, &start_idx, &end_idx, &node);
        //node = (lines++ / 80) % 2;
        node *= POWER8NODE;
        for(i = 0; i < address_names.size; ++i) {
            if(0 == strcmp(address_names.data[i].id, id)) {
                unsigned long long base = PAGE_BASE((unsigned long long)address_names.data[i].ptr + start_idx * address_names.data[i].unit);
                unsigned long long end  = PAGE_BASE((unsigned long long)address_names.data[i].ptr + end_idx * address_names.data[i].unit);
                unsigned long size = ((unsigned long)(end - base)) / PAGE_SIZE;
                if ( size != 0 ) {

                    struct bitmask *nodes;
                    nodes = numa_allocate_nodemask();
                    numa_bitmask_setbit(nodes, node);
                    //assert((((size_t)base)%PAGE_SIZE) == 0);
                    #ifdef BIND_DATA
                    printf("bind [%p, %p] to node %d\n", base, 
                                 base + size * PAGE_SIZE, 
                                 node);
                    if (mbind((void*)base, size * PAGE_SIZE, 
                                   MPOL_BIND, 
                                   nodes->maskp, nodes->size + 1, 
                                   MPOL_MF_MOVE)) {
                        perror("err: ");
                    }
                    numa_bitmask_free(nodes);
                    #endif
                }
            }
        }
    }
}

void verify_address_range() {
    pid_t pid = getpid();
    char verify_cmd[512];
    sprintf(verify_cmd, "cat /proc/%d/numa_maps > numa_maps", pid);
    if(system(verify_cmd)) {
        perror("verify address binding err:");
    }
}
