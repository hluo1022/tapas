#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <thread>
using namespace std;

int *A;

void func(uint64_t len){
    uint64_t sum = 0;
    for(uint64_t rep = 0; rep < 100; rep++)
    for(uint64_t i = 0; i < len; i++)
        sum += A[i];
    cout << sum << endl;
}

int main(int argc, char **argv){
    cout << "Usage : command len" << endl;
    uint64_t len = atoll(argv[1]);
    A = new int[len];

    int nt = 2;
    if(argc > 2){
        nt = atoi(argv[2]);
    }

    thread *t[128];
    for(int i = 0; i < nt; i++)
        t[i] = new thread(func, len);
    for(int i = 0; i < nt; i++)
        t[i]->join();

    return 0;
}
